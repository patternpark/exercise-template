// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { get, IncomingMessage } from 'http';
import { get as httpsGet } from 'https';
import { Stream } from 'stream';
import { readFile } from 'fs';
import { promisify } from 'util';

/**
 * Specifies how often a redirect should be followed.
 */
const FOLLOW_REDIRECTS = 2;
const REDIRECT_CODES = [301, 302, 307, 308];

export const LOCAL_FILE_URL_INDICATOR = 'file://';

export const readFileAsync = promisify(readFile);

/**
 * Converts a stream into a string.
 * @param stream The stream which should be converted.
 */
export async function convertStreamToString(stream: Stream): Promise<string> {
  const chunks: any[] = [];
  return new Promise((resolve, reject) => {
    stream.on('data', (chunk) => chunks.push(chunk));
    stream.on('error', reject);
    stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')));
  });
}

/**
 * Downloads the content of a specific URL.
 *
 * @param url The URL whose content is to be downloaded.
 * @param followRedirectCounter Specifies how often a redirect should be followed. Default `FOLLOW_REDIRECTS`.
 */
export async function downloadFile(
  url: string,
  followRedirectCounter = FOLLOW_REDIRECTS
): Promise<string | undefined> {
  return new Promise((resolve) => {
    const callbackFunc = async (response: IncomingMessage): Promise<void> => {
      if (
        REDIRECT_CODES.includes(response.statusCode ?? 0) &&
        followRedirectCounter > 0 &&
        response.headers.location
      ) {
        resolve(
          await downloadFile(response.headers.location, --followRedirectCounter)
        );

        return;
      }

      if (response.statusCode !== 200 && response.statusCode !== 202)
        resolve(undefined); // exception is not catchable ( idk why :thinking: )

      resolve(await convertStreamToString(response));
    };

    const parsedURL = new URL(url);
    if (url.startsWith('https'))
      httpsGet(
        {
          host: parsedURL.host,
          hostname: parsedURL.hostname,
          rejectUnauthorized: false,
          path: parsedURL.pathname,
        },
        callbackFunc
      );
    else get(url, callbackFunc);
  });
}

/**
 * Gets the content of an URL or path.
 *
 * @param urlOrPath The URL or path whose content should be returned.
 */
export async function getContentFromURLOrPath(
  urlOrPath: string
): Promise<string> {
  // it is a url, so download the content
  if (isURL(urlOrPath)) {
    const content = await downloadFile(urlOrPath);
    if (content === undefined)
      throw new Error(
        'Invalid URL. The response was empty (status code was not 200/202!).'
      );

    return content;
  }

  // it is a normal path, so read the file AND
  // checks on file:// and cuts!
  if (urlOrPath.toLowerCase().startsWith(LOCAL_FILE_URL_INDICATOR))
    urlOrPath = urlOrPath.substring(LOCAL_FILE_URL_INDICATOR.length);

  const queryStart = urlOrPath.indexOf('?');
  if (queryStart > 0) {
    return (await readFileAsync(urlOrPath.substring(0, queryStart))).toString();
  }

  return (await readFileAsync(urlOrPath)).toString();
}

/**
 * Determines whether the input is an URL.
 * @param input
 */
export function isURL(input: string): boolean {
  return input.toLowerCase().startsWith('http');
}
