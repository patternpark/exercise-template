// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

export const EVENT_GET_START_ARGS = 'getStartArgs';
export const EVENT_GET_START_ARGS_RESULT = 'getStartArgsResult';
export const EVENT_SET_FULLSCREEN = 'setFullscreen';
export const EVENT_GET_JSON_FILE = 'getJSONFile';
export const EVENT_GET_JSON_FILE_RESULT = 'getJSONFileResult';
export const EVENT_CLOSE_APP = 'closeApp';
