// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { app, BrowserWindow, ipcMain } from 'electron';
import { join } from 'path';
import {
  EVENT_CLOSE_APP,
  EVENT_GET_JSON_FILE,
  EVENT_GET_JSON_FILE_RESULT,
  EVENT_GET_START_ARGS,
  EVENT_GET_START_ARGS_RESULT,
  EVENT_SET_FULLSCREEN,
} from './events.constants';
import { getContentFromURLOrPath } from './read.utility';

const isDebugMode = !app.isPackaged; // see: https://www.electronjs.org/docs/api/app#appispackaged-readonly   !(process.env.NODE_ENV === 'production');
const DIRECTORY_OR_URL_ROOT = isDebugMode
  ? 'http://localhost:4222'
  : join(__dirname, '../exercise-template');

const ARGUMENT_INDEX_START = isDebugMode ? 2 : 1;
const URL = isDebugMode
  ? DIRECTORY_OR_URL_ROOT
  : join(DIRECTORY_OR_URL_ROOT, 'index.html');

let mainWindow: BrowserWindow;
if (isDebugMode) {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  require('electron-reload')(__dirname, {
    electron: require(`${__dirname}/../../node_modules/electron`),
  });
}

app.on('ready', () => {
  mainWindow = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true, // allows IPC and other APIs
      contextIsolation: false,
    },
    show: !isDebugMode,
  });

  app.commandLine.appendSwitch('autoplay-policy', 'no-user-gesture-required');
  if (URL.startsWith('http')) mainWindow.loadURL(URL);
  else mainWindow.loadFile(URL);

  if (isDebugMode) mainWindow.webContents.openDevTools();

  mainWindow.setFullScreen(!isDebugMode);
  mainWindow.setMenuBarVisibility(isDebugMode);

  if (isDebugMode) mainWindow.showInactive();
});

app.on('window-all-closed', () => {
  app.quit();
});

ipcMain.on(EVENT_GET_START_ARGS, (event) => {
  event.reply(
    EVENT_GET_START_ARGS_RESULT,
    [...process.argv].splice(ARGUMENT_INDEX_START)
  );
});

ipcMain.on(EVENT_SET_FULLSCREEN, () => {
  setFullscreenOnAllVideoTags();
});

ipcMain.on(EVENT_GET_JSON_FILE, async (event, file: string) => {
  let object: object = {};
  try {
    object = JSON.parse(
      await getContentFromURLOrPath(`${DIRECTORY_OR_URL_ROOT}/${file}`)
    );
  } catch (e) {
    console.info("JSON file doesn't exist or invalid");
  }
  event.reply(EVENT_GET_JSON_FILE_RESULT, object);
});

ipcMain.on(EVENT_CLOSE_APP, () => {
  app.quit();
});

/**
 * @author Aniketos Stamatios (Pseudonym)
 */
function setFullscreenOnAllVideoTags() {
  mainWindow.webContents.executeJavaScript(
    `Array.from(document.querySelectorAll("video")).forEach(e => e.requestFullscreen.call(e));`,
    true
  );
}
