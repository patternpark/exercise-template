// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

const packager = require('electron-packager');
const setLanguages = require('electron-packager-languages');

const INDICATOR_WIN = 'win';
const INDICATOR_LIN = 'lin';

const VERSION = '1.0.0.0';

const defaultOptions = {
  name: 'pattern-park-exercises',
  buildPath: __dirname,
  version: VERSION,
  buildVersion: VERSION,
  dir: 'dist',
  out: 'release-builds',
  overwrite: true,
  // asar: true,
  icon: 'dist/assets/logo.gif',
  appCopyright: 'Hochschule Stralsund',
  prune: true,
  afterCopy: [setLanguages(['de'])],
};

if (process.argv.find((arg) => arg === INDICATOR_WIN))
  packager({
    ...defaultOptions,
    platform: 'win32',
    arch: 'ia32',
    appVersion: VERSION,
    win32metadata: {
      ProductName: 'Pattern Park Exercises',
    },
  });

if (process.argv.find((arg) => arg === INDICATOR_LIN))
  packager({
    ...defaultOptions,
    platform: 'linux',
    arch: 'x64',
    appVersion: VERSION,
  });
