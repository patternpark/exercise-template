// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

export const HEADER_TITLE = 'Aufgabenstellung';
export const HEADER_MESSAGE = 'Hier soll die Aufgabenstellung beschrieben werden';

export const TIP_TITLE = 'Hinweis';
export const TIP_INIT_MESSAGE = 'Klicke einmal auf eines der Symbole und setze es dann in das richtige Feld.';

export const ARG_TYPE_URL = 'url';

export const ARG_TYPE_MOVIE = 'movie';

export const ARG_TYPE_MOVIE_WITH_SUBTITLES = 'movie-subtitles';
