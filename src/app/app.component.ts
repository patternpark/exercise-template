// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym), Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net, Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ResizedEvent } from 'angular-resize-event';
import mermaid from 'mermaid';
import { ARG_TYPE_MOVIE, ARG_TYPE_MOVIE_WITH_SUBTITLES, ARG_TYPE_URL, HEADER_TITLE, TIP_TITLE } from './constants';
import { TaskDescriptionService } from './shared/services/task-description.service';
import { GlobalEventListenerService } from './shared/services/global-event-listener.service';
import { IpcService } from './shared/services/ipc.service';

@Component({
  selector: 'patternpark-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public readonly TIP_TITLE = TIP_TITLE;
  public readonly HEADER_TITLE = HEADER_TITLE;

  public tipMessage: Observable<string>;
  public taskDescription: Observable<string>;

  constructor(
    public readonly taskDescriptionService: TaskDescriptionService,
    public readonly globalEventListenerService: GlobalEventListenerService,
    private routerService: Router,
    private ipcService: IpcService
  ) {
    this.taskDescription = this.taskDescriptionService.taskDescriptionEvent;
    this.tipMessage = this.taskDescriptionService.taskDescriptionTipEvent;

    this.ipcService.startArgsListener.subscribe((url) => {
      const [type, file, filePath2] = url;

      if (type === ARG_TYPE_URL) this.routerService.navigate([file]);
      else if (type === ARG_TYPE_MOVIE) this.routerService.navigate(['/movie', { videoPath: file }]);
      else if (type === ARG_TYPE_MOVIE_WITH_SUBTITLES)
        this.routerService.navigate(['/movie/subtitles', { videoPath: file, subtitles: filePath2 }]);
    });
  }

  onResize(event: ResizedEvent): void {
    this.globalEventListenerService.resize = event;
  }

  ngOnInit(): void {
    mermaid.initialize({
      theme: 'neutral',
      startOnLoad: false,
      securityLevel: 'loose',
    });
  }
}
