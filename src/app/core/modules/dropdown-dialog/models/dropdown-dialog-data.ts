// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Christian Hemp
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 christian.hemp[at]hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Observable } from 'rxjs';
import ClassFunction from '../../../../shared/mermaid-wrapper/class-diagram/class-function';
import { DialogData } from '../../dialog/models/dialog-data';
import { InstanceType } from './instance.type';

export interface DropdownDialogData extends DialogData {
  availableInstances: Observable<InstanceType[]>;
  method: ClassFunction;
  sourceLabelText: string;
  endLabelText: string;
}
