// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Christian Hemp
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 christian.hemp[at]hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DropdownDialogData } from './models/dropdown-dialog-data';
import { InstanceType } from './models/instance.type';

/**
 * @author Christian Hemp
 * @email christian.hemp[at]hochschule-stralsund.de
 */
@Component({
  selector: 'patternpark-dropdown-dialog',
  templateUrl: './dropdown-dialog.component.html',
  styleUrls: ['./dropdown-dialog.component.scss'],
})
export class DropdownDialogComponent {
  /**
   * The source instance that is selected by the user.
   */
  public selectedSourceInstance?: InstanceType;
  /**
   * The end instance that is selected by the user.
   */
  public selectedEndInstance?: InstanceType;

  constructor(
    public dialogRef: MatDialogRef<DropdownDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DropdownDialogData
  ) {}

  /**
   * Handles the abort click and closes the dialog.
   */
  onNoClick(): void {
    this.dialogRef.close();
  }

  /**
   * Handles when enter is pressed and checkes wether the instances are set.
   */
  onEnterKeyPressed(): void {
    if (this.selectedEndInstance && this.selectedSourceInstance && this.data.method)
      this.dialogRef.close({
        sourceInstance: this.selectedSourceInstance,
        endInstance: this.selectedEndInstance,
        method: this.data.method,
      });
  }
}
