// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Component, Input } from '@angular/core';

@Component({
  selector: 'patternpark-fieldset-box',
  templateUrl: './fieldset-box.component.html',
  styleUrls: ['./fieldset-box.component.scss'],
})
export class FieldsetBoxComponent {
  @Input()
  public legend: string;

  constructor() {
    this.legend = ' - ';
  }
}
