import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldsetBoxComponent } from './fieldset-box.component';

describe('FieldsetBoxComponent', () => {
  let component: FieldsetBoxComponent;
  let fixture: ComponentFixture<FieldsetBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FieldsetBoxComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldsetBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
