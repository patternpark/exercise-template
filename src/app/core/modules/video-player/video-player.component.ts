// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Aniketos Stamatios (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 aniketos.stamatios@protonmail.com
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IpcService } from '../../../shared/services/ipc.service';
import { ResizedEvent } from 'angular-resize-event';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { WindowService } from '../../../shared/services/window.service';
import { TemplatePortal } from '@angular/cdk/portal';

@Component({
  selector: 'patternpark-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss'],
})
export class VideoPlayerComponent implements OnInit, AfterViewInit {
  public videoPath = '';

  public autoWidth = false;

  @Input()
  public hasParent: boolean = false;

  @Input()
  public isMuted: boolean = false;

  @Output()
  public timeChanged: EventEmitter<number> = new EventEmitter<number>();

  @Output()
  public videoEnded: EventEmitter<never> = new EventEmitter<never>();

  @ViewChild('video', { static: true })
  private _videoElement: ElementRef<HTMLVideoElement> | undefined;

  @ViewChild('closeAppDialog')
  private _dialogCloseApp: TemplateRef<unknown> | undefined;

  private _overlayRef: OverlayRef | undefined;
  private _portal: TemplatePortal | undefined;

  public set playTime(val: number) {
    if (!this._videoElement?.nativeElement) return;

    this._videoElement.nativeElement.currentTime = val;
    this._videoElement.nativeElement.play();
  }

  constructor(
    private readonly _route: ActivatedRoute,
    private readonly _router: Router,
    private readonly _overlay: Overlay,
    private readonly _viewContainerRef: ViewContainerRef,
    private readonly _ipcService: IpcService,
    private readonly _windowService: WindowService
  ) {}

  ngOnInit(): void {
    this.videoPath = this._route.snapshot.paramMap.get('videoPath')!;
  }

  ngAfterViewInit(): void {
    this.videoHolderSizeChanged({
      newRect: { width: window.innerWidth, height: window.innerHeight },
    } as ResizedEvent);

    this._overlayRef = this._overlay.create({
      backdropClass: 'window-overlay-backdrop',
      positionStrategy: this._overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
  }

  onVideoEndEvent() {
    this.openCloseWindow();
  }

  onVideoFullscreenChange(): void {
    if (document.fullscreenElement) return;

    console.log('STOP FULLSCREEN');
    this._ipcService.setFullscreenOnAllVideoElements();
  }

  onTimeChange(event: Event): void {
    if (event.target instanceof HTMLVideoElement) this.timeChanged.emit(event.target.currentTime);
  }

  videoHolderSizeChanged(event: ResizedEvent): void {
    if (!this._videoElement || !this._videoElement.nativeElement) return;

    const aspectRatioVideo = this._videoElement.nativeElement.videoWidth / this._videoElement.nativeElement.videoHeight;
    const aspectRatioScreen = event.newRect.width / event.newRect.height;
    this.autoWidth = aspectRatioVideo > aspectRatioScreen;
  }

  replayVideo(): void {
    this.playTime = 0;

    this._windowService.closeWindow();
  }

  closeApp(): void {
    this._ipcService.closeApp();
  }

  private openCloseWindow(): void {
    if (!this._dialogCloseApp || !this._overlayRef) return;

    this._portal = new TemplatePortal(this._dialogCloseApp, this._viewContainerRef);
    this._windowService.overlayRef = this._overlayRef;
    this._overlayRef.attach(this._portal);
  }
}
