// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Christian Hemp
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 christian.hemp[at]hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { INSTANCE_TIP } from './constants';
import { DialogData } from './models/dialog-data';

/**
 * @author Christian Hemp
 * @email christian.hemp[at]hochschule-stralsund.de
 */
@Component({
  selector: 'patternpark-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
})
export class DialogComponent {
  /**
   * Formgroup that holds all input fields in this dialog.
   */
  public formGroup!: FormGroup;
  /**
   * The tip that is displayed when the name does not match the pattern.
   */
  public tip: string = INSTANCE_TIP;
  constructor(public dialogRef: MatDialogRef<DialogComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this._initFormGroup();
  }

  /**
   * Handles the abort click and closes the dialog.
   */
  onNoClick(): void {
    this.dialogRef.close();
  }

  /**
   * Handles the enter pressed event on the input.
   */
  onEnterKeyPressed() {
    if (this.data && this.formGroup.valid) this.dialogRef.close(this.data);
  }

  /**
   * Creates a new formgroup and adds the inputs, applies a pattern for validation and marks it as touched.
   */
  private _initFormGroup(): void {
    this.formGroup = new FormGroup({
      instanceName: new FormControl(['', Validators.pattern('^[a-zA-Z0-9_]+$'), Validators.required]),
    });
    this.formGroup.get('instanceName')?.markAsTouched();
  }
}
