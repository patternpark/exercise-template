// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Christian Hemp, Aniketos Stamatios (Pseudonym), Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 christian.hemp[at]hochschule-stralsund.de, aniketos.stamatios@protonmail.com, contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoxComponent } from './box/box.component';
import { ContentBoxComponent } from './content-box/content-box.component';
import { DisableDirective } from '../directives/disable.directive';
import { DialogComponent } from './dialog/dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { DropdownDialogComponent } from './dropdown-dialog/dropdown-dialog.component';
import { MatSelectModule } from '@angular/material/select';
import { FieldsetBoxComponent } from './fieldset-box/fieldset-box.component';
import { VideoPlayerComponent } from './video-player/video-player.component';
import { AngularResizeEventModule } from 'angular-resize-event';
import { VideoPlayerWithSubtitlesComponent } from './video-player-with-subtitles/video-player-with-subtitles.component';
import { SubtitlesBoxComponent } from './video-player-with-subtitles/subtitles-box/subtitles-box.component';
import { OldStyleDialogComponent } from './old-style-dialog/old-style-dialog.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { OldStyleButtonComponent } from './old-style-button/old-style-button.component';

@NgModule({
  declarations: [
    BoxComponent,
    ContentBoxComponent,
    DisableDirective,
    DialogComponent,
    DropdownDialogComponent,
    FieldsetBoxComponent,
    VideoPlayerComponent,
    VideoPlayerWithSubtitlesComponent,
    SubtitlesBoxComponent,
    OldStyleDialogComponent,
    OldStyleButtonComponent,
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    AngularResizeEventModule,
    DragDropModule,
  ],
  exports: [
    BoxComponent,
    ContentBoxComponent,
    DisableDirective,
    FieldsetBoxComponent,
    VideoPlayerComponent,
    VideoPlayerWithSubtitlesComponent,
    OldStyleDialogComponent,
    OldStyleButtonComponent,
  ],
})
export class UiComponentsModule {}
