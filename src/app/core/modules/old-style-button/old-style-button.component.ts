// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Component } from '@angular/core';

@Component({
  selector: 'patternpark-old-style-button',
  templateUrl: './old-style-button.component.html',
  styleUrls: ['./old-style-button.component.scss'],
})
export class OldStyleButtonComponent {}
