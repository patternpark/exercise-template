import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OldStyleButtonComponent } from './old-style-button.component';

describe('OldStyleButtonComponent', () => {
  let component: OldStyleButtonComponent;
  let fixture: ComponentFixture<OldStyleButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OldStyleButtonComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OldStyleButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
