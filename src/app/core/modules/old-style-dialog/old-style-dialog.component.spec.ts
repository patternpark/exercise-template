import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OldStyleDialogComponent } from './old-style-dialog.component';

describe('OldStyleDialogComponent', () => {
  let component: OldStyleDialogComponent;
  let fixture: ComponentFixture<OldStyleDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OldStyleDialogComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OldStyleDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
