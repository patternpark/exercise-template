// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'patternpark-old-style-dialog',
  templateUrl: './old-style-dialog.component.html',
  styleUrls: ['./old-style-dialog.component.scss'],
})
export class OldStyleDialogComponent {
  @Input()
  public title = '';

  @Input()
  public leftButton = 'Abbruch';

  @Input()
  public rightButton = 'Okay';

  @Output()
  public leftButtonClicked: EventEmitter<never> = new EventEmitter<never>();

  @Output()
  public rightButtonClicked: EventEmitter<never> = new EventEmitter<never>();
}
