import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoPlayerWithSubtitlesComponent } from './video-player-with-subtitles.component';

describe('VideoPlayerWithSubtitlesComponent', () => {
  let component: VideoPlayerWithSubtitlesComponent;
  let fixture: ComponentFixture<VideoPlayerWithSubtitlesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VideoPlayerWithSubtitlesComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoPlayerWithSubtitlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
