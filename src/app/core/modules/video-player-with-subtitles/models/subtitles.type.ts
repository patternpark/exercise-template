// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

/**
 * Represents a list of subtitles that will be displayed at any given time.
 *
 * The key represents the second from when the text (the value) should be
 * displayed. This is displayed until the next element "overwrites" the previous
 * element.
 */
export type Subtitles = {
  [key: number]: string;
};
