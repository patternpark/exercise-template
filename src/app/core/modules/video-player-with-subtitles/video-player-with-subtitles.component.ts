// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Component } from '@angular/core';
import { IpcService } from '../../../shared/services/ipc.service';
import { ActivatedRoute } from '@angular/router';
import { Subtitles } from './models/subtitles.type';

@Component({
  selector: 'patternpark-video-player-with-subtitles',
  templateUrl: './video-player-with-subtitles.component.html',
  styleUrls: ['./video-player-with-subtitles.component.scss'],
})
export class VideoPlayerWithSubtitlesComponent {
  private _subtitles: Subtitles = {};
  private _currentSecondKey = 0;

  public currentText: string = '';

  constructor(private readonly _route: ActivatedRoute, private readonly _ipcService: IpcService) {
    this._ipcService
      .getJSONFile<Subtitles>(this._route.snapshot.paramMap.get('subtitles')!)
      .then((result) => (this._subtitles = result));
  }

  setSubtitle(seconds: number): void {
    seconds = Math.trunc(seconds);
    let lastStartSecond = -1;
    for (const startSecond of Object.keys(this._subtitles).map((n) => parseInt(n))) {
      if (startSecond > seconds) break;

      if (startSecond === seconds) {
        lastStartSecond = seconds;
        break;
      }

      lastStartSecond = startSecond;
    }

    if (this._currentSecondKey === lastStartSecond) return;

    this._currentSecondKey = lastStartSecond;
    if (this._currentSecondKey < 0) this.currentText = '';
    else this.currentText = this._subtitles[this._currentSecondKey];
  }
}
