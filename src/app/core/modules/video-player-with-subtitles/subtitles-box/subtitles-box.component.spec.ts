import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubtitlesBoxComponent } from './subtitles-box.component';

describe('SubtitlesComponent', () => {
  let component: SubtitlesBoxComponent;
  let fixture: ComponentFixture<SubtitlesBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SubtitlesBoxComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubtitlesBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
