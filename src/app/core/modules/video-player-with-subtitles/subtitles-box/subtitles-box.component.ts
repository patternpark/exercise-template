// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Component } from '@angular/core';

@Component({
  selector: 'patternpark-subtitles-box',
  templateUrl: './subtitles-box.component.html',
  styleUrls: ['./subtitles-box.component.scss'],
})
export class SubtitlesBoxComponent {}
