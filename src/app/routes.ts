// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                  - Alle -
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 -
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Routes } from '@angular/router';
import { NotFoundComponent } from './modules/not-found/not-found.component';
import { AdapterTaskWithUmlComponent } from './modules/adapter/adapter-task-with-uml/adapter-task-with-uml.component';
import { AdapterTaskWithoutUmlComponent } from './modules/adapter/adapter-task-without-uml/adapter-task-without-uml.component';
import { TaskWithUmlComponent } from './modules/factory-method/task-with-uml/task-with-uml.component';
import { TaskPage1Component } from './modules/factory-method/task-without-uml/task-page1/task-page1.component';
import { TaskPage2Component } from './modules/factory-method/task-without-uml/task-page2/task-page2.component';
import { VideoPlayerComponent } from './core/modules/video-player/video-player.component';
import { VideoPlayerWithSubtitlesComponent } from './core/modules/video-player-with-subtitles/video-player-with-subtitles.component';
import { WithoutUmlComponent } from './modules/singleton/without-uml/without-uml.component';
import { BridgeTaskWithoutUmlComponent } from './modules/bridge/bridge-task-without-uml/bridge-task-without-uml.component';
import { BridgeTaskWithUmlComponent } from './modules/bridge/bridge-task-with-uml/bridge-task-with-uml.component';
import { StrategyTaskWithUmlComponent } from './modules/strategy/strategy-with-uml/strategy-task-with-uml.component';
import { StrategyTaskWithoutUmlComponent } from './modules/strategy/strategy-task-without-uml/strategy-task-without-uml.component';
import { VisitorTaskWithUmlComponent } from './modules/visitor/visitor-task-with-uml/visitor-task-with-uml.component';
import { VisitorTaskWithoutUmlComponent } from './modules/visitor/visitor-task-without-uml/visitor-task-without-uml.component';
import { MvcTaskWithUmlComponent } from './modules/mvc/mvc-task-with-uml/mvc-task-with-uml.component';
import { MvcTaskWithoutUmlComponent } from './modules/mvc/mvc-task-without-uml/mvc-task-without-uml.component';
import { MementoTaskWithUmlComponent } from './modules/memento/memento-task-with-uml/memento-task-with-uml.component';
import { MementoTaskWithoutUmlComponent } from './modules/memento/memento-task-without-uml/memento-task-without-uml.component';

export const ROUTES: Routes = [
  { path: 'movie', component: VideoPlayerComponent },
  { path: 'movie/subtitles', component: VideoPlayerWithSubtitlesComponent },
  {
    path: 'factory-method',
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'without-uml/task1' },
      { path: 'uml/task1', component: TaskWithUmlComponent },
      {
        path: 'without-uml/task1',
        component: TaskPage1Component,
      },
      { path: 'without-uml/task2', component: TaskPage2Component },
    ],
  },
  {
    path: 'adapter',
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'without-uml' },
      { path: 'uml', component: AdapterTaskWithUmlComponent },
      {
        path: 'without-uml',
        component: AdapterTaskWithoutUmlComponent,
      },
    ],
  },
  {
    path: 'singleton',
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'without-uml' },
      { path: 'without-uml', component: WithoutUmlComponent },
    ],
  },
  {
    path: 'bridge',
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'without-uml/task1' },
      { path: 'uml/task1', component: BridgeTaskWithUmlComponent },
      {
        path: 'without-uml/task1',
        component: BridgeTaskWithoutUmlComponent,
      },
    ],
  },
  {
    path: 'strategy',
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'without-uml/task1' },
      { path: 'uml/task1', component: StrategyTaskWithUmlComponent },
      { path: 'without-uml/task1', component: StrategyTaskWithoutUmlComponent },
    ],
  },
  {
    path: 'visitor',
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'without-uml' },
      { path: 'uml', component: VisitorTaskWithUmlComponent },
      { path: 'without-uml', component: VisitorTaskWithoutUmlComponent },
    ],
  },
  {
    path: 'mvc',
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'without-uml/task1' },
      { path: 'uml/task1', component: MvcTaskWithUmlComponent },
      { path: 'without-uml/task1', component: MvcTaskWithoutUmlComponent },
    ],
  },
  {
    path: 'memento',
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'without-uml/task1' },
      { path: 'uml/task1', component: MementoTaskWithUmlComponent },
      { path: 'without-uml/task1', component: MementoTaskWithoutUmlComponent },
    ],
  },
  { path: '**', component: NotFoundComponent },
];
