// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AngularResizeEventModule } from 'angular-resize-event';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderIconComponent } from './modules/header-icon/header-icon.component';
import { NotFoundComponent } from './modules/not-found/not-found.component';
import { FactoryMethodModule } from './modules/factory-method/factory-method.module';
import { UiComponentsModule } from './core/modules/ui-components.module';
import { AdapterModule } from './modules/adapter/adapter.module';
import { SingletonModule } from './modules/singleton/singleton.module';
import { BridgeModule } from './modules/bridge/bridge.module';
import { StrategyModule } from './modules/strategy/strategy.module';
import { MvcModule } from './modules/mvc/mvc.module';
import { VisitorModule } from './modules/visitor/visitor.module';

@NgModule({
  declarations: [AppComponent, HeaderIconComponent, NotFoundComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FactoryMethodModule,
    AdapterModule,
    UiComponentsModule,
    AngularResizeEventModule,
    SingletonModule,
    BridgeModule,
    StrategyModule,
    MvcModule,
    VisitorModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
