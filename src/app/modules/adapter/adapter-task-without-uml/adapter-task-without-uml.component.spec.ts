import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdapterTaskWithoutUmlComponent } from './adapter-task-without-uml.component';

describe('AdapterTaskWithoutUmlComponent', () => {
  let component: AdapterTaskWithoutUmlComponent;
  let fixture: ComponentFixture<AdapterTaskWithoutUmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdapterTaskWithoutUmlComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdapterTaskWithoutUmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
