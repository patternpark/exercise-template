import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TaskDescriptionService } from '../../../../shared/services/task-description.service';
import { WRONG_EXAMPLE, WRONG_ROLE } from '../constants';
import { EXAMPLE } from '../example';
import { Item } from '../item';
import { ROLE } from '../role';

@Component({
  selector: 'patternpark-adapter-example',
  templateUrl: './adapter-example.component.html',
  styleUrls: ['./adapter-example.component.scss'],
})
export class AdapterExampleComponent {
  @Input()
  public example: EXAMPLE = EXAMPLE.NONE;

  public readonly EXAMPLE = EXAMPLE;
  public readonly ROLE = ROLE;

  public adapter: Item[] = [];
  public adapterImpl: Item[] = [];
  public client: Item[] = [];
  public target: Item[] = [];

  @Output() public taskDone = new EventEmitter<boolean>();

  constructor(private _taskDescService: TaskDescriptionService) {}

  drop(event: CdkDragDrop<Item[]>, example: EXAMPLE, role: ROLE): void {
    const itemExample = event.previousContainer.data[event.previousIndex].example;
    const itemRole = event.previousContainer.data[event.previousIndex].role;

    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else if (itemExample !== example) {
      this._taskDescService.taskDescriptionTip = WRONG_EXAMPLE;
    } else {
      if (itemRole !== role) {
        this._taskDescService.taskDescriptionTip = WRONG_ROLE;
      } else {
        transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
        this.checkIfComplete();
      }
    }
  }

  private checkIfComplete = () => {
    const isTaskComplete =
      this.adapter.length > 0 && this.adapterImpl.length > 0 && this.client.length > 0 && this.target.length > 0;

    this.taskDone.emit(isTaskComplete);
  };
}
