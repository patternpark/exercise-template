import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdapterExampleComponent } from './adapter-example.component';

describe('AdapterExampleComponent', () => {
  let component: AdapterExampleComponent;
  let fixture: ComponentFixture<AdapterExampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdapterExampleComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdapterExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
