import { EXAMPLE } from './example';
import { ROLE } from './role';

export class Item {
  protected _name: string;
  protected _example: EXAMPLE;
  protected _role: ROLE;

  public constructor(name: string, example: EXAMPLE, role: ROLE) {
    this._name = name;
    this._example = example;
    this._role = role;
  }

  public get name(): string {
    return this._name;
  }

  public get example(): EXAMPLE {
    return this._example;
  }

  public get role(): ROLE {
    return this._role;
  }
}
