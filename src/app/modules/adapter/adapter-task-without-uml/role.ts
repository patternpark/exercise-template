export enum ROLE {
  NONE = '',
  ADAPTER = 'Adapter',
  ADAPTER_IMPL = 'Konkreter Adapter',
  CLIENT = 'Klient',
  TARGET = 'Dienst',
}
