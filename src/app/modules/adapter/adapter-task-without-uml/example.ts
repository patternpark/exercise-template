export enum EXAMPLE {
  NONE = '',
  POWER_OUTLET = 'Du möchtest ein Elektrogerät aus Deutschland in den USA nutzen.',
  LANGUAGES = 'Du sprichst nur Deutsch, möchtest dich aber mit jemandem unterhalten, der nur Spanisch spricht.',
  SOFTWARE = 'Du möchtest zwei bestehende Softwaresysteme mit verschiedenen Interfaces verbinden.',
}
