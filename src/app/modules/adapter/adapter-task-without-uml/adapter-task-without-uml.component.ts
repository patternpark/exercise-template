import { Component, OnInit } from '@angular/core';
import { TaskDescriptionService } from '../../../shared/services/task-description.service';
import { shuffle } from '../../../shared/utilities/array-utility';
import {
  ADAPTER_TASK_WITHOUT_UML,
  CLASS_ADAPTER,
  COMPLETE,
  CONCRETE_PLUG_ADAPTER,
  CONCRETE_TRANSLATOR,
  EUROPEAN_POWER_OUTLET,
  GERMAN_PERSON,
  INITIAL_HINT,
  INTERFACE_ADAPTER,
  PLUG_ADAPTER,
  SOFTWARESYSTEM_1,
  SOFTWARESYSTEM_2,
  SPANISH_PERSON,
  TRANSLATOR,
  US_POWER_OUTLET,
} from './constants';
import { EXAMPLE } from './example';
import { Item } from './item';
import { ROLE } from './role';

@Component({
  selector: 'patternpark-adapter-task-without-uml',
  templateUrl: './adapter-task-without-uml.component.html',
  styleUrls: ['./adapter-task-without-uml.component.scss', '../../../shared/styles/cdk-prettify.scss'],
})
export class AdapterTaskWithoutUmlComponent implements OnInit {
  private _numberOfTasks = Object.keys(EXAMPLE).length - 1;
  private _numberOfCompleteTasks = 0;

  public allItems: Item[] = [
    new Item(EUROPEAN_POWER_OUTLET, EXAMPLE.POWER_OUTLET, ROLE.CLIENT),
    new Item(PLUG_ADAPTER, EXAMPLE.POWER_OUTLET, ROLE.ADAPTER),
    new Item(CONCRETE_PLUG_ADAPTER, EXAMPLE.POWER_OUTLET, ROLE.ADAPTER_IMPL),
    new Item(US_POWER_OUTLET, EXAMPLE.POWER_OUTLET, ROLE.TARGET),
    new Item(SOFTWARESYSTEM_1, EXAMPLE.SOFTWARE, ROLE.CLIENT),
    new Item(INTERFACE_ADAPTER, EXAMPLE.SOFTWARE, ROLE.ADAPTER),
    new Item(CLASS_ADAPTER, EXAMPLE.SOFTWARE, ROLE.ADAPTER_IMPL),
    new Item(SOFTWARESYSTEM_2, EXAMPLE.SOFTWARE, ROLE.TARGET),
    new Item(GERMAN_PERSON, EXAMPLE.LANGUAGES, ROLE.CLIENT),
    new Item(TRANSLATOR, EXAMPLE.LANGUAGES, ROLE.ADAPTER),
    new Item(CONCRETE_TRANSLATOR, EXAMPLE.LANGUAGES, ROLE.ADAPTER_IMPL),
    new Item(SPANISH_PERSON, EXAMPLE.LANGUAGES, ROLE.TARGET),
  ];

  public readonly EXAMPLE = EXAMPLE;

  constructor(private _taskDescService: TaskDescriptionService) {
    _taskDescService.taskDescription = ADAPTER_TASK_WITHOUT_UML;
    _taskDescService.taskDescriptionTip = INITIAL_HINT;
    _taskDescService.navigationVisibility = false;
  }
  ngOnInit(): void {
    shuffle(this.allItems);
  }

  public checkIfTaskComplete = (isTaskComplete: boolean) => {
    if (isTaskComplete) {
      this._numberOfCompleteTasks++;
    }
    if (this._numberOfCompleteTasks === this._numberOfTasks) {
      this._taskDescService.taskDescriptionTip = COMPLETE;
    }
  };
}
