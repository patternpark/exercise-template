export const ADAPTER_TASK_WITHOUT_UML =
  'Ordne die Bausteine des Adapter-Patterns ihrer korrekten Rolle im passenden Problem zu.';
export const INITIAL_HINT = 'Du kannst die Bausteine per Drag & Drop bewegen.';
export const WRONG_ROLE = 'Das ist zwar das richtige Problem aber die falsche Rolle für diesen Baustein.';
export const WRONG_EXAMPLE = 'Das ist das falsche Problem für diesen Baustein.';
export const CORRECT = 'Richtig!';
export const COMPLETE = 'Glückwunsch, du hast die Aufgabe vollständig gelöst!';

export const EUROPEAN_POWER_OUTLET = 'Deutscher Stecker';
export const PLUG_ADAPTER = 'Stromadapter';
export const CONCRETE_PLUG_ADAPTER = 'Stromadapter von deutschem System zu US-System';
export const US_POWER_OUTLET = 'Amerikanische Steckdose';

export const GERMAN_PERSON = 'Deutsche Person';
export const TRANSLATOR = 'Übersetzer';
export const CONCRETE_TRANSLATOR = 'Übersetzer von Deutsch zu Spanisch';
export const SPANISH_PERSON = 'Spanische Person';

export const SOFTWARESYSTEM_1 = 'Softwaresystem 1';
export const INTERFACE_ADAPTER = 'Interface Adapter';
export const CLASS_ADAPTER = 'Klasse Adapter';
export const SOFTWARESYSTEM_2 = 'Softwaresystem 2';
