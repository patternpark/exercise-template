export class Answer {
  protected _text: string;
  protected _reference: string;

  public constructor(text: string, reference: string) {
    this._text = text;
    this._reference = reference;
  }

  public get text(): string {
    return this._text;
  }

  public get reference(): string {
    return this._reference;
  }
}
