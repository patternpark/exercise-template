import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { TaskDescriptionService } from '../../../shared/services/task-description.service';
import {
  ACTOR_ANSWER_BUTTONS,
  ACTOR_ANSWER_WRONG_NAME,
  ACTORS,
  ADAPTER,
  ADAPTER_TASK_WITH_UML,
  ADAPTER_TO_CLIENT,
  CLIENT_TO_ADAPTER,
  CLIENT_TO_ITERATOR,
  COMPLETE,
  CONCRETE_OPERATION,
  CONCRETE_OPERATION_CORRECT_ARROW,
  CONCRETE_OPERATION_WRONG_ARROW,
  CONVERTED_RESULTS,
  CORRECT,
  CORRECT_SEQUENCE_DIAGRAM,
  INITIAL_HINT,
  ITERATOR,
  ITERATOR_TO_CLIENT,
  MESSAGE_ANSWER_BUTTONS,
  MESSAGE_ANSWER_WRONG_DIRECTION,
  MESSAGE_ANSWER_WRONG_TYPE,
  MESSAGES,
  WRONG,
  WRONG_SEQUENCE_DIAGRAM,
} from './constants';
import mermaid from 'mermaid';
import { Answer } from './answer';
import { replaceAll } from '../../../shared/utilities/string-utility';

@Component({
  selector: 'patternpark-adapter-task-with-uml',
  templateUrl: './adapter-task-with-uml.component.html',
  styleUrls: ['./adapter-task-with-uml.component.scss'],
})
export class AdapterTaskWithUmlComponent implements AfterViewInit {
  @ViewChild('sequenceDiagramAdapter', { static: true })
  public mermaidDiv?: ElementRef<HTMLDivElement>;
  public answerButtons: Answer[] = [];

  private _diagramDefinition = WRONG_SEQUENCE_DIAGRAM;
  private _mermaidElement!: HTMLElement;
  private _isAdapterSet: boolean = false;

  constructor(private _taskDescService: TaskDescriptionService) {
    _taskDescService.taskDescription = ADAPTER_TASK_WITH_UML;
    _taskDescService.navigationVisibility = false;
    _taskDescService.taskDescriptionTip = INITIAL_HINT;
  }

  ngAfterViewInit(): void {
    if (!this.mermaidDiv) return;

    mermaid.init('.mermaid');
    this._mermaidElement = this.mermaidDiv?.nativeElement;
    this.rerender();

    this._taskDescService.taskDescriptionTip = INITIAL_HINT;
  }

  public checkAnswer = (answer: Answer) => {
    switch (answer.reference) {
      case ITERATOR: {
        this.checkIteratorAnswer(answer);
        break;
      }
      case CONCRETE_OPERATION: {
        this.checkConcreteOperationsAnswer(answer);
        break;
      }
      case CONVERTED_RESULTS: {
        this.checkConvertedResultsAnswer(answer);
        break;
      }
      default: {
        this.wrongAnswer();
      }
    }
  };

  private checkIteratorAnswer = (answer: Answer) => {
    if (answer.text === ACTOR_ANSWER_WRONG_NAME) {
      this._diagramDefinition = replaceAll(this._diagramDefinition, ITERATOR, ADAPTER);
      this._isAdapterSet = true;
      this.rerender();
    } else {
      this.wrongAnswer();
    }
  };

  private checkConcreteOperationsAnswer = (answer: Answer) => {
    if (answer.text === MESSAGE_ANSWER_WRONG_TYPE) {
      this._diagramDefinition = this._diagramDefinition.replace(
        CONCRETE_OPERATION_WRONG_ARROW,
        CONCRETE_OPERATION_CORRECT_ARROW
      );

      this.rerender();
    } else {
      this.wrongAnswer();
    }
  };

  private checkConvertedResultsAnswer = (answer: Answer) => {
    if (answer.text === MESSAGE_ANSWER_WRONG_DIRECTION) {
      if (this._isAdapterSet) {
        this._diagramDefinition = this._diagramDefinition.replace(CLIENT_TO_ADAPTER, ADAPTER_TO_CLIENT);
      } else {
        this._diagramDefinition = this._diagramDefinition.replace(CLIENT_TO_ITERATOR, ITERATOR_TO_CLIENT);
      }
      this.rerender();
    } else {
      this.wrongAnswer();
    }
  };

  private wrongAnswer = () => {
    this._taskDescService.taskDescriptionTip = WRONG;
    this.answerButtons = [];
  };

  private checkIfComplete = () => {
    if (replaceAll(this._diagramDefinition, '/s/g', '') === replaceAll(CORRECT_SEQUENCE_DIAGRAM, '/s/g', '')) {
      this._taskDescService.taskDescriptionTip = COMPLETE;
    } else {
      this._taskDescService.taskDescriptionTip = CORRECT;
    }
  };

  private rerender() {
    this.answerButtons = [];
    this.checkIfComplete();
    mermaid.render('sequenceDiagramAdapter', this._diagramDefinition, (svgCode: string) => {
      this.setClickHandlers(svgCode);
    });
  }

  private renderAnswerButtons = (name: string) => {
    if (ACTORS.includes(name)) {
      this.answerButtons = ACTOR_ANSWER_BUTTONS.map((answerText) => new Answer(answerText, name));
    } else if (MESSAGES.includes(name)) {
      this.answerButtons = MESSAGE_ANSWER_BUTTONS.map((answerText) => new Answer(answerText, name));
    }
  };

  private setClickHandlers = (svgCode: string) => {
    this._mermaidElement.innerHTML = svgCode;
    this.setClickHandlersForActors();
    this.setClickHandlersForMessages();
  };

  private setClickHandlersForMessages = () => {
    const allMessageTexts = this._mermaidElement.querySelectorAll('text.messageText');

    for (const messageElement of Array.from(allMessageTexts)) {
      let message = 'None';
      const arrow = messageElement.nextElementSibling;

      if (messageElement.textContent !== null) {
        message = messageElement.textContent;
      }

      messageElement.addEventListener('click', () => {
        this.renderAnswerButtons(message);
      });
      messageElement.classList.add('pointable');

      arrow?.addEventListener('click', () => {
        this.renderAnswerButtons(message);
      });
      arrow?.classList.add('pointable');
    }
  };

  private setClickHandlersForActors = () => {
    const allActorNames = this._mermaidElement.querySelectorAll('rect.actor + text.actor');

    for (const selElement of Array.from(allActorNames)) {
      let actorName = 'None';
      const group = selElement.parentElement;

      const rectAndText = group?.querySelectorAll('.actor');

      if (rectAndText !== undefined && rectAndText[1].textContent !== null) {
        actorName = rectAndText[1].textContent;
      }

      rectAndText?.forEach((element) => {
        element.classList.add('pointable');
        element.addEventListener('click', () => {
          this.renderAnswerButtons(actorName);
        });
      });
    }
  };
}
