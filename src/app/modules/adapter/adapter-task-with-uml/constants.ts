export const ADAPTER_TASK_WITH_UML = `Leider hat das Sequenzdiagramm für das Adapter-Pattern unten einige Fehler.
Klicke auf die falschen Stellen und wähle dann den passenden Fehler aus, um das Diagramm zu korrigieren.`;

export const INITIAL_HINT = 'Du kannst Objekte und Nachrichten anklicken.';

export const ACTOR_ANSWER_WRONG_NAME = 'Dieses Objekt ist falsch benannt';
const ACTOR_ANSWER_WRONG_ORDER = 'Dieses Objekt ist an der falschen Position';
const ACTOR_ANSWER_WRONG_CALL = 'Dieses Objekt wird falsch aufgerufen';
export const ACTOR_ANSWER_BUTTONS = [ACTOR_ANSWER_WRONG_NAME, ACTOR_ANSWER_WRONG_ORDER, ACTOR_ANSWER_WRONG_CALL];

export const MESSAGE_ANSWER_WRONG_DIRECTION = 'Diese Nachricht ist verkehrt herum';
export const MESSAGE_ANSWER_WRONG_TYPE = 'Diese Nachricht ist vom falschen Typ (Aufruf/Rückkehr)';
const MESSAGE_ANSWER_WRONG_OBJECT = 'Diese Nachricht zeigt auf das falsche Objekt';
export const MESSAGE_ANSWER_BUTTONS = [
  MESSAGE_ANSWER_WRONG_DIRECTION,
  MESSAGE_ANSWER_WRONG_TYPE,
  MESSAGE_ANSWER_WRONG_OBJECT,
];

export const CORRECT = 'Richtig!';
export const WRONG = 'Das ist leider falsch.';
export const COMPLETE = 'Glückwunsch, du hast die Aufgabe vollständig gelöst!';

export const ADAPTER = 'Adapter';
export const ITERATOR = 'Iterator';
export const CONCRETE_OPERATION = 'konkreteOperation()';
export const CONVERTED_RESULTS = 'Umgewandelte Ergebnisse';

export const ACTORS = [ITERATOR, ADAPTER, 'Klient', 'Ziel'];
export const MESSAGES = ['operation()', CONCRETE_OPERATION, 'Ergebnisse', CONVERTED_RESULTS];

export const ADAPTER_TO_CLIENT = '\nAdapter-->>Klient:';
export const ITERATOR_TO_CLIENT = '\nIterator-->>Klient:';
export const CLIENT_TO_ADAPTER = '\nKlient-->>Adapter:';
export const CLIENT_TO_ITERATOR = '\nKlient-->>Iterator:';

export const CONCRETE_OPERATION_WRONG_ARROW = '-->>Ziel: konkreteOperation()';
export const CONCRETE_OPERATION_CORRECT_ARROW = '->>Ziel: konkreteOperation()';

export const CORRECT_SEQUENCE_DIAGRAM =
  'sequenceDiagram\nactivate Klient\nKlient->>Adapter: operation()\ndeactivate Klient\nactivate Adapter\nNote right of Adapter: Parameter umwandeln\nAdapter->>Adapter: \nAdapter->>Ziel: konkreteOperation()\ndeactivate Adapter\nactivate Ziel\nZiel-->>Adapter: Ergebnisse\ndeactivate Ziel\nactivate Adapter\nNote right of Adapter: Ergebnisse umwandeln\nAdapter->>Adapter: \nAdapter-->>Klient: Umgewandelte Ergebnisse \ndeactivate Adapter\nactivate Klient\ndeactivate Klient';

export const WRONG_SEQUENCE_DIAGRAM =
  'sequenceDiagram\nactivate Klient\nKlient->>Iterator: operation()\ndeactivate Klient\nactivate Iterator\nNote right of Iterator: Parameter umwandeln\nIterator->>Iterator: \nIterator-->>Ziel: konkreteOperation()\ndeactivate Iterator\nactivate Ziel\nZiel-->>Iterator: Ergebnisse\ndeactivate Ziel\nactivate Iterator\nNote right of Iterator: Ergebnisse umwandeln\nIterator->>Iterator: \nKlient-->>Iterator: Umgewandelte Ergebnisse \ndeactivate Iterator\nactivate Klient\ndeactivate Klient';
