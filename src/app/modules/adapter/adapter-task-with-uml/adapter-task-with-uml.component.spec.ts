import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdapterTaskWithUmlComponent } from './adapter-task-with-uml.component';

describe('AdapterTaskWithUmlComponent', () => {
  let component: AdapterTaskWithUmlComponent;
  let fixture: ComponentFixture<AdapterTaskWithUmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdapterTaskWithUmlComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdapterTaskWithUmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
