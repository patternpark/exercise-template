import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdapterTaskWithUmlComponent } from './adapter-task-with-uml/adapter-task-with-uml.component';
import { AdapterTaskWithoutUmlComponent } from './adapter-task-without-uml/adapter-task-without-uml.component';
import { UiComponentsModule } from '../../core/modules/ui-components.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { AdapterExampleComponent } from './adapter-task-without-uml/adapter-example/adapter-example.component';

@NgModule({
  declarations: [AdapterTaskWithUmlComponent, AdapterTaskWithoutUmlComponent, AdapterExampleComponent],
  imports: [CommonModule, UiComponentsModule, DragDropModule],
  exports: [AdapterTaskWithoutUmlComponent, AdapterTaskWithUmlComponent],
})
export class AdapterModule {}
