// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Softwareprojektorganisation (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer, Christian Hemp, Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de, Christian.Hemp [at] hochschule-stralsund.de,
//                           contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VisitorTaskWithUmlComponent } from './visitor-task-with-uml/visitor-task-with-uml.component';
import { VisitorTaskWithoutUmlComponent } from './visitor-task-without-uml/visitor-task-without-uml.component';
import { UiComponentsModule } from '../../core/modules/ui-components.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { VisitorMethodParameterDropDownComponent } from './visitor-task-without-uml/components/visitor-method-parameter-drop-down/visitor-method-parameter-drop-down.component';

@NgModule({
  declarations: [VisitorTaskWithUmlComponent, VisitorTaskWithoutUmlComponent, VisitorMethodParameterDropDownComponent],
  imports: [CommonModule, UiComponentsModule, MatFormFieldModule, MatInputModule, MatSelectModule],
})
export class VisitorModule {}
