// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Projekt Organisation (Wintersemester 2021/22)
//
//  Authors:                 Christian Hemp
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 christian.hemp[at]hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

/**
 * The tip that describes how to handle the task.
 */
export const TASK_TIP = 'Klicke auf die Nummer eines Aufrufs und wähle die richtige Bezeichnung des Aufrufs aus.';
/**
 * Message that is shown when the user has all calls correct.
 */
export const SUCCESS_MESSAGE = 'Glückwunsch die Aufgabe wurde erfolgreich gelöst.';
/**
 * A text that summarizes the task with UML.
 */
export const TASK_DESCRIPTION =
  'In dieser Aufgabe geht es darum, dem Seqenzdiagramm die richtigen Methodenbezeichnungen zu geben. Klicke dazu auf die nummerierten Aufrufe und wähle den passenden Namen der Methode aus.';
/**
 * The title that is shown in the fieldset-box
 */
export const SEQUENCE_LEGEND = 'Sequenzdiagramm';
/**
 * The needed diagramId.
 */
export const DIAGRAM_ID = 'visitorSeqId';
/**
 * The string that represents the sequence diagram in mermaid.
 */
export const DIAGRAM_DEFINITION =
  '%%{init: {"theme": "neutral", "themeVariables": { "signalTextColor": "#00f"}}}%%\nsequenceDiagram\nGeisterschloss->>+Besucher: [1]\nGeisterschloss->>Geisterschloss: [2]\nGeisterschloss->>+Zombie: [3]\nZombie-->>Geisterschloss: \nGeisterschloss->>+Geist:[4]\nGeist-->>Geisterschloss: \nGeisterschloss->>+Skelett:[5]\nSkelett-->>Geisterschloss: \nGeisterschloss->>Zombie: [6]\nZombie->>Besucher: [7]\nBesucher-->>Zombie: \nZombie->>Besucher:[8]\nBesucher-->>Zombie: \nZombie-->>-Geisterschloss: \nGeisterschloss->>Geist:[9]\nGeist->>Besucher:[10]\nBesucher-->>Geist: \nGeist->>Besucher:[11]\nGeist-->>Besucher: \nGeist-->>-Geisterschloss: \nGeisterschloss->>Skelett:[12]\nSkelett->>Besucher:[13]\nBesucher-->>Skelett: \nSkelett->>Besucher:[14]\nBesucher-->>Skelett: \nSkelett-->>-Geisterschloss: \nBesucher-->>-Geisterschloss: ';
/**
 * The message that is shown when the user is correct.
 */
export const CORRECT = 'Richtig! Weiter so!';
/**
 * The message that is shown when the user is incorrect.
 */
export const FALSE = 'Leider falsch. Probier es weiter.';
/**
 * Names that are available to choose from.
 */
export const START_TOUR = 'starteTour():void';
export const ERSCHAFFE_GEIST = 'erschaffeGeist():Geist';
export const BESUCHE_SKELETT = 'besuche(Skelett):void';
export const ERSCHAFFE_KREATUREN = 'erschaffeKreaturen():void';
export const REAGIERE_AUF_ZOMBIE = 'reagiereAuf(Zombie):void';
export const REAGIERE_AUF_GEIST = 'reagiereAuf(Geist):void';
export const REAGIERE_AUF_SKELETT = 'reagiereAuf(Selett):void';
export const BESUCHE_ZOMBIE = 'besuche(Zombie):void';
export const ERSCHAFFE_SKELETT = 'erschaffeSkelett():Skelett';
export const ERSCHAFFE_ZOMBIE = 'erschaffeZombie():Zombie';
export const BESUCHE_GEIST = 'besuche(Geist):void';
export const ACCEPT_VISITOR = 'akzeptiere(Besucher):void';
/**
 * Array that contains all available method names.
 */
export const AVAILABLE_CALLS = [
  START_TOUR,
  ERSCHAFFE_GEIST,
  BESUCHE_SKELETT,
  REAGIERE_AUF_GEIST,
  ERSCHAFFE_ZOMBIE,
  REAGIERE_AUF_SKELETT,
  BESUCHE_ZOMBIE,
  ERSCHAFFE_KREATUREN,
  BESUCHE_GEIST,
  ACCEPT_VISITOR,
  REAGIERE_AUF_ZOMBIE,
  ERSCHAFFE_SKELETT,
];
