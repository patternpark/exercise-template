import { ComponentFixture, TestBed } from '@angular/core/testing';
import { VisitorTaskWithUmlComponent } from './visitor-task-with-uml.component';

describe('VisitorTaskWithUmlComponent', () => {
  let component: VisitorTaskWithUmlComponent;
  let fixture: ComponentFixture<VisitorTaskWithUmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VisitorTaskWithUmlComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitorTaskWithUmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
