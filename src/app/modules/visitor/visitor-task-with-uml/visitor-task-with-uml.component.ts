// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Projekt Organisation (Wintersemester 2021/22)
//
//  Authors:                 Christian Hemp
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 christian.hemp[at]hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //
import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { SEQUENCEDIAGRAM_LEGEND_TITLE } from '../../factory-method/task-with-uml/constants';
import mermaid from 'mermaid';
import {
  ACCEPT_VISITOR,
  AVAILABLE_CALLS,
  BESUCHE_GEIST,
  BESUCHE_SKELETT,
  BESUCHE_ZOMBIE,
  CORRECT,
  DIAGRAM_DEFINITION,
  DIAGRAM_ID,
  ERSCHAFFE_GEIST,
  ERSCHAFFE_KREATUREN,
  ERSCHAFFE_SKELETT,
  ERSCHAFFE_ZOMBIE,
  FALSE,
  REAGIERE_AUF_GEIST,
  REAGIERE_AUF_SKELETT,
  REAGIERE_AUF_ZOMBIE,
  START_TOUR,
  SUCCESS_MESSAGE,
  TASK_DESCRIPTION,
  TASK_TIP,
} from './constants';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';
import { Subject, Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';
import { MatSelect } from '@angular/material/select';
import { TaskDescriptionService } from 'src/app/shared/services/task-description.service';

@Component({
  selector: 'patternpark-visitor-task-with-uml',
  templateUrl: './visitor-task-with-uml.component.html',
  styleUrls: ['./visitor-task-with-uml.component.scss'],
})
export class VisitorTaskWithUmlComponent implements OnInit, AfterViewInit, OnDestroy {
  /**
   * Child HTML element reference that contains the mermaid diagram.
   */
  @ViewChild('visitorMermaidDiagram', { static: true })
  public visitorMermaidDiagramRef: ElementRef<HTMLDivElement> | undefined;
  /**
   * Child template reference that holds the dropdown element.
   */
  @ViewChild('dropdown')
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public dropDownItem!: TemplateRef<any>;
  /**
   * Select component that is inside the template.
   */
  @ViewChild('select')
  public mySelect!: MatSelect;
  /**
   * Title of the fieldbox.
   */
  public seqLegend = SEQUENCEDIAGRAM_LEGEND_TITLE;
  /**
   * All available calls the user can choose from.
   */
  public availableCalls = AVAILABLE_CALLS;

  /**
   * Dictionary to check wether the name of the call was chose correct.
   */
  private _checkCall: {
    [key: string]: string;
  } = {
    ['[1]']: START_TOUR,
    ['[2]']: ERSCHAFFE_KREATUREN,
    ['[3]']: ERSCHAFFE_ZOMBIE,
    ['[4]']: ERSCHAFFE_GEIST,
    ['[5]']: ERSCHAFFE_SKELETT,
    ['[6]']: ACCEPT_VISITOR,
    ['[7]']: BESUCHE_ZOMBIE,
    ['[8]']: REAGIERE_AUF_ZOMBIE,
    ['[9]']: ACCEPT_VISITOR,
    ['[10]']: BESUCHE_GEIST,
    ['[11]']: REAGIERE_AUF_GEIST,
    ['[12]']: ACCEPT_VISITOR,
    ['[13]']: BESUCHE_SKELETT,
    ['[14]']: REAGIERE_AUF_SKELETT,
  };
  /**
   * Counter that counts the amount of correct calls the user chose.
   */
  private _correctCounter = 0;
  /**
   * Amount of correct calls that is needed to finish the task.
   */
  private _neededCorrectCount = 14;
  /**
   * The HTML element of the sequence diagram.
   */
  private _visitorDiagramElement!: HTMLElement;
  /**
   * The id of the sequence diagram.
   */
  private _diagramId = DIAGRAM_ID;
  /**
   * The string that is used to draw the sequence diagram.
   */
  private _diagramDefinition = DIAGRAM_DEFINITION;
  /**
   * Overlay reference that refers to the cdkOverlay.
   */
  private _overlayRef!: OverlayRef | null;
  /**
   * Subscription that holds all subscriptions in this file.
   */
  private _sub: Subscription = new Subscription();
  /**
   * The call that is clicked by the user.
   */
  private _currentCall!: Element;
  /**
   * Subject that holds the newest selected method name by the user.
   */
  private _userSelectedCall$$: Subject<string> = new Subject<string>();
  /**
   * NodeList of all calls in the sequence diagram.
   */
  private _calls!: NodeListOf<Element>;

  constructor(
    private _overlay: Overlay,
    private _viewContainerRef: ViewContainerRef,
    private _taskDescriptionService: TaskDescriptionService
  ) {}

  /**
   * OnInit lifecyclehook that sets the description and the tips.
   */
  ngOnInit(): void {
    this._taskDescriptionService.taskDescription = TASK_DESCRIPTION;
    this._taskDescriptionService.taskDescriptionTip = TASK_TIP;
    this.checkUserSelection();
  }

  /**
   * AfterViewInit hook that initializes the mermaid diagram.
   */
  ngAfterViewInit(): void {
    if (!this.visitorMermaidDiagramRef) return;

    this._visitorDiagramElement = this.visitorMermaidDiagramRef?.nativeElement;
    mermaid.init('.mermaid');

    mermaid.render(this._diagramId, this._diagramDefinition, (svgCode: string) => {
      this._visitorDiagramElement.innerHTML = svgCode;
      this.setCallClickHandlers();
    });
  }

  ngOnDestroy(): void {
    // Unsubscibe when component is dead.
    this._sub.unsubscribe();
  }

  /**
   * Detaches the overlay when the dropdown isn't open.
   * @param open
   */
  closeMenu(open: boolean): void {
    if (!open) this._overlayRef?.detach();
  }

  /**
   * Updates the current user selection.
   * @param method Method name
   */
  onMethodSelected(method: string): void {
    this._userSelectedCall$$.next(method);
  }

  /**
   * Checks wether the selection of the user is correct or not and than sets the tip and detaches the overlay.
   */
  private checkUserSelection(): void {
    this._sub.add(
      this._userSelectedCall$$.pipe(distinctUntilChanged()).subscribe((selectedCall) => {
        if (this._currentCall.textContent && this._checkCall[this._currentCall.textContent] === selectedCall) {
          this._currentCall.textContent = selectedCall;
          this._taskDescriptionService.taskDescriptionTip = CORRECT;
          this._correctCounter++;
          if (this._correctCounter === this._neededCorrectCount) {
            this._taskDescriptionService.taskDescriptionTip = SUCCESS_MESSAGE;
          }
        } else this._taskDescriptionService.taskDescriptionTip = FALSE;
        this._overlayRef?.detach();
      })
    );
  }

  /**
   * Sets the click handler for all available calls in the diagram and creates a overlay with a mat-select if clicked.
   */
  private setCallClickHandlers(): void {
    this._calls = this._visitorDiagramElement.querySelectorAll('text.messageText');

    for (const call of Array.from(this._calls)) {
      call.classList.add('pointable');
      if (call) this._currentCall = call;

      call.addEventListener('click', () => {
        if (call && call.textContent) {
          this._currentCall = call;

          if (!this._checkCall[call.textContent]) return;

          this._overlayRef?.detach();
          const positionStrategy = this._overlay
            .position()
            .flexibleConnectedTo(call)
            .setOrigin(call)
            .withPositions([
              {
                originX: 'end',
                originY: 'bottom',
                overlayX: 'end',
                overlayY: 'top',
              },
            ]);

          this._overlayRef = this._overlay.create({
            positionStrategy,
            scrollStrategy: this._overlay.scrollStrategies.reposition(),
          });

          this._overlayRef.attach(new TemplatePortal(this.dropDownItem, this._viewContainerRef));
          setTimeout(() => this.mySelect.open(), 1);
        }
      });
    }
  }
}
