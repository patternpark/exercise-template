// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Softwareprojektorganisation (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

/**
 * Responsible for containing the items and possible options to be selected for the
 * {@link VisitorTaskWithoutUmlComponent}.
 *
 * @author Frederic Bauer
 */
export default class TaskItem {
  /**
   * The string representation / content of the item / option.
   * @private
   */
  private readonly _itemName: string;

  public constructor(itemName: string) {
    this._itemName = itemName;
  }

  public get itemName() {
    return this._itemName;
  }
}
