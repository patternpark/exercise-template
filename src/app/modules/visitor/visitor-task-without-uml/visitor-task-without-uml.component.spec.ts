import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitorTaskWithoutUmlComponent } from './visitor-task-without-uml.component';

describe('VisitorTaskWithoutUmlComponent', () => {
  let component: VisitorTaskWithoutUmlComponent;
  let fixture: ComponentFixture<VisitorTaskWithoutUmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VisitorTaskWithoutUmlComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitorTaskWithoutUmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
