// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Softwareprojektorganisation (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import TaskItem from './models/task-item';
import { EItemType } from './item-type.enum';
import { SolutionCheckType } from './solution-check-type';

/**
 * Contains the name of a function, which serves as an example option in the task.
 */
const TASK_EXAMPLE_METHOD1 = 'akzeptiere(?)';
/**
 * Contains the name of a function, which serves as an example option in the task.
 */
const TASK_EXAMPLE_METHOD2 = 'reagiereAuf(?)';

/**
 * Contains the name of a parameter, which serves as an example option in the task.
 */
const TASK_EXAMPLE_PARAMETER_TYPE1 = 'Besucher';
/**
 * Contains the name of a parameter, which serves as an example option in the task.
 */
const TASK_EXAMPLE_PARAMETER_TYPE2 = 'Geist';
/**
 * Contains the name of a parameter, which serves as an example option in the task.
 */
const TASK_EXAMPLE_PARAMETER_TYPE3 = 'Skelett';
/**
 * Contains the name of a parameter, which serves as an example option in the task.
 */
const TASK_EXAMPLE_PARAMETER_TYPE4 = 'Zombie';

/**
 * Description displayed to the student, which points ot the required steps to complete the task.
 */
export const TASK_DESCRIPTION =
  'Ordne den Items (Besucher und Attraktionen) die richtigen Methoden zu! Achte dabei auch auf die richtigen Parameter-Typen für die Methoden!';
/**
 * Basic tip displayed to the student, which contains information regarding how to interact with the task.
 */
export const TASK_BASIC_TIP =
  'Klicke auf eines der leeren Felder in den Spalten und selektiere eine Methode oder einen Parameter-Typen, um sie zuzuordnen. Du musst zuerst eine Methode eingeben, bevor du den zugehörigen Parameter-Typen auswählen kannst!';

/**
 * Tip displayed once a correct selection was made by the student.
 */
export const TASK_CORRECT_SELECTION_TIP = "Gut gemacht. '{0}' ist richtig.";
/**
 * Tip displayed once an incorrect selection was made by the student.
 */
export const TASK_INCORRECT_SELECTION_TIP = "Die Option '{0}' ist hier leider falsch.";
/**
 * Tip displayed once the student has completed the task.
 */
export const TASK_COMPLETED_TIP =
  'Glückwunsch. Du hast die Aufgabe erfolgreich abgeschlossen. Du kannst dieses Fenster nun schließen.';

/**
 * Contains the possible parameter-type options to be displayed to the student.
 */
export const TASK_PARAMETER_TYPES = [
  new TaskItem(TASK_EXAMPLE_PARAMETER_TYPE1),
  new TaskItem(TASK_EXAMPLE_PARAMETER_TYPE2),
  new TaskItem('Spinne'),
  new TaskItem(TASK_EXAMPLE_PARAMETER_TYPE3),
  new TaskItem(TASK_EXAMPLE_PARAMETER_TYPE4),
  new TaskItem('Unbekannt / Keiner'),
];

/**
 * Contains the possible method options to be displayed by the student.
 */
export const TASK_METHODS = [
  new TaskItem(TASK_EXAMPLE_METHOD1),
  new TaskItem('baueAttraktion(?)'),
  new TaskItem('erschrecken(?)'),
  new TaskItem('lachen(?)'),
  new TaskItem(TASK_EXAMPLE_METHOD2),
  new TaskItem('schreien(?)'),
];

/**
 * Dictionary like structure which contains the methods and parameter types required for each type of item
 * in order to correctly complete the task.
 */
export const ITEM_SOLUTION_CHECK: SolutionCheckType = {
  [EItemType.VISITOR.valueOf()]: [
    { name: TASK_EXAMPLE_METHOD2, amount: 3 },
    { name: TASK_EXAMPLE_PARAMETER_TYPE2, amount: 1 },
    { name: TASK_EXAMPLE_PARAMETER_TYPE3, amount: 1 },
    { name: TASK_EXAMPLE_PARAMETER_TYPE4, amount: 1 },
  ],
  [EItemType.ATTRACTION.valueOf()]: [
    { name: TASK_EXAMPLE_METHOD1, amount: 3 },
    { name: TASK_EXAMPLE_PARAMETER_TYPE1, amount: 3 },
  ],
};

/**
 * Label to be displayed on one of the selections.
 */
export const METHOD_HEADLINE_NAME = 'Methode';
/**
 * Label to be displayed on one of the selections.
 */
export const PARAMETER_HEADLINE_NAME = '{0} {1} Parameter';

/**
 * Contains number labels to be displayed on the selections and simultaneously
 * determines how many selections will be displayed in the task for the visitor.
 */
export const VISITOR_METHOD_HEADLINE_COUNTS = [1, 2, 3];
/**
 * Contains number labels to be displayed on the selections and simultaneously
 * determines how many selections will be displayed in the task for the attractions.
 */
export const ATTRACTION_METHOD_HEADLINE_COUNTS = [1];
