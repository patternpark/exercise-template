// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Softwareprojektorganisation (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

/**
 * Custom dictionary-like type containing the entries used to determine the correctness of a chosen option
 * by the student and by extension allowing to check the task for completion.
 *
 * @author Frederic Bauer
 */
export type SolutionCheckType = {
  [key: number]: { name: string; amount: number }[];
};
