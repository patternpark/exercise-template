// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Softwareprojektorganisation (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

/**
 * Represents the different types of items (in this case the fields, containing the selections etc.)
 * in the task.
 *
 * @author Frederic Bauer
 */
export enum EItemType {
  /**
   * Blanket type which describes the type not being known (yet). Allows initialisation
   * of attributes or parameters without presuming the type of item beforehand.
   */
  UNKNOWN = 0,
  /**
   * Describes visitor items (as associated with the pattern).
   */
  VISITOR = 1,
  /**
   * Describes attraction items / visitable items (as associated with the pattern).
   */
  ATTRACTION = 2,
}
