// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Softwareprojektorganisation (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { EItemType } from '../item-type.enum';
import TaskItem from '../models/task-item';
import { MatSelect } from '@angular/material/select';
import { MatFormField } from '@angular/material/form-field';

@Injectable({
  providedIn: 'root',
})
/**
 * Responsible for relaying a form selection change event to its observers and making the event
 * available outside the origin component via event.
 *
 * @author Frederic Bauer
 */
export class FormSelectChangeListenerService {
  /**
   * The observable event, which is fired, whenever a new item is input into the service.
   * @private
   */
  private readonly _formSelectionChanged = new Subject<TaskItem>();
  /**
   * The item type of the parent element to the selections in which the original event was fired.
   * See {@link VisitorMethodParameterDropDownComponent} and {@link EItemType} for more information.
   * @private
   */
  private _parentItemType: EItemType = EItemType.VISITOR;
  /**
   * Contains the selection elements which were affected when the event was fired.
   * @private
   */
  private _htmlSelectionElements: MatSelect[] = [];
  /**
   * Contains the parent element to the selection items.
   * @private
   */
  private _htmlSelectionCarrierElement: MatFormField | undefined;

  /**
   * Provides public access to the internal event of the service by allowing observers to
   * register themselves to the event.
   */
  public readonly formSelectionEvent = this._formSelectionChanged.asObservable();

  /**
   * Sets the selected item of the user to the service and fires an event accordingly.
   * See {@link _formSelectionChanged} and {@link formSelectionEvent}.
   * @param val The item that has been determined and selected by the user.
   */
  public set formSelection(val: TaskItem) {
    this._formSelectionChanged.next(val);
  }

  /**
   * Sets {@link _parentItemType}.
   * @param itemType
   */
  public set parentElementType(itemType: EItemType) {
    this._parentItemType = itemType;
  }

  /**
   * Sets {@link _htmlSelectionElements}
   * @param htmlSelection
   */
  public set htmlSelectElements(htmlSelection: MatSelect[]) {
    this._htmlSelectionElements = htmlSelection;
  }

  /**
   * Sets {@link _htmlSelectionCarrierElement}.
   * @param carrier
   */
  public set htmlSelectionCarrier(carrier: MatFormField) {
    this._htmlSelectionCarrierElement = carrier;
  }

  /**
   * Gets {@link _htmlSelectionElements}.
   */
  public get htmlSelectElements() {
    return this._htmlSelectionElements;
  }

  /**
   * Gets {@link _parentItemType}.
   */
  public get parentElementType() {
    return this._parentItemType;
  }

  /**
   * Gets {@link _htmlSelectionCarrierElement} as {@link MatFormField}.
   */
  public get htmlSelectionCarrier() {
    return <MatFormField>this._htmlSelectionCarrierElement;
  }
}
