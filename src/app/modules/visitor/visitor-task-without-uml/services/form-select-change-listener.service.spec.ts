import { TestBed } from '@angular/core/testing';

import { FormSelectChangeListenerService } from './form-select-change-listener.service';

describe('FormSelectChangeListenerService', () => {
  let service: FormSelectChangeListenerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormSelectChangeListenerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
