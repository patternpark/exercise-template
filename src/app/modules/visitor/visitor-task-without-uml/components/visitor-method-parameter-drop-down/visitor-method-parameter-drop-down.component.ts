// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Softwareprojektorganisation (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Component, Input } from '@angular/core';
import TaskItem from '../../models/task-item';
import { METHOD_LABEL, METHOD_PARAMETER_LABEL } from './constants';
import { MatSelect } from '@angular/material/select';
import { FormSelectChangeListenerService } from '../../services/form-select-change-listener.service';
import { EItemType } from '../../item-type.enum';
import { TASK_METHODS, TASK_PARAMETER_TYPES } from '../../constants';
import { MatFormField } from '@angular/material/form-field';

@Component({
  selector: 'patternpark-visitor-method-parameter-drop-down',
  templateUrl: './visitor-method-parameter-drop-down.component.html',
  styleUrls: ['./visitor-method-parameter-drop-down.component.scss'],
})
/**
 * Responsible for handling all functionality related to the drop-down component.
 *
 * @author Frederic Bauer
 */
export class VisitorMethodParameterDropDownComponent {
  @Input()
  /**
   * Describes the item type of the container of this drop-down component see {@link EItemType} for more info.
   */
  public parentItemType: EItemType;
  @Input()
  /**
   * The label to apply to the first selection field of the component.
   */
  public methodLabel: string;
  @Input()
  /**
   * The label to apply to the second selection field of the component.
   */
  public parameterLabel: string;
  @Input()
  /**
   * A list of "methods" to apply to the method drop-down selection of the component.
   */
  public methods: TaskItem[];
  @Input()
  /**
   * A list of "parameter types" to apply to the parameter type drop-down selection of the component.
   */
  public methodParameterTypes: TaskItem[];

  constructor(private _formSelectChangeService: FormSelectChangeListenerService) {
    this.parentItemType = EItemType.UNKNOWN;
    this.methodLabel = METHOD_LABEL;
    this.parameterLabel = METHOD_PARAMETER_LABEL;
    this.methods = [];
    this.methodParameterTypes = [];
  }

  /**
   * Handles the event when a "method"-element has been selected by the user.
   * @param selectionElement The selection element which has fired the event.
   * @param parameterTypeSelection The remaining (other) selection element which is also part of the component.
   * @param selectionCarrierElement The form field carrying the selections.
   */
  handleMethodSelection(
    selectionElement: MatSelect,
    parameterTypeSelection: MatSelect,
    selectionCarrierElement: MatFormField
  ): void {
    let correspondingTaskItem = TASK_METHODS.find((item) => item.itemName === selectionElement.value);

    if (!correspondingTaskItem) return;

    this.handleSelectionChange(
      correspondingTaskItem,
      [selectionElement, parameterTypeSelection],
      selectionCarrierElement
    );
  }

  /**
   * Handles the event when a "parameter type"-element has been selected by the user.
   * @param selectionElement The selection element which has fired the event.
   * @param selectionCarrierElement The form field carrying the selections.
   */
  handleParameterSelection(selectionElement: MatSelect, selectionCarrierElement: MatFormField): void {
    let correspondingTaskItem = TASK_PARAMETER_TYPES.find((item) => item.itemName === selectionElement.value);

    if (!correspondingTaskItem) return;

    this.handleSelectionChange(correspondingTaskItem, [selectionElement], selectionCarrierElement);
  }

  /**
   * Handles the selection event regardless of which type of selection was made. Unites the common
   * operations regarding the event of the two previous function {@link handleParameterSelection} and
   * {@link handleMethodSelection}.
   * @param correspondingTaskItem The task item found by the previous methods related to the selection made by the user.
   * @param selections The selection elements which were part of the event (and component):
   * @param selectionCarrierElement The parent element containing the selection elements.
   * @private
   */
  private handleSelectionChange(
    correspondingTaskItem: TaskItem,
    selections: MatSelect[],
    selectionCarrierElement: MatFormField
  ): void {
    this._formSelectChangeService.htmlSelectElements = selections;
    this._formSelectChangeService.parentElementType = this.parentItemType;
    this._formSelectChangeService.htmlSelectionCarrier = selectionCarrierElement;
    this._formSelectChangeService.formSelection = correspondingTaskItem;
  }
}
