import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitorMethodParameterDropDownComponent } from './visitor-method-parameter-drop-down.component';

describe('VisitorMethodParameterDropDownComponent', () => {
  let component: VisitorMethodParameterDropDownComponent;
  let fixture: ComponentFixture<VisitorMethodParameterDropDownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VisitorMethodParameterDropDownComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitorMethodParameterDropDownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
