// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Softwareprojektorganisation (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

/**
 * Represents the label displayed in the selections for methods.
 */
export const METHOD_LABEL = 'Methode';
/**
 * Represents the label displayed in the selections for patterns.
 */
export const METHOD_PARAMETER_LABEL = 'Parameter';
