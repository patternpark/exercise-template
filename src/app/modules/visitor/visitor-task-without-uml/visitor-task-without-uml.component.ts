// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Softwareprojektorganisation (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Component, OnInit } from '@angular/core';
import { TaskDescriptionService } from '../../../shared/services/task-description.service';
import {
  ATTRACTION_METHOD_HEADLINE_COUNTS,
  ITEM_SOLUTION_CHECK,
  METHOD_HEADLINE_NAME,
  PARAMETER_HEADLINE_NAME,
  TASK_BASIC_TIP,
  TASK_COMPLETED_TIP,
  TASK_CORRECT_SELECTION_TIP,
  TASK_DESCRIPTION,
  TASK_INCORRECT_SELECTION_TIP,
  TASK_METHODS,
  TASK_PARAMETER_TYPES,
  VISITOR_METHOD_HEADLINE_COUNTS,
} from './constants';
import TaskItem from './models/task-item';
import { EItemType } from './item-type.enum';
import { FormSelectChangeListenerService } from './services/form-select-change-listener.service';
import { MatSelect } from '@angular/material/select';
import { getAnimationDuration, getBorderColorAnimation } from '../../../shared/animations/user-interactions';
import { COLOR_FAILED, COLOR_SUCCESSFUL } from '../../../shared/styles/color-variables';
import { SolutionCheckType } from './solution-check-type';

@Component({
  selector: 'patternpark-visitor-task-without-uml',
  templateUrl: './visitor-task-without-uml.component.html',
  styleUrls: ['./visitor-task-without-uml.component.scss'],
})
/**
 * Class representing the task component, handling the functionality and operations
 * associated with the task.
 *
 * @author Frederic Bauer
 */
export class VisitorTaskWithoutUmlComponent implements OnInit {
  /**
   * Contains {@link EItemType} and provides access to it in the corresponding html template.
   */
  public readonly ITEM_TYPE_ENUM = EItemType;
  /**
   * Contains the method names of the {@link TASK_METHODS} constant and allows access to them
   * in the corresponding html template.
   */
  public readonly methodNames: TaskItem[] = TASK_METHODS;
  /**
   * Contains the parameter type names of the {@link TASK_PARAMETER_TYPES} constant and allows
   * access to them in the corresponding html template.
   */
  public readonly parameterTypeNames: TaskItem[] = TASK_PARAMETER_TYPES;
  /**
   * Contains the method html headline of the {@link METHOD_HEADLINE_NAME} constant and allows
   * access to it in the corresponding html template.
   */
  public readonly methodHTMLHeadline: string = METHOD_HEADLINE_NAME;
  /**
   * Contains the parameter type html headline of the {@link PARAMETER_HEADLINE_NAME} constant
   * and allows access to it in the corresponding html template.
   */
  public readonly parameterHTMLHeadline: string = PARAMETER_HEADLINE_NAME;
  /**
   * Contains the visitor selection counter of the {@link VISITOR_METHOD_HEADLINE_COUNTS} constant and
   * allows access to it in the corresponding html template.
   */
  public readonly visitorHeadlineCount: number[] = VISITOR_METHOD_HEADLINE_COUNTS;
  /**
   * Contains the attraction's selection counter {@link VISITOR_METHOD_HEADLINE_COUNTS} constant and
   * allows access to it in the corresponding html template.
   */
  public readonly attractionHeadlineCount: number[] = ATTRACTION_METHOD_HEADLINE_COUNTS;
  /**
   * Representation of the {@link ITEM_SOLUTION_CHECK} solution array for use in
   * managing the task and interactions of the user with the task.
   * @private
   */
  private readonly _solutionArray: SolutionCheckType;

  constructor(
    private _taskDescriptionService: TaskDescriptionService,
    private _formSelectionService: FormSelectChangeListenerService
  ) {
    this._taskDescriptionService.taskDescription = TASK_DESCRIPTION;
    this._taskDescriptionService.taskDescriptionTip = TASK_BASIC_TIP;
    this._solutionArray = JSON.parse(JSON.stringify(ITEM_SOLUTION_CHECK));
  }

  ngOnInit(): void {
    this._formSelectionService.formSelectionEvent.subscribe(this.reactToFormChange.bind(this));
  }

  /**
   * Handles the event of the {@link FormSelectChangeListenerService} in reaction to a change in
   * a form element. Checks whether the chosen option is correct and indicates the result accordingly
   * to the user.
   * @param selectedItem The task item representing a method or parameter type which has been selected by the user.
   * @private
   */
  private reactToFormChange(selectedItem: TaskItem): void {
    if (this._formSelectionService.htmlSelectElements.length < 1) return;

    const isCorrect: boolean = this.checkForCorrectness(selectedItem);
    const formSelectionElements: MatSelect[] = this._formSelectionService.htmlSelectElements;
    const selectionCarrier = this._formSelectionService.htmlSelectionCarrier._elementRef.nativeElement;

    // Remove any still running animation before starting a new one.
    selectionCarrier.removeAttribute('data-successful');
    selectionCarrier.removeAttribute('data-failed');

    if (isCorrect) {
      selectionCarrier.setAttribute('data-successful', true);

      const animation = selectionCarrier.animate(getBorderColorAnimation(COLOR_SUCCESSFUL), getAnimationDuration(4000));

      animation.onfinish = () => {
        selectionCarrier.removeAttribute('data-successful');
      };

      for (const htmlSelection of formSelectionElements) {
        htmlSelection.disabled = !htmlSelection.disabled;
      }

      if (this.checkForTaskCompleteness()) {
        this._taskDescriptionService.taskDescriptionTip = TASK_COMPLETED_TIP;
        return;
      }

      this._taskDescriptionService.taskDescriptionTip = TASK_CORRECT_SELECTION_TIP;
    } else {
      selectionCarrier.setAttribute('data-failed', true);
      const animation = selectionCarrier.animate(getBorderColorAnimation(COLOR_FAILED), getAnimationDuration(4000));
      animation.onfinish = () => {
        selectionCarrier.removeAttribute('data-failed');
      };
      formSelectionElements[0].value = '';
      this._taskDescriptionService.taskDescriptionTip = TASK_INCORRECT_SELECTION_TIP;
    }

    this._taskDescriptionService.taskDescriptionTip = this._taskDescriptionService.taskDescriptionTip.format(
      selectedItem.itemName
    );
  }

  /**
   * Checks whether the selection made by the user is correct.
   * @param selectedItem The item selected by the user.
   * @private
   * @return boolean - Whether the selection is correct or not.
   */
  private checkForCorrectness(selectedItem: TaskItem): boolean {
    const solutionArray = this._solutionArray[this._formSelectionService.parentElementType];

    const index: number = solutionArray.findIndex((obj) => obj.name === selectedItem.itemName);

    if (index < 0) return false;

    const solutionItem = solutionArray[index];

    if (solutionItem.amount < 2) {
      solutionArray.splice(index, 1);
    } else {
      --solutionItem.amount;
    }

    return true;
  }

  /**
   * Checks whether the task has been completed successfully.
   * @private
   * @returns boolean - Whether the task has been completed successfully or not.
   */
  private checkForTaskCompleteness(): boolean {
    let missingItemsCount: number = 0;
    for (let key in this._solutionArray) {
      missingItemsCount += this._solutionArray[key].length;
    }

    return missingItemsCount < 1;
  }
}
