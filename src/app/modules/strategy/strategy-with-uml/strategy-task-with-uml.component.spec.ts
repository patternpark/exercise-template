import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StrategyTaskWithUmlComponent } from './strategy-task-with-uml.component';

describe('StrategyWithUmlComponent', () => {
  let component: StrategyTaskWithUmlComponent;
  let fixture: ComponentFixture<StrategyTaskWithUmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StrategyTaskWithUmlComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StrategyTaskWithUmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
