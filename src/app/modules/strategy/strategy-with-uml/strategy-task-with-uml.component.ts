import { Component } from '@angular/core';
import { TaskDescriptionService } from '../../../shared/services/task-description.service';

@Component({
  selector: 'patternpark-strategy-exercise-with-uml',
  templateUrl: './strategy-task-with-uml.html',
  styleUrls: ['./strategy-task-with-uml.component.scss'],
})
export class StrategyTaskWithUmlComponent {
  constructor(private _taskDescService: TaskDescriptionService) {
    _taskDescService.taskDescription =
      'In dieser Aufgabe geht es darum, Klassennamen und die dazugehörigen Funktionen den entsprechenden Objekten im UML-Diagramm zuzuordnen.' +
      "Dieses Diagramm besteht aus einer Klasse 'Verkäufer', welche ein Objekt einer Klasse entgegennimmt, welches das Interface 'Verkaufsstrategie' implementiert. " +
      'Dieses Interface wird von drei verschiedenen Klassen implementiert, welche verschiedene Verkaufsstrategien darstellen. ' +
      'Setzen Sie die gegebenen Klassen- und Funktionsnamen sinnvoll in das UML-Diagramm ein.';
    _taskDescService.taskDescriptionTip =
      'Die Rahmen der einzelnen UML-Klassen färben sich, je nachdem, inwiefern die eingesetzten Kästchen korrekt sind. ' +
      'Ein grauer Rahmen indiziert keinen Fehler, bei einem orangenen Rahmen ist ein eingesetztes Kästchen falsch ' +
      'und wenn eine Kombination zweier Kästchen falsch ist, ist der Rahmen rot.';
  }

  public readonly CLASS_POOL_CONTAINER_ID = 'container1';

  dropContainer(ev: DragEvent, id: string) {
    ev.preventDefault();
    let data = ev.dataTransfer!.getData('text');
    document.getElementById(id)!.appendChild(document.getElementById(data)!);

    this.evaluateAll();
  }
  allowDropContainer(ev: DragEvent) {
    ev.preventDefault();
  }

  drop(event: DragEvent) {
    event.preventDefault();
    const eventTarget: HTMLElement = event.currentTarget as HTMLElement;
    const data: string = event.dataTransfer!.getData('text');

    eventTarget.appendChild(document.getElementById(data)!);

    const parentElement: HTMLElement = eventTarget.parentNode as HTMLElement;

    this.evaluateExercise(parentElement);
    this.evaluateAll();
  }

  allowDrop(ev: DragEvent) {
    if ((ev.target! as HTMLElement).className !== 'dragObject') {
      if ((ev.target! as HTMLElement).children.length < 1) {
        ev.preventDefault();
      }
    }
  }

  drag(ev: DragEvent) {
    ev.dataTransfer!.setData('text', (ev.target as HTMLElement).id);
  }

  evaluateExercise(elementToEvaluate: HTMLElement) {
    const children: HTMLCollection = elementToEvaluate.children;

    // if both subBoxes filled
    if (children[0].children[0] && children[1].children[0]) {
      this.handleBothCheckboxesFilled(children, elementToEvaluate);
    }
    // if only one subbox filled
    else {
      if (children[0].children[0]) {
        this.handleFirstCheckboxFilled(children, elementToEvaluate);
      } else if (children[1].children[0]) {
        this.handleSecondCheckboxFilled(children, elementToEvaluate);
      } else {
        elementToEvaluate.style.borderColor = 'black';
      }
    }
  }

  evaluateAll() {
    const allUmlObjects: HTMLCollectionOf<Element> = document.getElementsByClassName('umlObject');
    for (const umlObject of Array.from(allUmlObjects)) {
      this.evaluateExercise(umlObject as HTMLElement);
    }
  }

  private handleFirstCheckboxFilled(children: HTMLCollection, parent: HTMLElement) {
    // checks if className-box is filled with className
    if (
      (children[0] as HTMLElement).dataset.affiliation == (children[0].children[0] as HTMLElement).dataset.affiliation
    ) {
      parent.style.borderColor = 'grey';
      // checks, if correct uml-object
      if ((children[0] as HTMLElement).dataset.div !== (children[0].children[0] as HTMLElement).dataset.div) {
        parent.style.borderColor = 'orange';
      }
    } else {
      parent.style.borderColor = 'orange';
    }
  }

  private handleSecondCheckboxFilled(children: HTMLCollection, parent: HTMLElement) {
    // checks if function-box is filled with function
    if (
      (children[1] as HTMLElement).dataset.affiliation == (children[1].children[0] as HTMLElement).dataset.affiliation
    ) {
      parent.style.borderColor = 'grey';
      // checks, if correct uml-object
      if ((children[1] as HTMLElement).dataset.div == (children[1].children[0] as HTMLElement).dataset.div) {
        parent.style.borderColor = 'grey';
      } else {
        parent.style.borderColor = 'orange';
      }
    } else {
      parent.style.borderColor = 'orange';
    }
  }

  private handleBothCheckboxesFilled(children: HTMLCollection, parent: HTMLElement) {
    // checks if className-box is filled with className, same for function
    if (
      (children[0] as HTMLElement).dataset.affiliation ==
        (children[0].children[0] as HTMLElement).dataset.affiliation &&
      (children[1] as HTMLElement).dataset.affiliation == (children[1].children[0] as HTMLElement).dataset.affiliation
    ) {
      parent.style.borderColor = 'grey';
      // checks if correct uml-object
      if (
        (children[0] as HTMLElement).dataset.div == (children[0].children[0] as HTMLElement).dataset.div &&
        (children[1] as HTMLElement).dataset.div == (children[1].children[0] as HTMLElement).dataset.div
      ) {
        parent.style.borderColor = 'green';
      } else {
        // at least one of the divs dont fit
        parent.style.borderColor = 'red';
      }
    } else {
      // at least one of the types dont fit
      parent.style.borderColor = 'red';
    }
  }
}
