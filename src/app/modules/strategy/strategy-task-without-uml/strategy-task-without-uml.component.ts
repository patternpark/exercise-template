import { Component } from '@angular/core';
import {
  CALCULATOR_TIP,
  GENERAL_TIP,
  NOT_A_GOAL,
  NOT_A_STRATEGY,
  STRATEGY_TASK_WITHOUT_UML_DESCRIPTION,
  WORD_POOL_CONTAINER_ID,
} from './constants';
import { TaskDescriptionService } from '../../../shared/services/task-description.service';

@Component({
  selector: 'patternpark-strategy-exercise-without-uml',
  templateUrl: './strategy-task-without-uml.html',
  styleUrls: ['./strategy-task-without-uml.component.scss'],
})
export class StrategyTaskWithoutUmlComponent {
  constructor(private _taskDescService: TaskDescriptionService) {
    this._taskDescService.taskDescription = STRATEGY_TASK_WITHOUT_UML_DESCRIPTION;
    this._taskDescService.taskDescriptionTip = GENERAL_TIP;
  }

  public readonly WORD_POOL_CONTAINER_ID = WORD_POOL_CONTAINER_ID;
  private _x = 0;

  showCalculatorTip() {
    this._taskDescService.taskDescriptionTip = CALCULATOR_TIP;
  }

  showGeneralTip() {
    this._taskDescService.taskDescriptionTip = GENERAL_TIP;
  }

  dropContainer(ev: DragEvent, id: string) {
    ev.preventDefault();
    const data: string = ev.dataTransfer!.getData('text');
    document.getElementById(id)!.appendChild(document.getElementById(data)!);
    this.evaluateExercise();
  }

  allowDropContainer(ev: DragEvent) {
    ev.preventDefault();
  }

  drop(ev: DragEvent) {
    ev.preventDefault();
    const data: string = ev.dataTransfer!.getData('text');
    (<HTMLDivElement>ev.target).appendChild(document.getElementById(data)!);
    this.evaluateExercise();
  }

  allowDrop(ev: DragEvent) {
    if ((<HTMLDivElement>ev.target).className !== 'dragObject') {
      if ((<HTMLDivElement>ev.target).children.length < 1) {
        ev.preventDefault();
      }
    }
  }

  drag(ev: DragEvent) {
    ev.dataTransfer!.setData('text', (<HTMLDivElement>ev.target).id);
  }

  evaluateExercise() {
    let el: HTMLCollection = document.getElementsByClassName('dropPoint');
    let elBox: HTMLCollectionOf<Element> = document.getElementsByClassName('validationBox');

    for (let i = 0; i < elBox.length; i++) {
      elBox[i].setAttribute(
        'style',
        'background-color: ' + this.getRowStatusColor(<HTMLElement>el[i * 2], <HTMLElement>el[i * 2 + 1]) + ';'
      );
      console.log(elBox[i]);

      if (el[i * 2].childNodes[0] !== undefined && (el[i * 2].childNodes[0] as HTMLElement).dataset.goal == 'false') {
        this._taskDescService.taskDescriptionTip = NOT_A_GOAL;
      }
      if (
        el[i * 2 + 1].childNodes[0] !== undefined &&
        (el[i * 2 + 1].childNodes[0] as HTMLElement).dataset.goal == 'true'
      ) {
        this._taskDescService.taskDescriptionTip = NOT_A_STRATEGY;
      }
    }
  }

  getRowStatusColor(el1: HTMLElement, el2: HTMLElement) {
    if (el1.childNodes[0] == undefined && el2.childNodes[0] == undefined) {
      return '#ffffff';
    }

    if (el1.childNodes[0] == undefined || el2.childNodes[0] == undefined) {
      return '#aaaaaa';
    }

    if (
      (el1.childNodes[0] as HTMLElement).dataset.goal == 'false' ||
      (el2.childNodes[0] as HTMLElement).dataset.goal == 'true'
    ) {
      return '#ff4444';
    }

    if (
      (el1.childNodes[0] as HTMLElement).dataset.affiliation == (el2.childNodes[0] as HTMLElement).dataset.affiliation
    ) {
      return '#44ff44';
    }

    if (
      (el1.childNodes[0] as HTMLElement).dataset.affiliation !== (el2.childNodes[0] as HTMLElement).dataset.affiliation
    ) {
      return '#ff4444';
    }

    return '#ffffff';
  }
}
