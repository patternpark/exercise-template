import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StrategyTaskWithoutUmlComponent } from './strategy-task-without-uml.component';

describe('StrategyExerciseWithoutUMLComponent', () => {
  let component: StrategyTaskWithoutUmlComponent;
  let fixture: ComponentFixture<StrategyTaskWithoutUmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StrategyTaskWithoutUmlComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StrategyTaskWithoutUmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
