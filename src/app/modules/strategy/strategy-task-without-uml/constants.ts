export const WORD_POOL_CONTAINER_ID = 'container1';
export const STRATEGY_TASK_WITHOUT_UML_DESCRIPTION =
  'In dieser Aufgabe geht es darum, das Prinzip hinter dem Strategie-Pattern genauer zu verstehen. ' +
  'Wir haben mehrere Objekte der Taschenrechner-Klasse, welche die Funktion BERECHNE(x,y) (CALCULATE) aufrufen. Diese Objekte sollen dabei verschiedene Ergebnisse erreichen. ' +
  'Dazu müssen sie jeweils mit einer bestimmten Strategie Instanziiert werden. Die Aufgabe besteht nun darin, den Ergebnis-Zahlen' +
  'die jeweils richtige Strategien zuzuweisen, welche die Klasse mit den gegebenen Parametern verwenden muss.';

export const GENERAL_TIP = 'Das Ziel ist eine Zahl, welche mit einer Strategie berechnet werden kann';
export const CALCULATOR_TIP = 'Die Übung basiert auf der Rechnung mit den Zahlen 5 und 5';
export const NOT_A_GOAL = 'Das Ziel sollte eine Zahl sein';
export const NOT_A_STRATEGY = 'Die Strategie sollte eine Operationsart sein';
