import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StrategyTaskWithoutUmlComponent } from './strategy-task-without-uml/strategy-task-without-uml.component';
import { StrategyTaskWithUmlComponent } from './strategy-with-uml/strategy-task-with-uml.component';
import { UiComponentsModule } from '../../core/modules/ui-components.module';

@NgModule({
  declarations: [StrategyTaskWithoutUmlComponent, StrategyTaskWithUmlComponent],
  imports: [CommonModule, UiComponentsModule],
  exports: [StrategyTaskWithoutUmlComponent, StrategyTaskWithUmlComponent],
})
export class StrategyModule {}
