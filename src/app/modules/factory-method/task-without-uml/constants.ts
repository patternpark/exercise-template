// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { TaskItem } from './task-page2/models/task-item';
import { ETaskItemType } from './task-item-type.enum';

/**
 * A description to be displayed, once the user completed the task.
 */
export const TASK_COMPLETE = 'Glückwunsch, du hast die Aufgabe erfolgreich abgeschlossen!';

/**
 * A description to be displayed, once the user completed the task (telling them to head to the next task page).
 */
export const TASK_TURN_TO_NEXT_PAGE = 'Gehe nun zur nächsten Seite (klicke auf den Pfeil) für den Rest der Aufgabe.';

/**
 * A description to be displayed, once the user has completed the task (telling them to close the task window).
 */
export const END_TASK = 'Du kannst das Aufgabenfenster nun schließen.';

/**
 * Contains the example creator and product items for the two tasks.
 */
export const FACTORY_TASK_WITHOUT_UML_CREATOR_WITH_PRODUCTS = [
  new TaskItem('Pizzeria', ETaskItemType.CREATOR, [
    new TaskItem('Pizza', ETaskItemType.PRODUCT),
    new TaskItem('Spaghetti Bolognese', ETaskItemType.PRODUCT),
  ]),
  new TaskItem('China Imbiss', ETaskItemType.CREATOR, [
    new TaskItem('Bratnudeln', ETaskItemType.PRODUCT),
    new TaskItem('Peking-Ente', ETaskItemType.PRODUCT),
  ]),
  new TaskItem('Tischler', ETaskItemType.CREATOR, [new TaskItem('Tisch', ETaskItemType.PRODUCT)]),
  new TaskItem('Kiosk', ETaskItemType.CREATOR, [
    new TaskItem('Zeitung', ETaskItemType.PRODUCT),
    new TaskItem('Poster', ETaskItemType.PRODUCT),
  ]),
];

/**
 * Contains the misleading task item examples which are neither products nor creators.
 */
export const FACTORY_TASK_WITHOUT_UML_NOTHING = [
  new TaskItem('Ordnungsamt', ETaskItemType.NOTHING),
  new TaskItem('Mario', ETaskItemType.NOTHING),
  new TaskItem('Druckerpatronen', ETaskItemType.NOTHING),
];
