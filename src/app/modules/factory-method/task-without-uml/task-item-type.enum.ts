// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

/**
 * Represents the different types a task item can have.
 *
 * @author Frederic Bauer
 */
export enum ETaskItemType {
  /**
   * When the task item is neither a creator nor a product.
   */
  NOTHING,
  /**
   * When the task item is a creator.
   */
  CREATOR,
  /**
   * When the task item is a product.
   */
  PRODUCT,
}
