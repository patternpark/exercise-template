// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Component } from '@angular/core';

/**
 * @author Frederic Bauer
 */
@Component({
  selector: 'patternpark-task-without-uml',
  templateUrl: './task-without-uml.component.html',
  styleUrls: ['./task-without-uml.component.scss'],
})
export class TaskWithoutUmlComponent {}
