import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskPage1Component } from './task-page1.component';

describe('TaskPage1Component', () => {
  let component: TaskPage1Component;
  let fixture: ComponentFixture<TaskPage1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TaskPage1Component],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskPage1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
