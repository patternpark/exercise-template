// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Component, OnInit } from '@angular/core';
import {
  FACTORY_TASK_WITHOUT_UML_CREATOR_WITH_PRODUCTS,
  FACTORY_TASK_WITHOUT_UML_NOTHING,
  TASK_COMPLETE,
  TASK_TURN_TO_NEXT_PAGE,
} from '../constants';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { ETaskItemType } from '../task-item-type.enum';
import { TaskDescriptionService } from '../../../../shared/services/task-description.service';
import { getAnimationDuration, getBorderColorAnimation } from '../../../../shared/animations/user-interactions';
import { shuffle } from '../../../../shared/utilities/array-utility';
import { TaskPageBase } from '../task-page-base';
import { TaskItemBase } from '../models/task-item-base';
import { COLOR_FAILED, COLOR_SUCCESSFUL } from '../../../../shared/styles/color-variables';
import {
  BORDER_ANIMATION_DURATION,
  COLUMN,
  FACTORY_TASK_WITHOUT_UML_PAGE1,
  TASK_PAGE1_TIP,
  TASK_PAGE1_WRONG_COLUMN,
} from './constants';
import { HeaderImageService } from '../../../../shared/services/header-image.service';

/**
 * Responsible for handling the first page of the task without uml, including checking the solution
 * for correctness and more.
 *
 * @author Frederic Bauer
 */
@Component({
  selector: 'patternpark-task-page1',
  templateUrl: './task-page1.component.html',
  styleUrls: [
    '../../../../shared/styles/cdk-prettify.scss',
    '../task-without-uml.component.scss',
    './task-page1.component.scss',
  ],
})
export class TaskPage1Component extends TaskPageBase implements OnInit {
  /**
   * Exposes the enum to the html template.
   */
  public readonly TASK_ITEM_TYPE_ENUM = ETaskItemType;
  /**
   * List for creator type task items (exposed to the template).
   */
  public creators: TaskItemBase[] = [];
  /**
   * List for product type task items (exposed to the template).
   */
  public products: TaskItemBase[] = [];
  /**
   * List for all task items regardless of type (exposed to the template).
   */
  public allTaskItems: TaskItemBase[] = [];

  constructor(
    private _taskDescService: TaskDescriptionService,
    private readonly _headerImageService: HeaderImageService
  ) {
    super();
    this._headerImageService.headerIcon = 'assets/factory-method/header-icon.png';

    _taskDescService.taskDescription = FACTORY_TASK_WITHOUT_UML_PAGE1;
    _taskDescService.taskDescriptionTip = TASK_PAGE1_TIP;

    _taskDescService.navigationVisibility = false;
    _taskDescService.navigationPreviousItem = {
      url: 'factory-method/without-uml/task1',
      isEnabled: false,
      isVisible: false,
    };
    _taskDescService.navigationNextItem = {
      url: 'factory-method/without-uml/task2',
      isEnabled: true,
      isVisible: true,
    };
  }

  ngOnInit(): void {
    this.fillTaskArray();
  }

  /**
   * Deals with the Angular-Drop-List functionality wherein the items that are being dragged and dropped
   * will be handled accordingly.
   *
   * @param event Event that occurs when an item is being dropped into one of the predefined lists.
   * @param listType The type of the items stored in the drop list, determining based on type, whether an item can be dropped into the list.
   */
  drop(event: CdkDragDrop<TaskItemBase[]>, listType: ETaskItemType): void {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else if (listType === event.previousContainer.data[event.previousIndex].taskType) {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.container.data.length
      );
      event.container.element.nativeElement.parentElement?.animate(
        getBorderColorAnimation(COLOR_SUCCESSFUL),
        getAnimationDuration(BORDER_ANIMATION_DURATION)
      );

      if (!this.checkIfTaskComplete()) return;

      this._taskDescService.taskDescriptionTip = `${TASK_COMPLETE} ${TASK_TURN_TO_NEXT_PAGE}`;
      this._taskDescService.navigationVisibility = true;
    } else {
      event.container.element.nativeElement.parentElement?.animate(
        getBorderColorAnimation(COLOR_FAILED),
        getAnimationDuration(BORDER_ANIMATION_DURATION)
      );
      this._taskDescService.taskDescriptionTip = TASK_PAGE1_WRONG_COLUMN.format(
        event.previousContainer.data[event.previousIndex].name,
        COLUMN
      );
    }
  }

  /**
   * Handles the task setup, in which the declared items for the task will be added to
   * the internal list, so that they are accessible in the corresponding task.
   * @private
   */
  private fillTaskArray(): void {
    this.allTaskItems.push(...this.splitCreatorsFromProducts(FACTORY_TASK_WITHOUT_UML_CREATOR_WITH_PRODUCTS));
    this.allTaskItems.push(...FACTORY_TASK_WITHOUT_UML_NOTHING);

    shuffle(this.allTaskItems);
  }

  /**
   * Checks whether the task has been completed successfully by the user.
   * @private
   * @returns boolean - Stating whether the task has been completed by the user.
   */
  private checkIfTaskComplete(): boolean {
    return (
      this.creators.length === FACTORY_TASK_WITHOUT_UML_CREATOR_WITH_PRODUCTS.length &&
      this.products.length === this.getNumberOfAllProducts(FACTORY_TASK_WITHOUT_UML_CREATOR_WITH_PRODUCTS)
    );
  }
}
