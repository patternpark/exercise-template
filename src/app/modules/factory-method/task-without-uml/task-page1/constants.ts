// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

/**
 * A task description for the first page / task "without uml".
 *
 * @author Frederic Bauer
 */
export const FACTORY_TASK_WITHOUT_UML_PAGE1 =
  'Ordne die folgenden Elemente jeweils der Erzeuger-Spalte und der Produkt-Spalte zu. Vorsicht, nicht alle Elemente gehören in die zwei Kategorien (zum Beispiel, weil sie nicht zu den gelisteten Erzeugern passen usw.).';

/**
 * An error message, stating that the user tried to enter an item into the wrong column.
 */
export const TASK_PAGE1_WRONG_COLUMN =
  'Dieses Element ({0}) kann nicht in die {1} eingefügt werden (es hat den falschen Typ).';

/**
 * The tip to be displayed on the page (regarding how to interact with the given task).
 */
export const TASK_PAGE1_TIP = 'Klicke auf eines der Elemente und ziehe es dann in das richtige Feld.';

/**
 * Contains a magic string, describing a column.
 */
export const COLUMN = 'Spalte';

/**
 * Contains a magic number, describing the animation duration for the border colour for task 1.
 */
export const BORDER_ANIMATION_DURATION = 3000;
