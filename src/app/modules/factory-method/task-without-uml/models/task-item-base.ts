// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { ETaskItemType } from '../task-item-type.enum';

/**
 * An item, which stores required information about the items belonging to the task. It serves as a
 * representation of the single elements present in the task.
 *
 * @author Frederic Bauer
 */
export class TaskItemBase {
  /**
   * Describes the name of the item (often corresponding with the displayed text).
   * @protected
   */
  protected _name: string;
  /**
   * Describes the type of the item in relation to the task.
   * @see ETaskItemType For further info on the types of task items.
   * @protected
   */
  protected _taskItemType: ETaskItemType;

  public constructor(name: string, taskItemType: ETaskItemType) {
    this._name = name;
    this._taskItemType = taskItemType;
  }

  /**
   * Returns the name attribute of the item.
   */
  public get name(): string {
    return this._name;
  }

  /**
   * Returns the task type attribute of the item.
   */
  public get taskType(): ETaskItemType {
    return this._taskItemType;
  }
}
