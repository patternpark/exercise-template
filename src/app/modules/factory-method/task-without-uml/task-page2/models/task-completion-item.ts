// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { TaskItem } from './task-item';
import { EUserActionResult } from '../user-action-result.enum';

/**
 * Item for dealing with the users actions in context of the task itself. Checks for whether certain
 * actions are allowed etc.
 *
 * @author Frederic Bauer
 */
export default class TaskCompletionItem {
  /**
   * List of all successful connections the user has made for the task.
   * @private
   */
  private readonly _connectedProducts: TaskItem[] = [];
  /**
   * List of the creator items belonging to the task.
   * @private
   */
  private _creators: TaskItem[] = [];

  /**
   * Setter to set the creators for the item.
   * @param creators
   */
  public set creators(creators: TaskItem[]) {
    this._creators = creators;
  }

  /**
   * Adds a connection the user has made between a creator and product,
   * provided the connection is correct.
   * @param creator The creator item to connect to the product.
   * @param product The product to connect to the creator.
   * @returns EUserActionResult - Stating the status of the action (whether it could be performed or not).
   */
  public addConnection(creator: TaskItem, product: TaskItem): EUserActionResult {
    if (!this._creators.includes(creator) || !creator.connections.includes(product)) {
      return EUserActionResult.incorrectConnection;
    } else if (this._connectedProducts.includes(product)) {
      return EUserActionResult.connectionAlreadyExists;
    }

    this._connectedProducts.push(product);
    return EUserActionResult.correctConnection;
  }

  /**
   * Checks for whether all required connections of the task have been set.
   *
   * @returns boolean - Stating whether the task has been completed.
   */
  public checkIfAllConnectionsSet(): boolean {
    let numberOfConnections = 0;
    for (const creator of this._creators) {
      numberOfConnections += creator.connections.length;
    }
    return numberOfConnections === this._connectedProducts.length;
  }
}
