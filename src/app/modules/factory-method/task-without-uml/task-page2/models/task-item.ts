// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { TaskItemBase } from '../../models/task-item-base';
import { ETaskItemType } from '../../task-item-type.enum';

/**
 * Extension of {@link TaskItemBase}, which adds required information for the
 * next task ({@link TaskPage2Component}) .
 *
 * @author Frederic Bauer
 */
export class TaskItem extends TaskItemBase {
  /**
   * List of products belonging to the creator.
   * @private
   */
  private _connections: TaskItem[] = [];

  public constructor(name: string, taskItemType: ETaskItemType, connections?: TaskItem[]) {
    super(name, taskItemType);

    if (connections) {
      this.addConnections(...connections);
    }
  }

  /**
   * Getter, which returns the {@link _connections} of the creator.
   * @returns TaskItem[] - The connections of the creator.
   */
  public get connections(): TaskItem[] {
    return this._connections;
  }

  /**
   * Adds task items as connections to this.
   * @param connections One or more task items to add as connections.
   */
  public addConnections(...connections: TaskItem[]): void {
    this._connections.push(...connections);
  }
}
