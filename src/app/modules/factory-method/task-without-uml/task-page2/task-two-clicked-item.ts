// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { TaskItem } from './models/task-item';

/**
 * Serves as a helper to {@link TaskPage2Component}, containing both the html representation of a clicked
 * task item as well as the task item itself.
 *
 * @author Frederic Bauer
 */
export class TaskTwoClickedItem {
  /**
   * The html representation of the task item.
   * @private
   */
  private readonly _htmlElement: HTMLElement;
  /**
   * The task item that has been clicked.
   * @private
   */
  private readonly _associatedTaskListItem: TaskItem;

  public constructor(htmlElement: HTMLElement, associatedTaskListItem: TaskItem) {
    this._htmlElement = htmlElement;
    this._associatedTaskListItem = associatedTaskListItem;
  }

  /**
   * Getter, which returns the {@link _htmlElement}.
   */
  public get htmlElement(): HTMLElement {
    return this._htmlElement;
  }

  /**
   * Getter, which returns the {@link _associatedTaskListItem}.
   */
  public get associatedTaskListItem(): TaskItem {
    return this._associatedTaskListItem;
  }
}
