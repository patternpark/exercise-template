// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

/**
 * A task description for the second page / task "without uml".
 */
export const FACTORY_TASK_WITHOUT_UML_PAGE2 =
  'Ordne die konkreten Produkte den konkreten Erzeugern zu. Können auch mehrere Produkte durch einen Erzeuger erzeugt werden?';

/**
 * An error message, stating that the user tried to connect items that don't belong together.
 */
export const TASK_PAGE2_WRONG_CONNECTION = 'Diese Elemente ("{0}" und "{1}") gehören nicht zusammen!';

/**
 * An error message, stating that the user tried connect items that have already been connected.
 */
export const TASK_PAGE2_CONNECTION_ALREADY_EXISTS = 'Diese Elemente ("{0}" und "{1}") sind schon verbunden!';

/**
 * A status message, stating that the user successfully connected two items.
 */
export const TASK_PAGE2_CORRECT_CONNECTION = 'Gut gemacht, weiter so! Es sind noch nicht alle Elemente verbunden.';

/**
 * The tip to be displayed on the page (regarding how to interact with the given task).
 */
export const TASK_PAGE2_TIP = 'Um die Felder zu verbinden, klicke erst auf das eine, dann das andere Feld.';

/**
 * Specifies the color of the drawn Leader-Line.
 */
export const LEADER_LINE_COLOR = 'orange';

/**
 * Specifies the strength / width of the drawn line.
 */
export const LEADER_LINE_SIZE = 2;

/**
 * Specifies the line path option.
 */
export const LEADER_LINE_PATH = 'straight';

/**
 * Specifies whether the drawn line will be dashed or not.
 */
export const LEADER_LINE_DASH = true;
