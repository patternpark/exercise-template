import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskPage2Component } from './task-page2.component';

describe('TaskPage2Component', () => {
  let component: TaskPage2Component;
  let fixture: ComponentFixture<TaskPage2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TaskPage2Component],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskPage2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
