// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Component, OnInit } from '@angular/core';
import { TaskItem } from './models/task-item';
import { TaskDescriptionService } from '../../../../shared/services/task-description.service';
import { END_TASK, FACTORY_TASK_WITHOUT_UML_CREATOR_WITH_PRODUCTS, TASK_COMPLETE } from '../constants';
import { ETaskItemType } from '../task-item-type.enum';
import { TaskPageBase } from '../task-page-base';
import { shuffle } from '../../../../shared/utilities/array-utility';
import { TaskTwoClickedItem } from './task-two-clicked-item';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { getAnimationDuration, getBorderColorAnimation } from '../../../../shared/animations/user-interactions';
import TaskCompletionItem from './models/task-completion-item';
import { EUserActionResult } from './user-action-result.enum';
import { GlobalEventListenerService } from '../../../../shared/services/global-event-listener.service';
import {
  FACTORY_TASK_WITHOUT_UML_PAGE2,
  LEADER_LINE_COLOR,
  LEADER_LINE_DASH,
  LEADER_LINE_PATH,
  LEADER_LINE_SIZE,
  TASK_PAGE2_CONNECTION_ALREADY_EXISTS,
  TASK_PAGE2_CORRECT_CONNECTION,
  TASK_PAGE2_TIP,
  TASK_PAGE2_WRONG_CONNECTION,
} from './constants';
import { HeaderImageService } from '../../../../shared/services/header-image.service';

// eslint-disable-next-line
declare let LeaderLine: any;

/**
 * Responsible for handling the second task page for the task without uml. This includes dedaling with
 * user interactions through the html page and more.
 *
 * @author Frederic Bauer
 */
@Component({
  selector: 'patternpark-task-page2',
  templateUrl: './task-page2.component.html',
  styleUrls: [
    '../../../../shared/styles/cdk-prettify.scss',
    '../task-without-uml.component.scss',
    './task-page2.component.scss',
  ],
  animations: [
    trigger('clickedTrigger', [
      state(
        'clicked',
        style({
          borderColor: 'orange',
        })
      ),
      state(
        'not-clicked',
        style({
          borderColor: 'black',
        })
      ),
      transition('clicked <=> not-clicked', [animate('0.2s')]),
    ]),
  ],
})
export class TaskPage2Component extends TaskPageBase implements OnInit {
  /**
   * List of creator type task items.
   */
  public creatorTaskItems: TaskItem[] = [];
  /**
   * List of product type task items.
   */
  public productTaskItems: TaskItem[] = [];
  /**
   * Item representing the currently selected item of the user.
   */
  public currentlySelectedItem: TaskTwoClickedItem | null = null;
  /**
   * Instance of the {@link TaskCompletionItem}, handling multiple checks regarding the task.
   * @private
   */
  private _taskCorrection: TaskCompletionItem;
  /**
   * List of created leader-line objects.
   * @private
   */
  // eslint-disable-next-line
  private readonly _drawnLeaderLines: any[] = [];
  /**
   * List of created leader-line html representations.
   * @private
   */
  private readonly _htmlDrawnLeaderLines: HTMLElement[] = [];

  constructor(
    private _taskDescriptorService: TaskDescriptionService,
    private _globalEventListenerService: GlobalEventListenerService,
    private readonly _headerImageService: HeaderImageService
  ) {
    super();
    this._taskCorrection = new TaskCompletionItem();

    this._headerImageService.headerIcon = 'assets/factory-method/header-icon.png';

    this._taskDescriptorService.taskDescription = FACTORY_TASK_WITHOUT_UML_PAGE2;
    this._taskDescriptorService.taskDescriptionTip = TASK_PAGE2_TIP;
    this._taskDescriptorService.navigationVisibility = true;
    this._taskDescriptorService.navigationNextItem = {
      url: 'factory-method/without-uml/task2',
      isEnabled: false,
      isVisible: false,
    };
    this._taskDescriptorService.navigationPreviousItem = {
      url: 'factory-method/without-uml/task1',
      isEnabled: false,
      isVisible: true,
    };
  }

  ngOnInit(): void {
    this.fillLists();
    this._taskCorrection.creators = this.creatorTaskItems;
    /**
     * Subscribe to the resize event. Reposition the drawn Leader-Lines in accordance to the results
     * of the event (so that they are still in the right place even after resizing).
     */
    this._globalEventListenerService.resizeEvent.subscribe(() => {
      for (const line of this._drawnLeaderLines) {
        line.position();
      }

      // Correct the incorrect transposition based on html inheritance.
      for (const htmlLine of this._htmlDrawnLeaderLines) {
        let parentBoundingRec = htmlLine.parentElement?.getBoundingClientRect();
        if (!parentBoundingRec) continue;
        htmlLine.style.transform = `translate(${parentBoundingRec.left * -1}px,${parentBoundingRec.top * -1}px)`;
      }
    });
  }

  /**
   * Deals with the click event on task items, therefore dealing with the users
   * actions in the context of the given task.
   * @param event The click event.
   * @param taskItem The item belonging to the clicked html representation.
   * @param taskContainer The element which contains the clicked item.
   */
  connectClick(event: MouseEvent, taskItem: TaskItem, taskContainer: HTMLElement): void {
    if (!event || !event.currentTarget || this.currentlySelectedItem?.htmlElement === event.target) return;

    const clickedItem: TaskTwoClickedItem = new TaskTwoClickedItem(event.currentTarget as HTMLElement, taskItem);

    if (!this.currentlySelectedItem) {
      this.currentlySelectedItem = clickedItem;
      return;
    }
    clickedItem.htmlElement.animate(getBorderColorAnimation('orange'), getAnimationDuration(1500));

    const clickedItems = this.reorganizeToCreatorAndProduct(this.currentlySelectedItem, clickedItem);

    // Try to add the connection between the two clicked items.
    const uActionResult: EUserActionResult = this._taskCorrection.addConnection(
      clickedItems[0].associatedTaskListItem,
      clickedItems[1].associatedTaskListItem
    );
    if (uActionResult === EUserActionResult.correctConnection) {
      this.drawLine(clickedItems[0].htmlElement, clickedItems[1].htmlElement, taskContainer);
    }

    // Adapt the displayed task tip accordingly
    this.setTaskTip(
      uActionResult,
      clickedItems[0].associatedTaskListItem,
      clickedItems[1].associatedTaskListItem,
      this._taskCorrection.checkIfAllConnectionsSet()
    );

    this.currentlySelectedItem = null;
  }

  /**
   * Fills the list of task items for the visual representation of the task. Also
   * deals with shuffling the lists to ensure a different configuration most times.
   * @private
   */
  private fillLists(): void {
    const arrayOfTaskItems: TaskItem[] = this.splitCreatorsFromProducts(FACTORY_TASK_WITHOUT_UML_CREATOR_WITH_PRODUCTS);

    this.creatorTaskItems = arrayOfTaskItems.filter((item) => item.taskType === ETaskItemType.CREATOR);
    this.productTaskItems = arrayOfTaskItems.filter((item) => item.taskType === ETaskItemType.PRODUCT);
    shuffle(this.creatorTaskItems);
    shuffle(this.productTaskItems);
  }

  /**
   * Sets the task description tip based on the result of a user's action.
   * @param userActionResult The result of a user's action.
   * @param creator The clicked creator item (or first item, if two products have been clicked).
   * @param product The clicked product item (or second item, if two creators have been clicked).
   * @param taskCompleted Stating whether the task has been completed successfully already (optional).
   * @private
   */
  private setTaskTip(
    userActionResult: EUserActionResult,
    creator: TaskItem,
    product: TaskItem,
    taskCompleted: boolean = false
  ): void {
    if (taskCompleted) {
      this._taskDescriptorService.taskDescriptionTip = `${TASK_COMPLETE} ${END_TASK}`;
      return;
    }

    let taskTipString: string;

    if (userActionResult === EUserActionResult.correctConnection) {
      taskTipString = TASK_PAGE2_CORRECT_CONNECTION;
    } else if (userActionResult === EUserActionResult.incorrectConnection) {
      taskTipString = TASK_PAGE2_WRONG_CONNECTION;
    } else {
      taskTipString = TASK_PAGE2_CONNECTION_ALREADY_EXISTS;
    }

    this._taskDescriptorService.taskDescriptionTip = taskTipString.format(creator.name, product.name);
  }

  /**
   * Reorganizes the given items into a list with the desired order (with the creator being the first
   * item and the product only the second). Otherwise simply switches the order of the items if
   * only items of the same type have been supplied.
   * @param firstItem The first item supplied to the function.
   * @param secondItem The second item supplied to the function
   * @private
   * @returns TaskTwoClickedItem[] - The list of the items in the new order.
   */
  private reorganizeToCreatorAndProduct(
    firstItem: TaskTwoClickedItem,
    secondItem: TaskTwoClickedItem
  ): TaskTwoClickedItem[] {
    const creatorProductCarrier = [firstItem, secondItem];
    if (firstItem.associatedTaskListItem.taskType !== ETaskItemType.CREATOR) {
      creatorProductCarrier.splice(0, 1);
      creatorProductCarrier.push(firstItem);
    }

    return creatorProductCarrier;
  }

  /**
   * Creates Leader-Lines between the given objects.
   * @param firstClickedElement The first clicked html item (the origin of the Leader-Line).
   * @param secondClickedElement The second clicked html item (the end of the Leader-Line).
   * @param taskContainer The html element containing the two clicked items.
   * @private
   */
  private drawLine(
    firstClickedElement: HTMLElement,
    secondClickedElement: HTMLElement,
    taskContainer: HTMLElement
  ): void {
    const leaderLine = new LeaderLine(firstClickedElement, secondClickedElement, {
      color: LEADER_LINE_COLOR,
      size: LEADER_LINE_SIZE,
      path: LEADER_LINE_PATH,
      dash: LEADER_LINE_DASH,
    });
    // Retrieve the Leader-Line html element
    let createdLine = <HTMLElement>document.querySelector('body > .leader-line');
    const taskContainerBoundingRec = taskContainer.getBoundingClientRect();

    // Move the Leader-Line html element into the container and correct its positioning on the page.
    taskContainer.appendChild(createdLine);
    leaderLine.position();
    createdLine.style.transform = `translate(${taskContainerBoundingRec.left * -1}px,${
      taskContainerBoundingRec.top * -1
    }px)`;
    this._drawnLeaderLines.push(leaderLine);
    this._htmlDrawnLeaderLines.push(createdLine);
  }
}
