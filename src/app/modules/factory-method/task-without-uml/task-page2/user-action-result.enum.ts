// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

/**
 * Represents the result of a user's action in the {@link TaskPage2Component}.
 *
 * @author Frederic Bauer
 */
export enum EUserActionResult {
  /**
   * States that a connection has been set correctly.
   */
  correctConnection = 0,
  /**
   * States that an connection was incorrect.
   */
  incorrectConnection = 1,
  /**
   * States that a connection between the items already exists.
   */
  connectionAlreadyExists = 2,
}
