import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskWithoutUmlComponent } from './task-without-uml.component';

describe('TaskWithoutUmlComponent', () => {
  let component: TaskWithoutUmlComponent;
  let fixture: ComponentFixture<TaskWithoutUmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TaskWithoutUmlComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskWithoutUmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
