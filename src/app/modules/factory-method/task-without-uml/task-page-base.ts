// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { TaskItem } from './task-page2/models/task-item';

/**
 * The base template for both tasks without uml. Contains functionality to be available in both task
 * items.
 *
 * @author Frederic Bauer
 */
export class TaskPageBase {
  /**
   * Splits an array of creators with connected products into an array with both (separated) items together.
   * @param items Array of creators from which to split the connected products.
   * @protected
   * @returns TaskItem[] - The array containing both creators and their products.
   */
  protected splitCreatorsFromProducts(items: TaskItem[]): TaskItem[] {
    const allItems: TaskItem[] = [];

    for (const item of items) {
      allItems.push(item, ...item.connections);
    }

    return allItems;
  }

  /**
   * Determines the total amount of products associated with the list of creators given to the function.
   * @param items Array of creators to check for the amount of products.
   * @protected
   * @returns number - The number of products in connection to the given creators.
   */
  protected getNumberOfAllProducts(items: TaskItem[]): number {
    let amountOfProducts = 0;
    for (const item of items) {
      amountOfProducts += item.connections.length;
    }

    return amountOfProducts;
  }
}
