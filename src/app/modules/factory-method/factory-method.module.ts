// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer, Christian Hemp, Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de, Christian.Hemp [at] hochschule-stralsund.de,
//                           contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskWithUmlComponent } from './task-with-uml/task-with-uml.component';
import { TaskWithoutUmlComponent } from './task-without-uml/task-without-uml.component';
import { TaskPage1Component } from './task-without-uml/task-page1/task-page1.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { UiComponentsModule } from '../../core/modules/ui-components.module';
import { TaskPage2Component } from './task-without-uml/task-page2/task-page2.component';
import { ClassDiagramComponent } from './task-with-uml/class-diagram/class-diagram.component';
import { SequenceDiagramComponent } from './task-with-uml/sequence-diagram/sequence-diagram.component';
import { DiagramService } from './task-with-uml/services/diagram.service';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    TaskWithUmlComponent,
    TaskWithoutUmlComponent,
    TaskPage1Component,
    TaskPage2Component,
    ClassDiagramComponent,
    SequenceDiagramComponent,
  ],
  imports: [CommonModule, DragDropModule, UiComponentsModule, RouterModule],
  exports: [TaskWithUmlComponent, TaskPage1Component, TaskPage2Component, TaskWithoutUmlComponent],
  providers: [DiagramService],
})
export class FactoryMethodModule {}
