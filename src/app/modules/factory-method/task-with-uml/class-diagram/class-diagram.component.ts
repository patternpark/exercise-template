// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Christian Hemp
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 christian.hemp[at]hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../../../../core/modules/dialog/dialog.component';
import {
  ACCESS_MODIFIER_ERROR,
  CLASSDIAGRAM_LEGEND_TITLE,
  DIAGRAM_ID,
  DIALOG_DISCRIPTION,
  DIALOG_LABEL,
  DIALOG_TITLE,
  DROPDOWN_DEST_INSTACE,
  DROPDOWN_DIALOG_DESCRIPTION,
  DROPDOWN_DIALOG_TITLE,
  DROPDOWN_SOURCE_INSTANCE,
  FACTORY_METHOD_UML_CLASS_DIAGRAM,
  UML_TASK_TEXT,
  UNNECESSARY_CONSTRUCTOR,
} from '../constants';
import { DiagramService } from '../services/diagram.service';
import { DialogData } from '../../../../core/modules/dialog/models/dialog-data';
import MermaidWrapperClassDiagram from '../../../../shared/mermaid-wrapper/class-diagram/mermaid-wrapper-class-diagram';
import { DialogInput } from '../models/dialog-input';
import { DropdownDialogComponent } from '../../../../core/modules/dropdown-dialog/dropdown-dialog.component';
import { DropdownDialogInput } from '../models/dropdown-dialog-input';
import { DropdownDialogData } from '../../../../core/modules/dropdown-dialog/models/dropdown-dialog-data';
import { InstanceType } from '../../../../core/modules/dropdown-dialog/models/instance.type';
import { TaskDescriptionService } from '../../../../shared/services/task-description.service';
import { EAccessModifier } from '../../../../shared/mermaid-wrapper/class-diagram/access-modifier.enum';
import ClassFunction from '../../../../shared/mermaid-wrapper/class-diagram/class-function';
import { Subscription } from 'rxjs';
import mermaid from 'mermaid';

/**
 * @author Christian Hemp
 * @email christian.hemp[at]hochschule-stralsund.de
 */
@Component({
  selector: 'patternpark-class-diagram',
  templateUrl: './class-diagram.component.html',
  styleUrls: ['./class-diagram.component.scss'],
})
export class ClassDiagramComponent implements AfterViewInit, OnInit, OnDestroy {
  /**
   * Elementreference to the div that contains the mermaid class diagram.
   */
  @ViewChild('mermaid', { static: true })
  public mermaidDiv?: ElementRef<HTMLDivElement>;
  /**
   * Title for the fieldbox legend
   */
  public legend = CLASSDIAGRAM_LEGEND_TITLE;
  /**
   * Contains all created instances with name and classname.
   */
  private _createdInstances: InstanceType[] = [];
  /**
   * Holds all subscriptions made.
   */
  private _subscriptions: Subscription[] = [];

  constructor(
    private _diagramService: DiagramService,
    private _dialogService: MatDialog,
    private _taskDescriptionTipService: TaskDescriptionService
  ) {
    this._taskDescriptionTipService.taskDescription = UML_TASK_TEXT;
  }

  ngOnInit(): void {
    this._checkCreatedInstances();
  }

  ngAfterViewInit(): void {
    mermaid.init('.mermaid');
    this._initMermaidWrapper();
  }

  /**
   * Opens a dialog with the given data and reacts on the response when the dialog is closed.
   * @param data Data that is used to be displayed in the dialog component.
   */
  openDialog(data: DialogInput): void {
    const dialogRef = this._dialogService.open(DialogComponent, {
      data: {
        title: DIALOG_TITLE,
        description: DIALOG_DISCRIPTION.format(data.classAttr.holder.name),
        labelText: DIALOG_LABEL,
        labelValue: data.objectName,
      } as DialogData,
    });

    this._subscriptions.push(
      dialogRef.afterClosed().subscribe((result: DialogData) => {
        if (!result) return; // clicked on "abort"

        this._createdInstances.push({
          class: data.classAttr.holder,
          instanceName: result.labelValue,
        });
        this._diagramService.setCreatedInstance(this._createdInstances);
        this._diagramService.setDiagramElement({
          ...data,
          objectName: result.labelValue,
        });
      })
    );
  }

  /**
   * Opens the dropdown dialog component and handles the response when the dialog is closed.
   * @param data
   */
  openDropdownDialog(data: DropdownDialogInput) {
    const dialogRef = this._dialogService.open(DropdownDialogComponent, {
      data: {
        title: DROPDOWN_DIALOG_TITLE,
        description: DROPDOWN_DIALOG_DESCRIPTION.format(data.method.name),
        sourceLabelText: DROPDOWN_SOURCE_INSTANCE,
        endLabelText: DROPDOWN_DEST_INSTACE,
        labelValue: '',
        availableInstances: this._diagramService.createdInstances$,
        method: data.method,
      } as DropdownDialogData,
    });

    this._subscriptions.push(
      dialogRef
        .afterClosed()
        .subscribe((result: { sourceInstance: InstanceType; endInstance: InstanceType; method: ClassFunction }) => {
          if (!result) return; // clicked on "abort"
          if (result.method.accessModifier !== EAccessModifier.public) {
            if (result.endInstance !== result.sourceInstance) {
              this._taskDescriptionTipService.taskDescriptionTip = ACCESS_MODIFIER_ERROR;
              return;
            }
          }
          this._diagramService.setSequenceAction({
            ...data,
            selectedEndInstace: result.endInstance,
            selectedSourceInstance: result.sourceInstance,
          });
        })
    );
  }
  /**
   * Subscription clean up after the component is destroyed.
   */
  ngOnDestroy(): void {
    for (let sub of this._subscriptions) sub.unsubscribe();
  }

  /**
   * Inits the awesome mermaid wrapper and draws the diagram.
   * @returns
   */
  private _initMermaidWrapper(): void {
    if (!this.mermaidDiv) return;

    const wrapper = new MermaidWrapperClassDiagram(
      FACTORY_METHOD_UML_CLASS_DIAGRAM.getMermaidString(),
      DIAGRAM_ID,
      this.mermaidDiv.nativeElement
    );
    this._subscriptions.push(
      wrapper.drawnEvent.subscribe(() => {
        this.setEventListenersOnCtors(wrapper);
        this.setEventListenersOnFunctions(wrapper);
      })
    );
    wrapper.redraw();
  }

  /**
   * Listens to changes on the createdInstances observable.
   */
  private _checkCreatedInstances(): void {
    this._subscriptions.push(
      this._diagramService.createdInstances$.subscribe((createdInstances) => {
        this._createdInstances = createdInstances;
      })
    );
  }
  /**
   * Sets the clickListener for each constructor of all classes from the wrapper.
   * @param wrapper Awesome wrapper that holds all classes.
   */
  private setEventListenersOnCtors(wrapper: MermaidWrapperClassDiagram): void {
    wrapper.classes.forEach((cls) => {
      cls.constructors.forEach((ctor) => {
        ctor.htmlElement.classList.add('pointable');
        ctor.htmlElement.addEventListener('click', () => {
          if (!cls.isAbstract)
            this.openDialog({
              objectName: '',
              methodName: ctor.name,
              classAttr: ctor,
            });
          else this._taskDescriptionTipService.taskDescriptionTip = UNNECESSARY_CONSTRUCTOR.format(cls.name);
        });
      });
    });
  }

  /**
   * Sets all clickListener for each method on all given classes.
   * @param wrapper Awesome wrapper that holds all classes.
   */
  private setEventListenersOnFunctions(wrapper: MermaidWrapperClassDiagram): void {
    wrapper.classes.forEach((cls) => {
      cls.functions.forEach((fcntn) => {
        fcntn.htmlElement.classList.add('pointable');
        fcntn.htmlElement.addEventListener('click', () => {
          this.openDropdownDialog({
            method: fcntn,
          });
        });
      });
    });
  }
}
