// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Christian Hemp
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 christian.hemp[at]hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { InstanceType } from '../../../../core/modules/dropdown-dialog/models/instance.type';
import MermaidClass from '../../../../shared/mermaid-wrapper/class-diagram/mermaid-class';
import { DialogInput } from '../models/dialog-input';
import { DropdownDialogInput } from '../models/dropdown-dialog-input';

/**
 * Service that is used to share data between {@link ClassDiagramComponent} and {@link SequenceDiagramComponent}.
 */
@Injectable()
export class DiagramService {
  /**
   * Grants access to the selected classdiagram element that is clicked as Observable.
   */
  public readonly selectedDiagramElement$: Observable<DialogInput>;
  /**
   * Grants access to the action that the user wants to perform as Observable.
   */
  public readonly selectedSequenceAction$: Observable<DropdownDialogInput>;
  /**
   * Grants access to all created instances as Observable.
   */
  public readonly createdInstances$: Observable<InstanceType[]>;

  /**
   * Holds the selected classdiagram element that is clicked as Subject.
   */
  private _selectedDiagramElement$$ = new Subject<DialogInput>();
  /**
   * Holds the action that the user wants to perform as Subject.
   */
  private _selectedSequenceAction$$ = new Subject<DropdownDialogInput>();
  /**
   * Holds all created instances as BehavioursSubject.
   */
  private _createdInstances$$ = new BehaviorSubject<InstanceType[]>([]);
  /**
   * Holds all created instances as a local array
   */
  private _createdInstances: InstanceType[] = [];

  public constructor() {
    this.selectedDiagramElement$ = this._selectedDiagramElement$$.asObservable();
    this.selectedSequenceAction$ = this._selectedSequenceAction$$.asObservable();
    this.createdInstances$ = this._createdInstances$$.asObservable();
  }

  /**
   * Sets the clicked constructor.
   * @param currentDiagramElement current diagram element that holds information about classname method and instance name.
   */
  public setDiagramElement(currentDiagramElement: DialogInput) {
    this._selectedDiagramElement$$.next(currentDiagramElement);
  }

  /**
   * Sets the action a user wants to preform.
   * @param selectedSequenceAction selected action that is clicked.
   */
  public setSequenceAction(selectedSequenceAction: DropdownDialogInput) {
    this._selectedSequenceAction$$.next(selectedSequenceAction);
  }

  /**
   * Sets the createdInstances array.
   * @param createdInstances all created instances
   */
  public setCreatedInstance(createdInstances: InstanceType[]) {
    this._createdInstances = createdInstances;
    this._createdInstances$$.next(createdInstances);
  }

  /**
   * Deletes a created instance from createdInstances.
   * @param toBeDeletedInstanceName instance name of the object to delete
   * @param className class name of the object to delete
   */
  public deleteCreatedInstance(toBeDeletedInstanceName: string, className: MermaidClass) {
    const newInstanceArray = this._createdInstances.filter(
      (instance: InstanceType) =>
        instance.class.name !== className.name || instance.instanceName !== toBeDeletedInstanceName
    );
    this.setCreatedInstance(newInstanceArray);
  }
}
