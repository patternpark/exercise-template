// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Christian Hemp
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 christian.hemp[at]hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

/**
 * Represents an action that can be displayed in a {@link SequenceDiagramComponent}.
 */
export enum ACTION {
  /**
   * String that represents a request in mermaid.
   */
  request = '->>+',
  /**
   * String that represents a answer in mermaid.
   */
  answer = '-->>-',
}
