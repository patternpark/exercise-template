// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Christian Hemp
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 christian.hemp[at]hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //
import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import mermaid from 'mermaid';
import { Subject, Subscription } from 'rxjs';
import { TaskDescriptionService } from '../../../../shared/services/task-description.service';
import {
  ACCESS_MODIFIER_ERROR,
  ALL_ORDERS_COMPLETED,
  AM_ENDE_WIRD_GEGESSEN,
  BRATNUDELN,
  BRATNUDELN_CONSTRUCTOR,
  CHINA_IMBISS,
  CHINAIMBISS_CONSTRUCTOR,
  FALSE_IMBISS,
  FALSE_IMBISS_METHOD,
  FOOD_LIMITATION,
  HANDLE_ONE_ORDER_AT_A_TIME_ERROR,
  KUNDE,
  KUNDE_CANT_DELIVER_FOOD,
  KUNDE_FIRST,
  KUNDE_ISST,
  KUNDE_REQUESTS_FOOD_ERROR,
  MULTIPLE_CTORS,
  NO_FOOD_WITHOUT_IMBISS,
  PIZZA600,
  PIZZA_CONSTRUCTOR,
  PIZZERIA,
  PIZZERIA_CONSTRUCTOR,
  PREPARE_MEAL_FUNCTION,
  PRODUCT_BEFORE_ORDER_ERROR,
  SEQSTART,
  SEQUENCEDIAGRAM_LEGEND_TITLE,
  SINGLE_CTOR,
  SUCCESS_MESSAGE,
  SUCCESS_PARTIAL_TASK,
  TASK_TIP,
} from '../constants';
import { DiagramService } from '../services/diagram.service';
import { DialogInput } from '../models/dialog-input';
import ClassConstructor from '../../../../shared/mermaid-wrapper/class-diagram/class-constructor';
import { ACTION } from './sequence-action.enum';
import ClassMember from '../../../../shared/mermaid-wrapper/class-diagram/class-member';
import ClassFunction from '../../../../shared/mermaid-wrapper/class-diagram/class-function';
import MermaidClass from '../../../../shared/mermaid-wrapper/class-diagram/mermaid-class';
import { EAccessModifier } from '../../../../shared/mermaid-wrapper/class-diagram/access-modifier.enum';
import { DropdownDialogInput } from '../models/dropdown-dialog-input';

/**
 * @author Christian Hemp
 * @email christian.hemp[at]hochschule-stralsund.de
 */
@Component({
  selector: 'patternpark-sequence-diagram',
  templateUrl: './sequence-diagram.component.html',
  styleUrls: ['./sequence-diagram.component.scss'],
})
export class SequenceDiagramComponent implements AfterViewInit, OnInit, OnDestroy {
  /**
   * HTML element that contains the rendered diagram.
   */
  @ViewChild('mermaid2', { static: true })
  public mermaidDiv: ElementRef<HTMLDivElement> | undefined;
  /**
   * Title for the squencediagram.
   */
  public legend = SEQUENCEDIAGRAM_LEGEND_TITLE;
  /**
   * subscription array that holds all subscription.
   */
  private _subscriptions: Subscription[] = [];
  /**
   * Object array that holds all created instances with name and classname.
   */
  private _createdCtors: { class: ClassConstructor; instanceName: string }[] = [];
  /**
   * Array that holds submited actions.
   */
  private _actionList: ClassMember[] = [];
  /**
   * String that is parsed to a sequence diagram by mermaid.
   */
  private _graphinfo = SEQSTART;
  /**
   * Limitation dictionary that returns the evaluation value for specific actions and instances.
   */
  private _maxAmountOfInstances: {
    [key: string]: number;
  } = {
    [PIZZA600.className]: 2,
    [PREPARE_MEAL_FUNCTION.name]: 3,
    [`${PREPARE_MEAL_FUNCTION.name}${PIZZA600.className}`]: 2,
    [`${PREPARE_MEAL_FUNCTION.name}${BRATNUDELN.className}`]: 1,
  };
  /**
   * Requirements Dictionary that return the must have classes for specific instances.
   */
  private _requiredClassesForMethod: {
    [key: string]: string;
  } = {
    [PIZZERIA_CONSTRUCTOR.name]: KUNDE.className,
    [CHINAIMBISS_CONSTRUCTOR.name]: KUNDE.className,
    [PIZZA_CONSTRUCTOR.name]: PIZZERIA.className,
    [BRATNUDELN_CONSTRUCTOR.name]: CHINA_IMBISS.className,
    [`${PREPARE_MEAL_FUNCTION.name}${PIZZERIA.className}`]: PIZZERIA.className,
    [`${PREPARE_MEAL_FUNCTION.name}${CHINA_IMBISS.className}`]: CHINA_IMBISS.className,
    [`${KUNDE_ISST.name}${KUNDE.className}`]: KUNDE.className,
  };
  /**
   * Subject that holds information about a failed constructor.
   */
  private _failedConstructor$$ = new Subject<{
    instanceName: string;
    class: MermaidClass;
    failed: boolean;
  }>();

  /**
   * Boolean that indicates wether the task ist finished.
   */
  private _finished: boolean = false;
  /**
   * Number of action the user must do.
   */
  private _actionNumberWanted = 9;

  constructor(private _diagramService: DiagramService, private _taskDescriptionService: TaskDescriptionService) {}

  ngOnInit(): void {
    this._taskDescriptionService.taskDescriptionTip = TASK_TIP;
    this.checkConstructor();
    this.checkMethod();
    this.checkFailedConstructor();
  }

  ngAfterViewInit(): void {
    if (!this.mermaidDiv || !this.mermaidDiv.nativeElement) return;

    mermaid.init('.mermaid');
    this.rerender();
  }
  ngOnDestroy(): void {
    // unsubscribes each subscrition after component is destroyed.
    for (let sub of this._subscriptions) sub.unsubscribe();
  }

  /**
   * Rerenders the sequence diagram and sets the innerHTML of the mermaidDiv.
   */
  private rerender() {
    mermaid.render('seq-task1-factory-method', this._graphinfo, (svgCode: string) => {
      if (!this.mermaidDiv || !this.mermaidDiv.nativeElement) return;

      this.mermaidDiv.nativeElement.innerHTML = svgCode;
    });
  }

  /**
   * Checks whether the clicked constructor can be executed.
   */
  private checkConstructor(): void {
    this._subscriptions.push(
      this._diagramService.selectedDiagramElement$.subscribe((element: DialogInput) => {
        if (this._finished) return;

        if (element.classAttr instanceof ClassConstructor) {
          const ctor: ClassConstructor = element.classAttr;
          const foundCtors = this._createdCtors.filter((ctorElement) => ctorElement.class === ctor);
          const limitCtors: number = this._maxAmountOfInstances[element.classAttr.holder.name] ?? 1;
          const requiredConstructor = this._createdCtors.find(
            (createdCtor) => this._requiredClassesForMethod[element.classAttr.name] === createdCtor.class.holder.name
          );

          this.dealWithConstructorAddition(foundCtors, limitCtors, ctor, element, requiredConstructor);
          return;
        }
        this._actionList.push(element.classAttr);
      })
    );
  }

  private dealWithConstructorAddition(
    foundCtors: { class: ClassConstructor; instanceName: string }[],
    limitCtors: number,
    ctorToAdd: ClassConstructor,
    element: DialogInput,
    requiredConstructor: { class: ClassConstructor; instanceName: string } | undefined
  ) {
    if (!requiredConstructor && this._requiredClassesForMethod[element.classAttr.name]) {
      const nextDescription: string =
        this._requiredClassesForMethod[element.classAttr.name] === KUNDE.className
          ? KUNDE_FIRST
          : NO_FOOD_WITHOUT_IMBISS;

      this._taskDescriptionService.taskDescriptionTip = nextDescription.format(
        this._requiredClassesForMethod[element.classAttr.name],
        element.classAttr.holder.name
      );

      this.triggerFailedConstructor(element);
      return;
    }

    if (foundCtors.length === limitCtors) {
      this.dealWithConstructorsUpToLimit(limitCtors, ctorToAdd, element);
      return;
    } else if (this._createdCtors.find((ctorElement) => ctorElement.instanceName === element.objectName)) {
      this.dealWithConstructorsUpToLimit(0, ctorToAdd, element);
      return;
    }

    if (
      (ctorToAdd.holder.name === PIZZA600.className || ctorToAdd.holder.name === BRATNUDELN.className) &&
      (this._actionList[this._actionList.length - 1]?.name !== PREPARE_MEAL_FUNCTION.name ||
        (this._actionList[this._actionList.length - 1] as ClassFunction).returnType !== ctorToAdd.holder.name ||
        (this._actionList[this._actionList.length - 2] as ClassFunction)?.name === PREPARE_MEAL_FUNCTION.name)
    ) {
      this._taskDescriptionService.taskDescriptionTip = PRODUCT_BEFORE_ORDER_ERROR.format(ctorToAdd.holder.name);
      this.triggerFailedConstructor(element);
      return;
    }

    this._graphinfo = `${this._graphinfo}\nparticipant(${ctorToAdd.holder.name}) | ${element.objectName}`;
    this._createdCtors.push({
      class: ctorToAdd,
      instanceName: element.objectName,
    });

    this.buildMermaidStringForConstructor(element, requiredConstructor);
    this._actionList.push(ctorToAdd);
    this._taskDescriptionService.taskDescriptionTip = SUCCESS_PARTIAL_TASK;
    this.rerender();
  }

  private dealWithConstructorsUpToLimit(limitCtors: number, ctor: ClassConstructor, element: DialogInput): void {
    this._taskDescriptionService.taskDescriptionTip =
      limitCtors <= 1
        ? SINGLE_CTOR.format(ctor.holder.name)
        : MULTIPLE_CTORS.format(ctor.holder.name, limitCtors.toString());

    this.triggerFailedConstructor(element);
  }

  private triggerFailedConstructor(element: DialogInput): void {
    this._failedConstructor$$.next({
      instanceName: element.objectName,
      class: element.classAttr.holder,
      failed: true,
    });
  }

  private buildMermaidStringForConstructor(
    element: DialogInput,
    requiredConstructor: { class: ClassConstructor; instanceName: string } | undefined
  ): void {
    if (
      element.classAttr.holder.name !== PIZZERIA.className &&
      element.classAttr.holder.name !== CHINA_IMBISS.className &&
      requiredConstructor
    ) {
      this._graphinfo = `${this._graphinfo}\n(${this._requiredClassesForMethod[element.classAttr.name]}) | ${
        requiredConstructor.instanceName
      } ${ACTION.request} (${element.classAttr.holder.name}) | ${element.objectName}: ${element.methodName}\n(${
        element.classAttr.holder.name
      }) | ${element.objectName} ${ACTION.answer} (${this._requiredClassesForMethod[element.classAttr.name]}) | ${
        requiredConstructor.instanceName
      }: ${element.methodName}\n(${this._requiredClassesForMethod[element.classAttr.name]}) | ${
        requiredConstructor.instanceName
      } ${ACTION.answer} (${KUNDE.className}) | ${
        this._createdCtors.find((createdCtor) => createdCtor.class.holder.name === KUNDE.className)?.instanceName
      }: ${
        requiredConstructor.class.holder.functions.find((fcn) => fcn.name.includes(PREPARE_MEAL_FUNCTION.name))?.name
      }`;
    }
  }

  /**
   * Checks whether the user wanted class function can be performed.
   */
  private checkMethod(): void {
    this._subscriptions.push(
      this._diagramService.selectedSequenceAction$.subscribe((action) => {
        if (this._finished) return;

        const foundClient = this._createdCtors.find((ctor) => ctor.class.holder.name === KUNDE.className);

        if (
          !foundClient ||
          !action.selectedEndInstace ||
          !action.selectedSourceInstance ||
          this._requiredClassesForMethod[`${action.method.name}${action.method.holder.name}`] !==
            action.method.holder.name
        ) {
          this._taskDescriptionService.taskDescriptionTip = FALSE_IMBISS.format(action.method.holder.name);
          return;
        }

        if (
          this._actionList.filter((fcntn) => fcntn.name === PREPARE_MEAL_FUNCTION.name).length ===
          this._maxAmountOfInstances[action.method.name]
        ) {
          this._taskDescriptionService.taskDescriptionTip = ALL_ORDERS_COMPLETED;
          return;
        }

        if (
          this._actionList.filter(
            (method) => method instanceof ClassFunction && method.returnType === action.method.returnType
          ).length >= this._maxAmountOfInstances[`${action.method.name}${action.method.returnType}`]
        ) {
          this._taskDescriptionService.taskDescriptionTip = FOOD_LIMITATION.format(
            action.method.holder.name,
            action.method.returnType
          );
          return;
        }

        if (
          action.method.accessModifier !== EAccessModifier.public &&
          action.selectedEndInstace.class !== action.method.holder &&
          action.selectedSourceInstance.class !== action.method.holder
        ) {
          this._taskDescriptionService.taskDescriptionTip = ACCESS_MODIFIER_ERROR;
          return;
        }

        if (action.method.name.includes(PREPARE_MEAL_FUNCTION.name) && !this.checkPrepareMealFunctionCall(action)) {
          return;
        }

        if (action.method.name.includes(KUNDE_ISST.name) && this._actionList.length !== this._actionNumberWanted) {
          this._taskDescriptionService.taskDescriptionTip = AM_ENDE_WIRD_GEGESSEN;
          return;
        }
        this.handleSuccessfulAction(action);

        this._actionList.push(action.method);
        this._graphinfo += `\n(${action.selectedSourceInstance.class.name}) | ${action.selectedSourceInstance.instanceName} ${ACTION.request} (${action.selectedEndInstace.class.name}) | ${action.selectedEndInstace.instanceName}: ${action.method.name}`;
        this.rerender();
      })
    );
  }

  private handleSuccessfulAction(action: DropdownDialogInput) {
    this._finished =
      action.method.name.includes(KUNDE_ISST.name) && this._actionList.length === this._actionNumberWanted;

    this._taskDescriptionService.taskDescriptionTip = this._finished ? SUCCESS_MESSAGE : SUCCESS_PARTIAL_TASK;
  }

  private checkPrepareMealFunctionCall(action: DropdownDialogInput): boolean {
    if (action.selectedSourceInstance?.class.name !== KUNDE.className) {
      this._taskDescriptionService.taskDescriptionTip = KUNDE_REQUESTS_FOOD_ERROR.format(
        action.method.name,
        KUNDE.className
      );
      return false;
    } else if (action.selectedEndInstace?.class.name === KUNDE.className) {
      this._taskDescriptionService.taskDescriptionTip = KUNDE_CANT_DELIVER_FOOD.format(
        KUNDE.className,
        action.method.name
      );
      return false;
    } else if (this._actionList[this._actionList.length - 1].name === action.method.name) {
      this._taskDescriptionService.taskDescriptionTip = HANDLE_ONE_ORDER_AT_A_TIME_ERROR;
      return false;
    } else if (action.method.holder.name !== action.selectedEndInstace?.class.name) {
      this._taskDescriptionService.taskDescriptionTip = FALSE_IMBISS_METHOD.format(
        action.selectedEndInstace!.class.name,
        action.method.returnType
      );
      return false;
    }

    return true;
  }

  /**
   * Checks whether a constructor failed and if, deletes the instance.
   */
  private checkFailedConstructor(): void {
    this._subscriptions.push(
      this._failedConstructor$$.subscribe((combi) => {
        if (combi.failed) {
          this._diagramService.deleteCreatedInstance(combi.instanceName, combi.class);
        }
      })
    );
  }
}
