// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Christian Hemp
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 christian.hemp[at]hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Component } from '@angular/core';
import { HeaderImageService } from '../../../shared/services/header-image.service';

@Component({
  selector: 'patternpark-task-with-uml',
  templateUrl: './task-with-uml.component.html',
  styleUrls: ['./task-with-uml.component.scss'],
})
export class TaskWithUmlComponent {
  constructor(private readonly _headerImageService: HeaderImageService) {
    this._headerImageService.headerIcon = 'assets/factory-method/header-icon.png';
  }
}
