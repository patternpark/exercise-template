import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskWithUmlComponent } from './task-with-uml.component';

describe('TaskWithUmlComponent', () => {
  let component: TaskWithUmlComponent;
  let fixture: ComponentFixture<TaskWithUmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TaskWithUmlComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskWithUmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
