// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Christian Hemp
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 christian.hemp[at]hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { EAccessModifier } from '../../../shared/mermaid-wrapper/class-diagram/access-modifier.enum';
import UmlClassConstructorGenerator from '../../../shared/mermaid-wrapper/class-diagram/generator/uml-class-constructor-generator';
import UmlClassFunctionGenerator from '../../../shared/mermaid-wrapper/class-diagram/generator/uml-class-function-generator';
import UmlClassGenerator from '../../../shared/mermaid-wrapper/class-diagram/generator/uml-class-generator';
import UmlClassDiagramGenerator from '../../../shared/mermaid-wrapper/class-diagram/generator/uml-class-diagram-generator';
import { EClassRelationType } from '../../../shared/mermaid-wrapper/class-diagram/generator/class-diagram-relation-type.enum';
import ClassFunctionParameter from '../../../shared/mermaid-wrapper/class-diagram/class-function-parameter';
import { EClassAnnotations } from '../../../shared/mermaid-wrapper/class-diagram/class-annotations.enum';

/**
 * The id of the generated diagram.
 */
export const DIAGRAM_ID = 'cls-task1-factory-method';

/**
 * Dialog strings that are displayed in the dialog component.
 */
export const DIALOG_TITLE = 'Neues Objekt erstellen';

export const DIALOG_DISCRIPTION = 'Wie soll das neue Objekt vom Typ "{0}" heißen?';

export const DIALOG_LABEL = 'Name';

/**
 * Dropdown dialog strings that are displayed in the dropdown dialog component.
 */

export const DROPDOWN_DIALOG_TITLE = 'Neue Aktion erstellen';
export const DROPDOWN_DIALOG_DESCRIPTION = 'Bestimme den Ausführenden und den Aufrufenden von der "{0}" Methode.';
export const DROPDOWN_SOURCE_INSTANCE = 'Aufrufende Instanz';
export const DROPDOWN_DEST_INSTACE = 'Ausführende Instanz';

/**
 * Constructor object for the UML class "Kunde".
 */
export const KUNDE_CONSTRUCTOR = new UmlClassConstructorGenerator('kunde', EAccessModifier.public);

/**
 * "isst" function object for the UML class "Kunde".
 */
export const KUNDE_ISST = new UmlClassFunctionGenerator('isst', EAccessModifier.private);

/**
 * Class object for UML class "Kunde".
 */
export const KUNDE = new UmlClassGenerator('Kunde', [KUNDE_CONSTRUCTOR], [KUNDE_ISST]);

/**
 * Parameter object for "mahlzeitZubereiten" function.
 */
export const SORTE_PARAMETER = new ClassFunctionParameter('sorte', 'string');

/**
 * Abstract function "mahlzeitZubereiten" for UML class "Imbiss".
 */
export const PREPARE_MEAL_FUNCTION = new UmlClassFunctionGenerator(
  'mahlzeitZubereiten',
  EAccessModifier.public,
  [SORTE_PARAMETER],
  'Essen',
  true
);

/**
 * Abstract class object "Imbiss" for UML class "Imbiss".
 */
export const IMBISS = new UmlClassGenerator(
  'Imbiss',
  [],
  [PREPARE_MEAL_FUNCTION, new UmlClassFunctionGenerator('zutatenNachbestellen', EAccessModifier.private, [], 'void')],
  [],
  EClassAnnotations.abstract
);

/**
 * Function object "mahlzeitZubereiten" for UML class "Pizzeria".
 */
export const PIZZA_ZUBEREITEN = new UmlClassFunctionGenerator(
  'mahlzeitZubereiten',
  EAccessModifier.public,
  [SORTE_PARAMETER],
  'Pizza'
);

/**
 * Constructor object "Pizzeria" for UML class "Pizzeria".
 */
export const PIZZERIA_CONSTRUCTOR = new UmlClassConstructorGenerator('pizzeria', EAccessModifier.public);

/**
 * Class object "Pizzeria" for UML class "Pizzeria".
 */
export const PIZZERIA = new UmlClassGenerator('Pizzeria', [PIZZERIA_CONSTRUCTOR], [PIZZA_ZUBEREITEN]);

/**
 * Abstract class "Essen" for UML class "Essen".
 */
export const ESSEN = new UmlClassGenerator('Essen', [], [], [], EClassAnnotations.abstract);

/**
 * Constructor object for UML class "Pizza".
 */
export const PIZZA_CONSTRUCTOR = new UmlClassConstructorGenerator('pizza', EAccessModifier.public);

/**
 * Class object for UML class "Pizza".
 */
export const PIZZA600 = new UmlClassGenerator('Pizza', [PIZZA_CONSTRUCTOR]);

/**
 * Constructor object for UML class "Bratnudeln".
 */
export const BRATNUDELN_CONSTRUCTOR = new UmlClassConstructorGenerator('bratnudeln', EAccessModifier.public);

/**
 * Class object for UML class "Bratnudeln".
 */
export const BRATNUDELN = new UmlClassGenerator('Bratnudeln', [BRATNUDELN_CONSTRUCTOR]);

/**
 * Function object "mahlzeitZubereiten" for UML class "Bratnudeln".
 */
export const BRATNUDELN_ZUBEREITEN = new UmlClassFunctionGenerator(
  'mahlzeitZubereiten',
  EAccessModifier.public,
  [SORTE_PARAMETER],
  'Bratnudeln'
);

/**
 * Constructor object for UML class "ChinaImbiss".
 */
export const CHINAIMBISS_CONSTRUCTOR = new UmlClassConstructorGenerator('chinaImbiss', EAccessModifier.public);

/**
 * Class object for UML class "ChinaImbiss".
 */
export const CHINA_IMBISS = new UmlClassGenerator('ChinaImbiss', [CHINAIMBISS_CONSTRUCTOR], [BRATNUDELN_ZUBEREITEN]);

/**
 * Diagram object for the factory-method.
 */
export const FACTORY_METHOD_UML_CLASS_DIAGRAM = new UmlClassDiagramGenerator([
  KUNDE,
  IMBISS,
  PIZZA600,
  PIZZERIA,
  CHINA_IMBISS,
  BRATNUDELN,
  ESSEN,
]);

/**
 * Relation from "Kunde" to "Imbiss".
 */
FACTORY_METHOD_UML_CLASS_DIAGRAM.addRelationBetweenClasses(KUNDE, IMBISS, EClassRelationType.association);

/**
 * Inheritance from "Imbiss" to "Pizzeria".
 */
FACTORY_METHOD_UML_CLASS_DIAGRAM.addRelationBetweenClasses(IMBISS, PIZZERIA, EClassRelationType.inheritance);

/**
 * Inheritance from "Imbiss" to "ChinaImbiss".
 */
FACTORY_METHOD_UML_CLASS_DIAGRAM.addRelationBetweenClasses(IMBISS, CHINA_IMBISS, EClassRelationType.inheritance);

/**
 * Inheritance from "Essen" to "Pizza".
 */
FACTORY_METHOD_UML_CLASS_DIAGRAM.addRelationBetweenClasses(ESSEN, PIZZA600, EClassRelationType.inheritance);

/**
 * Inheritance from "Essen" to "Bratnudeln".
 */
FACTORY_METHOD_UML_CLASS_DIAGRAM.addRelationBetweenClasses(ESSEN, BRATNUDELN, EClassRelationType.inheritance);

/**
 * Realation from "Pizzeria" to "Pizza".
 */
FACTORY_METHOD_UML_CLASS_DIAGRAM.addRelationBetweenClasses(PIZZERIA, PIZZA600, EClassRelationType.association);

/**
 * Realation from "ChinaImbiss" to "Bratnudeln".
 */
FACTORY_METHOD_UML_CLASS_DIAGRAM.addRelationBetweenClasses(CHINA_IMBISS, BRATNUDELN, EClassRelationType.association);

/**
 * Description text for task with uml.
 */
export const UML_TASK_TEXT =
  'Im Pattern Park gibt es Imbisse, an denen ein Besucher seinen / ihren Hunger stillen kann. Das Ziel ist es, einen solchen Prozess modellhaft mit Hilfe des Sequenzdiagramms nachzustellen. Bereite dem Kunden also zwei Pizzen und eine Portion Bratnudeln zu und hilf dem Kunden, sie zu verspeisen.';

/**
 * String that indicates mermaid to draw a sequence diagram.
 */
export const SEQSTART = 'sequenceDiagram';

/**
 * String that indicates mermaid to draw an instance.
 */
export const SEQKUNDE = '\nparticipant (Kunde)';
export const SEQIMBISS = '\nparticipant (Imbiss)';
export const SEQPIZZA = '\nparticipant (Pizza)';
export const SEQPIZZERIA = '\nparticipant (Pizzeria)';
export const SEQESSEN = '\nparticipant (Essen)';

/**
 * Tips strings.
 */
export const SUCCESS_PARTIAL_TASK = 'Gut gemacht, weiter so! Es sind noch nicht alle Anforderungen erfüllt.';

export const TASK_TIP =
  'Klicke im oberen Klassendiagramm auf die Konstruktoren der Klassen, um davon Objekte im unteren Sequenzdiagramm zu erstellen. Um eine Methode aufzurufen, klicke auf die jeweilige Methode im Klassendiagramm und wähle die ausfürende und aufrufende Instanz aus.';

export const SINGLE_CTOR = 'Es darf nur eine einzige Instanz von "{0}" erstellt werden.';

export const MULTIPLE_CTORS = 'Es dürfen nur "{1}" Instanzen von "{0}" erstellt werden.';

export const KUNDENTIP = 'Es darf nur ein einziger Kunde erstellt werden';

export const PIZZERIATIP = 'Es darf nur einen einzigen Imbiss geben';

export const PIZZENTIP = 'Es dürfen nicht mehr als zwei Mahlzeiten eingefügt werden';

export const UNNECESSARY_CONSTRUCTOR = 'In diesem Zusammenhang wird keine Instanz von "{0}" benötigt.';

export const ACCESS_MODIFIER_ERROR =
  'Die Funktion ist nicht "public", deswegen kann sie nicht von außen aufgerufen werden.';

export const NO_FOOD_WITHOUT_IMBISS = 'Ohne "{0}" kann/können keine "{1}" zubereitet werden.';

export const KUNDE_FIRST = 'Eine Instanz der Klasse "{0}" muss vor der Instanz der Klasse "{1}" erzeugt werden.';

export const FALSE_IMBISS = 'Es fehlen noch Instanzen von der Klasse "{0}".';

export const PRODUCT_BEFORE_ORDER_ERROR =
  'Es muss erst eine Anfrage für die Zubereitung der Mahlzeit "{0}" gestartet werden.';

export const KUNDE_REQUESTS_FOOD_ERROR = 'Die Methode “{0}” kann nur von der Klasse “{1}” verwendet werden.';

export const KUNDE_CANT_DELIVER_FOOD = 'Die Klasse "{0}" hat keine Methode "{1}" ';

export const HANDLE_ONE_ORDER_AT_A_TIME_ERROR = 'Es gibt bereits eine Bestellung die offen ist.';

export const FALSE_IMBISS_METHOD = 'Die Klasse "{0}" kann keine "{1}" zubereiten.';

export const AM_ENDE_WIRD_GEGESSEN = 'Erst nach dem alle Bestellungen abgeschlossen sind, wird gegessen.';

export const SUCCESS_MESSAGE = 'Glückwunsch, die Aufgabe wurde erfolgreich gelöst.';

export const ALL_ORDERS_COMPLETED = 'Alle Bestellungen wurde getätigt.';

export const FOOD_LIMITATION = '"{0}" muss keine "{1}" mehr zubereiten.';

/**
 * Legend strings
 */
export const CLASSDIAGRAM_LEGEND_TITLE = 'Klassendiagramm';
export const SEQUENCEDIAGRAM_LEGEND_TITLE = 'Sequenzdiagramm';
