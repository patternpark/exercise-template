// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Christian Hemp
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 christian.hemp[at]hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import ClassMember from '../../../../shared/mermaid-wrapper/class-diagram/class-member';

/**
 * Interface to declare DialogInput.
 */
export interface DialogInput {
  /**
   * ClassMember that is clicked in the classdiagram.
   */
  classAttr: ClassMember;
  /**
   * Classname of the clicked constructor.
   */
  objectName: string;
  /**
   * Name of the clicked method.
   */
  methodName: string | null;
}
