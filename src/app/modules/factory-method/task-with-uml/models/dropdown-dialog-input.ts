// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Christian Hemp
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 christian.hemp[at]hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { InstanceType } from '../../../../core/modules/dropdown-dialog/models/instance.type';
import ClassFunction from '../../../../shared/mermaid-wrapper/class-diagram/class-function';

/**
 * Interface of dropdownDialog data.
 */
export interface DropdownDialogInput {
  /**
   * Method that is clicked.
   */
  method: ClassFunction;
  /**
   * Instance that calls the method.
   */
  selectedEndInstace?: InstanceType;
  /**
   * Instance that holds the method.
   */
  selectedSourceInstance?: InstanceType;
}
