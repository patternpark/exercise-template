import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MvcTaskWithUmlComponent } from './mvc-task-with-uml/mvc-task-with-uml.component';
import { MvcTaskWithoutUmlComponent } from './mvc-task-without-uml/mvc-task-without-uml.component';
import { UiComponentsModule } from '../../core/modules/ui-components.module';

@NgModule({
  declarations: [MvcTaskWithUmlComponent, MvcTaskWithoutUmlComponent],
  imports: [CommonModule, UiComponentsModule],
  exports: [MvcTaskWithUmlComponent, MvcTaskWithoutUmlComponent],
})
export class MvcModule {}
