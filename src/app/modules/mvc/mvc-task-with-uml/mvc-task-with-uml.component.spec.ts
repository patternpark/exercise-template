import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MvcTaskWithUmlComponent } from './mvc-task-with-uml.component';

describe('MvcTaskWithUmlComponent', () => {
  let component: MvcTaskWithUmlComponent;
  let fixture: ComponentFixture<MvcTaskWithUmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MvcTaskWithUmlComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MvcTaskWithUmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
