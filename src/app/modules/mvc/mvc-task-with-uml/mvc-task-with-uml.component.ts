import { Component, OnInit } from '@angular/core';
import { TaskDescriptionService } from '../../../shared/services/task-description.service';

@Component({
  selector: 'patternpark-mvc-task-with-uml',
  templateUrl: './mvc-task-with-uml.component.html',
  styleUrls: ['./mvc-task-with-uml.component.scss'],
})
export class MvcTaskWithUmlComponent implements OnInit {
  private _defTaskDesc =
    'Ergänze die leeren Felder in dem Klassendiagramm eines Model View Controllers mit den gegebenen Elementen.';
  private _defTaskDescTip = 'Ziehe die Elemente in die passenden Felder.';
  private _tryAgainTaskDescTip =
    'Das war nicht richtig. Versuche es nochmal. Ziehe die Elemente in die passenden Felder.';
  private _correctTaskDescTip = 'Genau. Weiter so. Ziehe die Elemente in die passenden Felder.';
  private _doneTaskDescTip =
    'Du hast es geschafft. Das Klassendiagramm eines Model View Controllers ist nun vollständig.';

  constructor(private _taskDescService: TaskDescriptionService) {
    _taskDescService.taskDescription = this._defTaskDesc;
    _taskDescService.taskDescriptionTip = this._defTaskDescTip;
  }

  ngOnInit(): void {
    return;
  }

  allowDrop(ev: DragEvent) {
    ev.preventDefault();
  }

  drag(ev: DragEvent) {
    ev.dataTransfer?.setData('text', (ev.target as HTMLElement).id);
  }

  drop(ev: DragEvent) {
    ev.preventDefault();

    const data: string = ev.dataTransfer!.getData('text');
    const dragElement: HTMLElement = document.getElementById(data) as HTMLElement;
    const targetElement: HTMLElement = this.getCorrectTargetElement(ev.target as HTMLElement);

    if (
      !targetElement.classList.contains('drop-possible') ||
      !(
        targetElement.id.includes('drop-' + dragElement.id.replace('drag-', '')) ||
        dragElement.id.includes('drag-' + targetElement.id.replace('drop-', ''))
      )
    ) {
      this._taskDescService.taskDescriptionTip = this._tryAgainTaskDescTip;
      return;
    }

    // place element
    targetElement.appendChild(dragElement);

    // correct style
    const droppedElement = targetElement.getElementsByClassName('draggable-element')[0] as HTMLElement;
    droppedElement.classList.remove('draggable-element');
    droppedElement.classList.add('dropped-element');

    if (dragElement.classList.contains('draggable-function')) {
      (dragElement.getElementsByClassName('draggable-element-text')[0] as HTMLElement).style.fontSize = '12px';
    }

    // vanish specific template
    if (targetElement.classList.contains('vanish')) {
      targetElement.getElementsByTagName('svg')[0].style.opacity = '0';
    }

    // prevent another drop
    targetElement.classList.remove('drop-possible');

    // prevent dragging
    dragElement.setAttribute('draggable', 'false');

    this.setTaskTip();
  }

  private setTaskTip() {
    // set tip
    if (
      document.getElementsByClassName('exercise-container-classes')[0].getElementsByClassName('draggable-element')
        .length <= 0
    ) {
      this._taskDescService.taskDescriptionTip = this._doneTaskDescTip;
    } else {
      this._taskDescService.taskDescriptionTip = this._correctTaskDescTip;
    }
  }

  private getCorrectTargetElement(eventTarget: HTMLElement) {
    // set correct element
    for (let i = 0; i < 2; ++i) {
      if ((!eventTarget.classList || !eventTarget.classList.contains('drop-template')) && eventTarget.parentNode) {
        eventTarget = eventTarget.parentNode as HTMLElement;
      }
    }
    return eventTarget;
  }
}
