import { Component, OnInit } from '@angular/core';
import {
  AUFGABENSTELLUNG,
  EXAMPLE_1_DESCRIPTION,
  EXAMPLE_2_DESCRIPTION,
  EXAMPLE_3_DESCRIPTION,
  EXAMPLE_1_ITEM_1,
  EXAMPLE_1_ITEM_2,
  EXAMPLE_1_ITEM_3,
  EXAMPLE_2_ITEM_1,
  EXAMPLE_2_ITEM_2,
  EXAMPLE_2_ITEM_3,
  EXAMPLE_3_ITEM_1,
  EXAMPLE_3_ITEM_2,
  EXAMPLE_3_ITEM_3,
  MODEL_TIP,
  VIEW_TIP,
  CONTROLLER_TIP,
} from './constants';

import { TaskDescriptionService } from '../../../shared/services/task-description.service';

@Component({
  selector: 'patternpark-mvc-task-without-uml',
  templateUrl: './mvc-task-without-uml.component.html',
  styleUrls: ['./mvc-task-without-uml.component.scss'],
})
export class MvcTaskWithoutUmlComponent implements OnInit {
  constructor(private _taskDescService: TaskDescriptionService) {
    _taskDescService.taskDescription = AUFGABENSTELLUNG;
    _taskDescService.taskDescriptionTip = MODEL_TIP;
  }

  public readonly EXAMPLE_1_DESCRIPTION = EXAMPLE_1_DESCRIPTION;
  public readonly EXAMPLE_2_DESCRIPTION = EXAMPLE_2_DESCRIPTION;
  public readonly EXAMPLE_3_DESCRIPTION = EXAMPLE_3_DESCRIPTION;
  public readonly EXAMPLE_1_ITEM_1 = EXAMPLE_1_ITEM_1;
  public readonly EXAMPLE_1_ITEM_2 = EXAMPLE_1_ITEM_2;
  public readonly EXAMPLE_1_ITEM_3 = EXAMPLE_1_ITEM_3;
  public readonly EXAMPLE_2_ITEM_1 = EXAMPLE_2_ITEM_1;
  public readonly EXAMPLE_2_ITEM_2 = EXAMPLE_2_ITEM_2;
  public readonly EXAMPLE_2_ITEM_3 = EXAMPLE_2_ITEM_3;
  public readonly EXAMPLE_3_ITEM_1 = EXAMPLE_3_ITEM_1;
  public readonly EXAMPLE_3_ITEM_2 = EXAMPLE_3_ITEM_2;
  public readonly EXAMPLE_3_ITEM_3 = EXAMPLE_3_ITEM_3;

  evaluateSelection(ev: MouseEvent) {
    ((<HTMLDivElement>ev.target).parentNode!.previousSibling as HTMLElement).innerText = (<HTMLDivElement>(
      ev.target
    )).innerText;
    if (
      ((<HTMLDivElement>ev.target).parentNode!.previousSibling as HTMLElement).dataset.solution ==
      (<HTMLDivElement>ev.target).innerText
    ) {
      ((<HTMLDivElement>ev.target).parentNode!.previousSibling as HTMLElement).style.borderColor = '#44ff44';
    } else {
      ((<HTMLDivElement>ev.target).parentNode!.previousSibling as HTMLElement).style.borderColor = '#ff4444';

      this.setDescriptionTip(
        parseInt(((<HTMLDivElement>ev.target).parentNode!.previousSibling as HTMLElement).dataset.componentnumber!)
      );
    }
  }

  setDescriptionTip(x: number) {
    switch (x) {
      case 1:
        this._taskDescService.taskDescriptionTip = MODEL_TIP;
        break;
      case 2:
        this._taskDescService.taskDescriptionTip = CONTROLLER_TIP;
        break;
      case 3:
        this._taskDescService.taskDescriptionTip = VIEW_TIP;
        break;
      default:
        this._taskDescService.taskDescriptionTip = MODEL_TIP;
        break;
    }
  }

  ngOnInit(): void {
    return;
  }
}
