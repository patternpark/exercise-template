import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MvcTaskWithoutUmlComponent } from './mvc-task-without-uml.component';

describe('MvcTaskWithoutUmlComponent', () => {
  let component: MvcTaskWithoutUmlComponent;
  let fixture: ComponentFixture<MvcTaskWithoutUmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MvcTaskWithoutUmlComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MvcTaskWithoutUmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
