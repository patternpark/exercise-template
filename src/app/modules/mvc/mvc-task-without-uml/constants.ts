export const AUFGABENSTELLUNG =
  'Es sind drei Beispiele gegeben, wo verschiedene Komponenten miteinander agieren. Die Aufgabe besteht darin, zu sagen, welche der Komponenten im MVC-Pattern das Modell, View oder den Controller darstellen.';

export const EXAMPLE_1_DESCRIPTION =
  'Du Sitzt im Fahrersitz eines Autos und drückst auf das Gaspedal, der Motor setzt sich in gang und der Tacho zeigt nun 50kmh an. ';
export const EXAMPLE_2_DESCRIPTION =
  'Du willst einen Reifen aufpumpen, deine Luftpumpe hat eine eingebaute Druckanzeige, mit jedem Schub zeigt die Pumpe einen höheren Druck an. ';
export const EXAMPLE_3_DESCRIPTION =
  'Der Kontrolleur des Towers schaut, an welcher Position sich gerade die Sitze befinden. Nachdem er die Position kennt, passt er dementsprechend die Farbe der Lichters des Towers an';

export const EXAMPLE_1_ITEM_1 = 'Motor';
export const EXAMPLE_1_ITEM_2 = 'Du selbst';
export const EXAMPLE_1_ITEM_3 = 'Tacho';

export const EXAMPLE_2_ITEM_1 = 'Reifen';
export const EXAMPLE_2_ITEM_2 = 'Luftpumpe';
export const EXAMPLE_2_ITEM_3 = 'Druckanzeige';

export const EXAMPLE_3_ITEM_1 = 'Tower';
export const EXAMPLE_3_ITEM_2 = 'Kontrolleur';
export const EXAMPLE_3_ITEM_3 = 'Lichter';

export const MODEL_TIP = 'Das Modell ist etwas, wo sich der Zustand ändern kann. Es kann manipuliert werden.';
export const VIEW_TIP = 'View ist die Komponente, welche für die Visuelle darstellung des Modells zuständig ist.';
export const CONTROLLER_TIP = 'Der Controller verändert das Modell wodurch sich die View-Komponente anpasst.';
