import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MementoTaskWithUmlComponent } from './memento-task-with-uml.component';

describe('MementoTaskWithUmlComponent', () => {
  let component: MementoTaskWithUmlComponent;
  let fixture: ComponentFixture<MementoTaskWithUmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MementoTaskWithUmlComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MementoTaskWithUmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
