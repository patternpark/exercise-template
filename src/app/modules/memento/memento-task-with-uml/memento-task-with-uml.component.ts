import { Component, OnInit } from '@angular/core';
import { TaskDescriptionService } from '../../../shared/services/task-description.service';

@Component({
  selector: 'patternpark-memento-task-with-uml',
  templateUrl: './memento-task-with-uml.component.html',
  styleUrls: ['./memento-task-with-uml.component.scss'],
})
export class MementoTaskWithUmlComponent implements OnInit {
  constructor(private _taskDescService: TaskDescriptionService) {
    _taskDescService.taskDescription =
      'Versvollständige das Sequenzdiagramm. Treffe dazu an den 6 markierten Stellen die richtige AuswahlVersvollständige das Sequenzdiagramm. Treffe dazu an den 6 markierten Stellen die richtige Auswahl.';
    _taskDescService.taskDescriptionTip = 'Gestrichelte Pfeile sind keine Funktionen';
  }

  evaluateSelection(ev: MouseEvent) {
    ((<HTMLDivElement>ev.target).parentNode!.previousSibling as HTMLElement).innerText = (<HTMLDivElement>(
      ev.target
    )).innerText;

    if (
      ((<HTMLDivElement>ev.target).parentNode!.previousSibling as HTMLElement).dataset.solution ==
      (<HTMLDivElement>ev.target).innerText
    ) {
      ((<HTMLDivElement>ev.target).parentNode!.previousSibling as HTMLElement).style.borderColor = '#44ff44';
    } else {
      ((<HTMLDivElement>ev.target).parentNode!.previousSibling as HTMLElement).style.borderColor = '#ff4444';
    }
  }

  ngOnInit(): void {
    return;
  }
}
