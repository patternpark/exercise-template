import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MementoTaskWithoutUmlComponent } from './memento-task-without-uml.component';

describe('MementoTaskWithoutUmlComponent', () => {
  let component: MementoTaskWithoutUmlComponent;
  let fixture: ComponentFixture<MementoTaskWithoutUmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MementoTaskWithoutUmlComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MementoTaskWithoutUmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
