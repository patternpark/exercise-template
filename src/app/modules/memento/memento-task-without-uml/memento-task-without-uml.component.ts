import { Component, OnInit } from '@angular/core';
import { TaskDescriptionService } from '../../../shared/services/task-description.service';

@Component({
  selector: 'patternpark-memento-task-without-uml',
  templateUrl: './memento-task-without-uml.component.html',
  styleUrls: ['./memento-task-without-uml.component.scss'],
})
export class MementoTaskWithoutUmlComponent implements OnInit {
  constructor(private _taskDescService: TaskDescriptionService) {
    _taskDescService.taskDescription =
      'Wir haben Beispielszenarien, welche sich mit dem Memento-Muster umsetzen lassen. ' +
      'Das bedeutet, dass jedes Beispiel einen Caretakter, einen Originator und ein Memento hat. ' +
      'Die Aufgabe besteht nun darin, den Teilnehmern des Szenarios die jeweilige Rolle zuzuweisen. ' +
      'Ziehe dazu die Text-Kästchen in die jeweiligen Boxen.';
    _taskDescService.taskDescriptionTip =
      'Wenn sich ein Text-Kästchen in der richtigen Reihe befindet, färbt sich die Box grün. ' +
      'Für den Fall, dass das gewählte Beispiel korrekt, aber die Rolle falsch gewählt ist, wird die Box orange. ' +
      'Wenn sowohl die Rolle als auch das Beispiel nicht passen, wird die gewählte Box rot markiert.';
  }

  public readonly PARTICIPANTS_CONTAINER_ID = 'container1';

  dropContainer(ev: DragEvent, id: string) {
    const data = ev.dataTransfer!.getData('text');
    document.getElementById(id)!.appendChild(document.getElementById(data)!);

    this.evaluateAll();
    ev.preventDefault();
  }
  allowDropContainer(ev: DragEvent) {
    ev.preventDefault();
  }

  drag(ev: DragEvent) {
    ev.dataTransfer!.setData('text', (ev.target as HTMLElement).id);
    this.resetColor();
  }
  allowDrop(ev: DragEvent) {
    if ((ev.target! as HTMLElement).className !== 'dragObject') {
      if ((ev.target! as HTMLElement).children.length < 1) {
        ev.preventDefault();
      }
    }
  }
  drop(ev: DragEvent) {
    let data = ev.dataTransfer!.getData('text');
    (ev.target! as HTMLElement).appendChild(document.getElementById(data)!);

    this.resetColor();
    this.evaluateAll();
    ev.preventDefault();
  }

  evaluateAll() {
    for (const exampleDropBox of Array.from(document.getElementsByClassName('exampleDropBox'))) {
      const currentDropBoxChild = exampleDropBox.children[0];
      if (currentDropBoxChild) {
        this.evaluateDropBox(exampleDropBox.id, currentDropBoxChild.id);
      }
    }
  }

  evaluateDropBox(parentId: string, childId: string) {
    const parentBox: HTMLElement = <HTMLElement>document.getElementById(parentId);
    const subBox: HTMLElement = <HTMLElement>document.getElementById(childId);

    if (parentBox.dataset.example == subBox.dataset.example) {
      if (parentBox.dataset.position == subBox.dataset.position) {
        parentBox.style.backgroundColor = '#dfffc3';
      } else {
        parentBox.style.backgroundColor = '#ffdc92';
      }
    } else {
      parentBox.style.backgroundColor = '#ffbcaf';
    }
  }

  resetColor() {
    for (const subBox of Array.from(document.getElementsByClassName('exampleSubBoxes'))) {
      if ((subBox as HTMLElement).children.length < 1) {
        (subBox as HTMLElement).style.backgroundColor = '#ffffff';
      }
    }
  }

  ngOnInit(): void {
    return;
  }
}
