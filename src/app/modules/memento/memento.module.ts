import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MementoTaskWithUmlComponent } from './memento-task-with-uml/memento-task-with-uml.component';
import { MementoTaskWithoutUmlComponent } from './memento-task-without-uml/memento-task-without-uml.component';
import { UiComponentsModule } from '../../core/modules/ui-components.module';

@NgModule({
  declarations: [MementoTaskWithUmlComponent, MementoTaskWithoutUmlComponent],
  imports: [CommonModule, UiComponentsModule],
  exports: [MementoTaskWithUmlComponent, MementoTaskWithoutUmlComponent],
})
export class MementoModule {}
