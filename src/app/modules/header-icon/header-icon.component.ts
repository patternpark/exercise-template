// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { AfterViewInit, Component, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { IpcService } from '../../shared/services/ipc.service';
import { HeaderImageService } from '../../shared/services/header-image.service';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';
import { Router } from '@angular/router';
import { WindowService } from '../../shared/services/window.service';

@Component({
  selector: 'patternpark-header-icon',
  templateUrl: './header-icon.component.html',
  styleUrls: ['./header-icon.component.scss'],
})
export class HeaderIconComponent implements AfterViewInit {
  @ViewChild('closeAppDialog')
  private _dialogCloseApp: TemplateRef<unknown> | undefined;

  private _overlayRef: OverlayRef | undefined;
  private _portal: TemplatePortal | undefined;

  constructor(
    public readonly headerImageService: HeaderImageService,
    private readonly _ipcService: IpcService,
    private readonly _router: Router,
    private readonly _overlay: Overlay,
    private readonly _viewContainerRef: ViewContainerRef,
    private readonly _windowService: WindowService
  ) {}

  ngAfterViewInit(): void {
    this._overlayRef = this._overlay.create({
      backdropClass: 'window-overlay-backdrop',
      positionStrategy: this._overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
    });
  }

  closeApp(): void {
    this._ipcService.closeApp();
  }

  openCloseWindow(): void {
    if (!this._dialogCloseApp || !this._overlayRef) return;

    this._portal = new TemplatePortal(this._dialogCloseApp, this._viewContainerRef);
    this._windowService.overlayRef = this._overlayRef;
    this._overlayRef.attach(this._portal);
  }

  closeDialog(): void {
    this._windowService.closeWindow();
  }
}
