// descriptions
export const DEF_TASK_DESCRIPTION =
  'Ergänze die leeren Felder in dem Klassendiagramm einer Brücke mit den gegebenen Klassen!';
export const DEF_TASK_DESCRIPTION_TIP = 'Ziehe die Klassen in die passenden Felder!';
export const TRY_AGAIN_TASK_DESCRIPTION_TIP =
  'Das war nicht richtig. Versuche es nochmal! Ziehe die Klassen in die passenden Felder!';
export const CORRECT_TASK_DESCIPTION_TIP = 'Genau! Weiter so! Ziehe die Klassen in die passenden Felder!';
export const DONE_TASK_DESCRIPTION_TIP = 'Du hast es geschafft! Das Klassendiagramm einer Brücke ist nun vollständig.';

export const DTO_DATA_KEY = 'text';
