import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BridgeTaskWithUmlComponent } from './bridge-task-with-uml.component';

describe('BridgeTaskWithUmlComponent', () => {
  let component: BridgeTaskWithUmlComponent;
  let fixture: ComponentFixture<BridgeTaskWithUmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BridgeTaskWithUmlComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BridgeTaskWithUmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
