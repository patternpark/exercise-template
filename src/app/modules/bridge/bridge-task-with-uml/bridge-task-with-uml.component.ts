import { Component } from '@angular/core';
import { TaskDescriptionService } from '../../../shared/services/task-description.service';
import {
  CORRECT_TASK_DESCIPTION_TIP,
  DEF_TASK_DESCRIPTION,
  DEF_TASK_DESCRIPTION_TIP,
  DONE_TASK_DESCRIPTION_TIP,
  DTO_DATA_KEY,
  TRY_AGAIN_TASK_DESCRIPTION_TIP,
} from './constants';

@Component({
  selector: 'patternpark-bridge-task-with-uml',
  templateUrl: './bridge-task-with-uml.component.html',
  styleUrls: ['./bridge-task-with-uml.component.scss'],
})
export class BridgeTaskWithUmlComponent {
  constructor(private _taskDescService: TaskDescriptionService) {
    _taskDescService.taskDescription = DEF_TASK_DESCRIPTION;
    _taskDescService.taskDescriptionTip = DEF_TASK_DESCRIPTION_TIP;
  }

  allowDrop(ev: DragEvent) {
    ev.preventDefault();
  }

  drag(ev: DragEvent) {
    ev.dataTransfer?.setData(DTO_DATA_KEY, (ev.target as HTMLElement).id);
  }

  drop(ev: DragEvent) {
    ev.preventDefault();
    const data: string = ev.dataTransfer!.getData(DTO_DATA_KEY);
    const targetElement: HTMLElement = this.getElement(ev.target as HTMLElement);
    const dropUmlIndex: number = this.getClassIndex(targetElement);

    if (dropUmlIndex < 0) return;

    let dropClass: string = targetElement.classList[dropUmlIndex];

    this.handleDrop(dropClass, data, targetElement);
  }

  private handleDrop(dropClass: string, data: string, targetElement: HTMLElement) {
    if (dropClass == data || (dropClass.includes('part') && data.includes(dropClass))) {
      // place element
      targetElement.appendChild(document.getElementById(data) as HTMLElement);

      this.setItemToCorrectPosition(targetElement);

      // prevent double placing
      targetElement.classList.remove(dropClass);

      this.setTipOnSuccess();
    } else {
      this._taskDescService.taskDescriptionTip = TRY_AGAIN_TASK_DESCRIPTION_TIP;
    }
  }

  private setTipOnSuccess() {
    // set tip
    if (
      document.getElementsByClassName('exercise-container-classes')[0].getElementsByClassName('draggable-element')
        .length <= 0
    ) {
      this._taskDescService.taskDescriptionTip = DONE_TASK_DESCRIPTION_TIP;
    } else {
      this._taskDescService.taskDescriptionTip = CORRECT_TASK_DESCIPTION_TIP;
    }
  }

  private setItemToCorrectPosition(targetElement: HTMLElement) {
    // set correct position
    const emptyTemplateTarget: Element = targetElement.getElementsByClassName('empty-template')[0];

    for (const cssClass of Array.from(emptyTemplateTarget.classList)) {
      if (cssClass.includes('empty-template-')) {
        const svgElement = targetElement.getElementsByClassName('draggable-element')[0].getElementsByTagName('svg')[0];
        svgElement.classList.add(cssClass);
        svgElement.style.position = 'absolute';
      }
    }
  }

  private getClassIndex(targetElement: HTMLElement) {
    // set index class in dragged element
    for (let i = 0; i < targetElement.classList.length; ++i) {
      if (targetElement.classList[i].includes('uml-class-')) {
        return i;
      }
    }
    return -1;
  }

  private getElement(targetElement: HTMLElement): HTMLElement {
    // set correct element
    for (let i = 0; i < 2; ++i) {
      if (
        (targetElement.classList == undefined || !targetElement.classList.contains('drop-element')) &&
        targetElement.parentNode != undefined
      ) {
        targetElement = targetElement.parentNode as HTMLElement;
      }
    }
    return targetElement;
  }
}
