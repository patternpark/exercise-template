import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BridgeTaskWithoutUmlComponent } from './bridge-task-without-uml/bridge-task-without-uml.component';
import { BridgeTaskWithUmlComponent } from './bridge-task-with-uml/bridge-task-with-uml.component';
import { UiComponentsModule } from '../../core/modules/ui-components.module';

@NgModule({
  declarations: [BridgeTaskWithoutUmlComponent, BridgeTaskWithUmlComponent],
  imports: [CommonModule, UiComponentsModule],
  exports: [BridgeTaskWithoutUmlComponent, BridgeTaskWithUmlComponent],
})
export class BridgeModule {}
