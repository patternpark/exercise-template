import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BridgeTaskWithoutUmlComponent } from './bridge-task-without-uml.component';

describe('BridgeTaskWithoutUmlComponent', () => {
  let component: BridgeTaskWithoutUmlComponent;
  let fixture: ComponentFixture<BridgeTaskWithoutUmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BridgeTaskWithoutUmlComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BridgeTaskWithoutUmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
