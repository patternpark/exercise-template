import { Component } from '@angular/core';
import { BRIDGE_TASK_WITHOUT_UML_DESCRIPTION, GENERAL_TIP, WIN_MESSAGE, WORD_POOL_CONTAINER_ID } from './constants';
import { TaskDescriptionService } from '../../../shared/services/task-description.service';

@Component({
  selector: 'patternpark-bridge-task-without-uml',
  templateUrl: './bridge-task-without-uml.component.html',
  styleUrls: ['./bridge-task-without-uml.component.scss'],
})
export class BridgeTaskWithoutUmlComponent {
  constructor(private _taskDescService: TaskDescriptionService) {
    _taskDescService.taskDescription = BRIDGE_TASK_WITHOUT_UML_DESCRIPTION;
    _taskDescService.taskDescriptionTip = GENERAL_TIP;
  }

  public readonly WORD_POOL_CONTAINER_ID = WORD_POOL_CONTAINER_ID;

  drag(ev: DragEvent) {
    ev.dataTransfer!.setData('text', (<HTMLDivElement>ev.target).id);
  }

  drop(ev: DragEvent) {
    ev.preventDefault();
    let data = ev.dataTransfer!.getData('text');
    (<HTMLDivElement>ev.target).appendChild(document.getElementById(data)!);
    this.evaluateExercise();
  }

  allowDrop(event: DragEvent) {
    if ((<HTMLDivElement>event.target).className !== 'dragObject') {
      if ((<HTMLDivElement>event.target).children.length < 1) {
        event.preventDefault();
      }
    }
  }

  dropContainer(ev: DragEvent, id: string) {
    ev.preventDefault();
    let data = ev.dataTransfer!.getData('text');
    document.getElementById(id)!.appendChild(document.getElementById(data)!);
  }

  allowDropContainer(ev: DragEvent) {
    ev.preventDefault();
  }

  evaluateExercise() {
    let isSolved = true;
    for (let i = 0; i < 4; i++) {
      let gridElement = document.getElementById('grid_div' + String(i) + '1');
      if (gridElement != null) {
        if (gridElement.hasChildNodes()) {
          let answerAffiliation = (gridElement.firstChild as HTMLElement).dataset.affiliation;
          let correctAffiliation = gridElement.dataset.affiliation;
          if (answerAffiliation != correctAffiliation) {
            isSolved = false;
          }
        } else {
          isSolved = false;
        }
      }
    }
    if (isSolved) {
      this._taskDescService.taskDescriptionTip = WIN_MESSAGE;
    }
  }
}
