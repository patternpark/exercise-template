export const BRIDGE_TASK_WITHOUT_UML_DESCRIPTION =
  'Ziel der Übung ist es, die aufgeführten Zuordnungen sinnvoll anzuordnen,' +
  'so dass das Brückenpattern verwendet wird, ähnlich wie im Animationsfilm beschrieben.' +
  'Die Objekte sollen in die entsprechenden Felder gezogen werden. Zur Hilfestellung bekommst du im Hinweisfeld eine Nachrichten.';
export const WORD_POOL_CONTAINER_ID = 'container1';
export const GENERAL_TIP = 'Ziehe die Zuweisungen in die richtigen Kästen';
export const WIN_MESSAGE = 'Super! Du hast die Aufgabe gelöst.';
