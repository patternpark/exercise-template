import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PoolsService {
  private _poolInital = [
    'Drucker',
    'Logger',
    'Datenbank-Verbindung',
    'Programmweiter Cache',
    'Geschäftslogik',
    'Variablen',
    'Statistiken',
  ];
  private _poolSingleton: string[] = [];
  private _poolNotSingleton: string[] = [];

  /* removes an element from the Inital Example Pool (_poolInitial) */
  public removeFromPoolInitial(element: string) {
    function isNotElement(value: string) {
      return value != element;
    }

    this._poolInital = this._poolInital.filter(isNotElement);
  }

  /* Removes an element from the Singleton Pool (_poolSingleton)*/
  public removeFromPoolSingleton(element: string) {
    function isNotElement(value: string) {
      return value != element;
    }

    this._poolSingleton = this._poolSingleton.filter(isNotElement);
  }
  /* removes an element from the Non-Singleton Pool (_poolNotSingleton) */
  public removeFromPoolNotSingleton(element: string) {
    function isNotElement(value: string) {
      return value != element;
    }

    this._poolNotSingleton = this._poolNotSingleton.filter(isNotElement);
  }

  /* functions to add an element to a Pool */
  public addToPoolInitial(element: string) {
    this._poolInital.push(element);
  }

  public addToPoolSingleton(element: string) {
    this._poolSingleton.push(element);
  }

  public addToPoolNotSingleton(element: string) {
    this._poolNotSingleton.push(element);
  }

  /* Getter for Pools */
  public getInitialPool() {
    return this._poolInital;
  }

  public getSingletonPool() {
    return this._poolSingleton;
  }

  public getNotSingletonPool() {
    return this._poolNotSingleton;
  }
}
