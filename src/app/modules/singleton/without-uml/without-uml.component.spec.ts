import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WithoutUmlComponent } from './without-uml.component';

describe('WithoutUmlComponent', () => {
  let component: WithoutUmlComponent;
  let fixture: ComponentFixture<WithoutUmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WithoutUmlComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WithoutUmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
