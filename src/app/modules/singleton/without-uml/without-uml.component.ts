import { TaskDescriptionService } from '../../../shared/services/task-description.service';
import { PoolsService } from './pools.service';
import { Component } from '@angular/core';

@Component({
  selector: 'patternpark-without-uml',
  templateUrl: './without-uml.component.html',
  styleUrls: ['./without-uml.component.scss'],
})
export class WithoutUmlComponent {
  public initialPool: string[];
  public singletonPool: string[];
  public notSingletonPool: string[];
  private _pools: PoolsService;
  private _description: TaskDescriptionService;

  /* gets the pools from the poolservice and sets the default description for the exercise*/
  constructor(pools: PoolsService, description: TaskDescriptionService) {
    this.initialPool = pools.getInitialPool();
    this.singletonPool = pools.getSingletonPool();
    this.notSingletonPool = pools.getNotSingletonPool();
    this._pools = pools;
    this._description = description;
    this._description.taskDescriptionTip =
      'Klicke auf eines der Beispiele und ordne es dann durch einem Klick auf den dazugehörigen Behälter zu.';
    this._description.taskDescription =
      'Ordne die Situationsbeispiele dem Singleton Behälter zu falls sich das Design-pattern dafür anwenden lässt, oder dem Nicht- Behälter falls ein Singleton dafür keine gute Wahl ist. ';
  }
  /* click-listener for <li> elements; removes the id from the current element with id selected (if any), and sets the clicked elements id to selected */
  onSelect(event: Event) {
    const eventTarget = event.currentTarget as HTMLElement;

    if (!eventTarget) return;

    if (eventTarget.id === 'selected') {
      eventTarget.id = '';
    } else {
      if (!document.getElementById('selected')) {
        document.getElementById('selected')?.removeAttribute('id');
      }
      eventTarget.id = 'selected';
    }
  }
  /* click-listener for <ul> elements; checks if an elemnt with id selected exists,
   then clears its id and reassigns it to the corresponding pool while removing it from the current one. (See pool service)*/
  onAssign(event: Event) {
    if (!document.getElementById('selected')) return;

    // extract information about which item is in what pool, and where it would need to be sent to
    const elementToRemove: string | undefined = document.getElementById('selected')?.innerText;
    const poolToRemoveFrom: string | undefined = document.getElementById('selected')?.parentElement?.id;
    const eventTarget: HTMLElement = event.currentTarget as HTMLElement;

    if (!elementToRemove || eventTarget.tagName !== 'UL' || poolToRemoveFrom == eventTarget.id) return;

    const poolToAddTo: string = eventTarget.id;

    // add the item to the right pool
    if (poolToAddTo === 'initial') {
      this._pools.addToPoolInitial(elementToRemove);
    } else if (poolToAddTo === 'singleton') {
      this._pools.addToPoolSingleton(elementToRemove);
    } else if (poolToAddTo === 'not-assigned') {
      this._pools.addToPoolNotSingleton(elementToRemove);
    }

    // remove the item from its original pool
    if (poolToRemoveFrom === 'initial') {
      this._pools.removeFromPoolInitial(elementToRemove);
    } else if (poolToRemoveFrom === 'singleton') {
      this._pools.removeFromPoolSingleton(elementToRemove);
    } else if (poolToRemoveFrom === 'not-assigned') {
      this._pools.removeFromPoolNotSingleton(elementToRemove);
    }

    // refresh pools with new state
    this.initialPool = this._pools.getInitialPool();
    this.singletonPool = this._pools.getSingletonPool();
    this.notSingletonPool = this._pools.getNotSingletonPool();
  }

  /* compares all pools with their corresponding correct sollution */
  checkSolution() {
    if (this.initialPool.length < 1) {
      // correct solutions:
      const notSingletonCorrect: string[] = ['Geschäftslogik', 'Variablen'];
      const singletonCorrect: string[] = [
        'Drucker',
        'Logger',
        'Datenbank-Verbindung',
        'Programmweiter Cache',
        'Statistiken',
      ];
      // compare arrays
      if (
        this.arraysMatching(this.singletonPool, singletonCorrect) &&
        this.arraysMatching(this.notSingletonPool, notSingletonCorrect)
      ) {
        // correct
        this._description.taskDescriptionTip = 'Herzlichen Glückwunsch, du hast die Übung erfolgreich gelöst';
        return;
      }
    }
    // false
    this._description.taskDescriptionTip = 'Die Lösung ist noch nicht ganz Richtig!';
  }
  /* function to determine if two arrays contain the same items without regard for order */
  arraysMatching(arr1: string[], arr2: string[]) {
    const ForwardMatch = arr1.every((value) => arr2.includes(value));
    const BackwardMatch = arr2.every((value) => arr1.includes(value));
    return ForwardMatch && BackwardMatch;
  }
}
