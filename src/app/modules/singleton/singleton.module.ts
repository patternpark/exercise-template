import { PoolsService } from './without-uml/pools.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WithoutUmlComponent } from './without-uml/without-uml.component';

@NgModule({
  declarations: [WithoutUmlComponent],
  imports: [CommonModule],
  // things that will be linked in src/app/routes.ts
  exports: [WithoutUmlComponent],
  // services we use
  providers: [PoolsService],
})
export class SingletonModule {}
