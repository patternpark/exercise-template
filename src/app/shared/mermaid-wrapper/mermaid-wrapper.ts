// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import mermaid from 'mermaid';
import { Subject } from 'rxjs';
import MermaidClass from './class-diagram/mermaid-class';
import ClassConstructor from './class-diagram/class-constructor';
import ClassField from './class-diagram/class-field';
import ClassFunction from './class-diagram/class-function';

/**
 * Represents an entire UML diagram, with all nodes.
 */
export default abstract class MermaidWrapper {
  /**
   * Specifies the HTML element that contains the diagram. The content is set by Mermaid.
   * @protected
   */
  protected readonly mermaidHolder: HTMLElement;
  /**
   * Specifies Mermaid string to be used to create the diagram.
   * @protected
   */
  protected readonly diagram: string;
  /**
   * Specifies the ID of the diagram that Mermaid should use as prefix.
   * @protected
   */
  protected readonly diagramID: string;
  /**
   *
   * @protected
   */
  protected readonly drawn = new Subject<MermaidWrapper>();

  /**
   * Specifies the observer that will notify when the graph is drawn.
   */
  public readonly drawnEvent = this.drawn.asObservable();

  public constructor(diagram: string, diagramID: string, mermaidHolder: HTMLElement) {
    this.diagram = diagram;
    this.diagramID = diagramID;
    this.mermaidHolder = mermaidHolder;
  }

  public redraw(): void {
    mermaid.render(this.diagramID, this.diagram, (svgCode: string) => {
      this.mermaidHolder.innerHTML = svgCode;

      this.redrawnDiagram();
      this.drawn.next(this);
    });
  }

  public abstract getAttributeOrClassFromElement(
    element: HTMLElement
  ): MermaidClass | ClassConstructor | ClassField | ClassFunction | undefined;

  protected abstract redrawnDiagram(): void;
}
