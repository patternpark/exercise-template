// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer, Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de, contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

/**
 * Represents the access modifiers of the Mermaid uml class diagram.
 *
 * @author Erik der Glückliche (Pseudonym), Frederic Bauer
 */
export enum EAccessModifier {
  /**
   * Private access modifier.
   */
  private = '-',
  /**
   * Protected access modifier.
   */
  protected = '#',
  /**
   * Public access modifier.
   */
  public = '+',
  /**
   * Internal access modifier.
   */
  internal = '~',
}
