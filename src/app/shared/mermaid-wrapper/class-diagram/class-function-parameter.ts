// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

/**
 * Represents a parameter in the UML diagram.
 */
export default class ClassFunctionParameter {
  /**
   * The name of the parameter.
   * @private
   */
  private readonly _name: string;
  /**
   * The type of teh parameter.
   * @private
   */
  private readonly _type: string;

  public constructor(name: string, type: string) {
    this._name = name;
    this._type = type;
  }

  public get name(): string {
    return this._name;
  }

  public get type(): string {
    return this._type;
  }
}
