// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Christian Hemp, Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 christian.hemp[at]hochschule-stralsund.de, Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

/**
 * Represents the different annotations a class can have in a Mermaid
 * uml class diagram.
 */
export enum EClassAnnotations {
  /**
   * Interface annotation for the uml class.
   */
  interface = 'Interface',
  /**
   * Service annotation for the uml class.
   */
  service = 'Service',
  /**
   * Abstract class annotation for the uml class.
   */
  abstract = 'abstract',
  /**
   * Enum annotation for the uml class.
   */
  enumeration = 'enumeration',
}
