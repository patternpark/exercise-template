// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import ClassField from './class-field';
import { EAccessModifier } from './access-modifier.enum';
import ClassFunctionParameter from './class-function-parameter';
import MermaidClass from './mermaid-class';
import { getParameters, getTypeFromSignature, REGEX_FRIENDLY_ACCESS_MODIFIERS } from './utility';

/**
 * Represents a class function/method in the UML diagram.
 */
export default class ClassFunction extends ClassField {
  private readonly _param: ClassFunctionParameter[];
  private readonly _isAbstract: boolean;

  public constructor(
    name: string,
    returnType: string,
    accessModifier: EAccessModifier,
    svgElementPart: HTMLElement,
    holder: MermaidClass,
    parameters: ClassFunctionParameter[] = [],
    isAbstract = false
  ) {
    super(name, returnType, accessModifier, svgElementPart, holder);

    this._param = parameters;
    this._isAbstract = isAbstract;
  }

  public get parameters(): ClassFunctionParameter[] {
    return [...this._param];
  }

  public get isAbstract(): boolean {
    return this._isAbstract;
  }

  public static getClassFunctions(node: MermaidClass, ignoreFunctionsWithoutReturnType = true): ClassFunction[] {
    return [
      ...this.getFunctionsWithAccessModifiers(node, ignoreFunctionsWithoutReturnType),
      ...this.getFunctionsWithoutAccessModifiers(node, ignoreFunctionsWithoutReturnType),
    ].filter((e) => e.name.toLowerCase() != node.name.toLowerCase());
  }

  private static getFunctionsWithAccessModifiers(
    node: MermaidClass,
    ignoreFunctionsWithoutReturnType: boolean
  ): ClassFunction[] {
    const result: ClassFunction[] = [];

    const accessModifierAndNameSplitter = new RegExp(
      `(?<=(${REGEX_FRIENDLY_ACCESS_MODIFIERS.join('|')}))[^\(\)]+(?=\\()`
    );

    for (const element of Array.from(node.htmlElement.querySelectorAll('.nodeLabel'))) {
      const matchGroups = accessModifierAndNameSplitter.exec(element.textContent ?? '');
      if (!matchGroups || matchGroups.length < 2) continue;

      const returnType = getTypeFromSignature(matchGroups.input);
      if (!returnType && ignoreFunctionsWithoutReturnType) continue;

      result.push(
        new ClassFunction(
          matchGroups[0],
          returnType,
          matchGroups[1] as EAccessModifier,
          element as HTMLElement,
          node,
          getParameters(matchGroups.input)
        )
      );
    }

    return result;
  }

  private static getFunctionsWithoutAccessModifiers(
    node: MermaidClass,
    ignoreFunctionsWithoutReturnType: boolean
  ): ClassFunction[] {
    const result: ClassFunction[] = [];

    const namesWithoutAccessModifierSplitter = new RegExp(
      `^[^${REGEX_FRIENDLY_ACCESS_MODIFIERS.join('')}][^\(\)]+(?=\\()`
    );

    for (const element of Array.from(node.htmlElement.querySelectorAll('.nodeLabel'))) {
      const matchGrps = namesWithoutAccessModifierSplitter.exec(element.textContent ?? '');
      if (!matchGrps || matchGrps.length < 1) continue;

      const returnType = getTypeFromSignature(matchGrps.input);
      if (!returnType && ignoreFunctionsWithoutReturnType) continue;

      result.push(
        new ClassFunction(
          matchGrps[0],
          returnType,
          EAccessModifier.internal,
          element as HTMLElement,
          node,
          getParameters(matchGrps.input)
        )
      );
    }

    return result;
  }
}
