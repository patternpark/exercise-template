// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import ClassMember from './class-member';
import { EAccessModifier } from './access-modifier.enum';
import MermaidClass from './mermaid-class';
import { getNameFromFieldDefinition, getTypeFromFieldDefinition, REGEX_FRIENDLY_ACCESS_MODIFIERS } from './utility';

/**
 * Represents a class field in the UML diagram.
 */
export default class ClassField extends ClassMember {
  private readonly _returnType: string;

  public constructor(
    name: string,
    returnType: string,
    accessModifier: EAccessModifier,
    svgElementPart: HTMLElement,
    holder: MermaidClass
  ) {
    super(name, accessModifier, svgElementPart, holder);

    this._returnType = returnType;
  }

  public get returnType(): string {
    return this._returnType;
  }

  public static getClassMembers(node: MermaidClass): ClassField[] {
    return [...this.getMembersWithAccessModifiers(node), ...this.getMembersWithoutAccessModifiers(node)];
  }

  private static getMembersWithAccessModifiers(node: MermaidClass): ClassField[] {
    const result: ClassField[] = [];

    const accessModifierAndNameSplitter = new RegExp(`(?<=(${REGEX_FRIENDLY_ACCESS_MODIFIERS.join('|')}))[^\(\)]+$`);

    for (const element of Array.from(node.htmlElement.querySelectorAll('.nodeLabel')) as HTMLElement[]) {
      const matchGrps = accessModifierAndNameSplitter.exec(element.textContent ?? '');
      if (!matchGrps || matchGrps.length < 2) continue;

      const returnType = getTypeFromFieldDefinition(matchGrps[0]);
      console.log(returnType);
      if (!returnType) continue;

      result.push(
        new ClassField(
          getNameFromFieldDefinition(matchGrps[0]),
          returnType,
          matchGrps[1] as EAccessModifier,
          element,
          node
        )
      );
    }

    return result;
  }

  private static getMembersWithoutAccessModifiers(node: MermaidClass): ClassField[] {
    const result: ClassField[] = [];

    const namesWithoutAccessModifierSplitter = new RegExp(`^[^${REGEX_FRIENDLY_ACCESS_MODIFIERS.join('')}][^\(\)]+$`);

    for (const element of Array.from(node.htmlElement.querySelectorAll('.nodeLabel')) as HTMLElement[]) {
      const matchGrps = namesWithoutAccessModifierSplitter.exec(element.textContent ?? '');
      if (!matchGrps || matchGrps.length < 1) continue;

      const returnType = getTypeFromFieldDefinition(matchGrps.input);
      if (!returnType) continue;

      result.push(
        new ClassField(getNameFromFieldDefinition(matchGrps[0]), returnType, EAccessModifier.internal, element, node)
      );
    }

    return result;
  }
}
