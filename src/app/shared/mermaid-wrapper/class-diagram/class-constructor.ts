// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import ClassMember from './class-member';
import { EAccessModifier } from './access-modifier.enum';
import MermaidClass from './mermaid-class';
import ClassFunctionParameter from './class-function-parameter';
import { getParameters, REGEX_FRIENDLY_ACCESS_MODIFIERS } from './utility';

/**
 * Represents a class constructor in the UML diagram.
 */
export default class ClassConstructor extends ClassMember {
  private readonly _param: ClassFunctionParameter[];

  public constructor(
    name: string,
    accessModifier: EAccessModifier,
    svgElementPart: HTMLElement,
    holder: MermaidClass,
    parameters: ClassFunctionParameter[] = []
  ) {
    super(name, accessModifier, svgElementPart, holder);

    this._param = parameters;
  }

  public get parameters(): ClassFunctionParameter[] {
    return [...this._param];
  }

  public static getClassConstructors(node: MermaidClass): ClassConstructor[] {
    return [
      ...this.getConstructorsWithAccessModifiers(node),
      ...this.getConstructorsWithoutAccessModifiers(node),
    ].filter((e) => e.name.toLowerCase() == node.name.toLowerCase());
  }

  private static getConstructorsWithAccessModifiers(node: MermaidClass): ClassConstructor[] {
    const result: ClassConstructor[] = [];

    const accessModifierAndNameSplitter = new RegExp(
      `(?<=(${REGEX_FRIENDLY_ACCESS_MODIFIERS.join('|')}))[^\(\)]+(?=\\()`
    );

    for (const element of Array.from(node.htmlElement.querySelectorAll('.nodeLabel'))) {
      const matchGroups = accessModifierAndNameSplitter.exec(element.textContent ?? '');

      if (!matchGroups || matchGroups.length < 2) continue;

      result.push(
        new ClassConstructor(
          matchGroups[0],
          matchGroups[1] as EAccessModifier,
          element as HTMLElement,
          node,
          getParameters(matchGroups.input)
        )
      );
    }

    return result;
  }

  private static getConstructorsWithoutAccessModifiers(node: MermaidClass): ClassConstructor[] {
    const result: ClassConstructor[] = [];

    const namesWithoutAccessModifierSplitter = new RegExp(
      `^[^${REGEX_FRIENDLY_ACCESS_MODIFIERS.join('')}][^\(\)]+(?=\\()`
    );

    for (const element of Array.from(node.htmlElement.querySelectorAll('.nodeLabel'))) {
      const matchGroups = namesWithoutAccessModifierSplitter.exec(element.textContent ?? '');

      if (!matchGroups || matchGroups.length < 1) continue;

      result.push(
        new ClassConstructor(
          matchGroups[0],
          EAccessModifier.internal,
          element as HTMLElement,
          node,
          getParameters(matchGroups.input)
        )
      );
    }

    return result;
  }
}
