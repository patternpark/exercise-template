// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import ClassFunctionParameter from '../class-function-parameter';
import UmlClassConstructorGenerator from './uml-class-constructor-generator';
import { EAccessModifier } from '../access-modifier.enum';

/**
 * Generator for functions of a class in the Mermaid uml class diagram.
 *
 * @author Frederic Bauer
 */
export default class UmlClassFunctionGenerator extends UmlClassConstructorGenerator {
  /**
   * The return type the function will have in the class diagram.
   * @protected
   */
  protected readonly _returnType: string;
  /**
   * Whether the function is abstract in the class diagram.
   * @protected
   */
  protected readonly _isAbstract: boolean;

  public constructor(
    name: string,
    accessModifier: EAccessModifier,
    parameters: ClassFunctionParameter[] = [],
    returnType: string = 'void',
    isAbstract: boolean = false
  ) {
    super(name, accessModifier, parameters);

    this._returnType = returnType;
    this._isAbstract = isAbstract;
  }

  /**
   * @inheritDoc
   */
  public getMermaidString(): string {
    return `${this._accessModifier}${this._name}(${this.getMermaidStringParameters()})${this._isAbstract ? '*' : ''} ${
      this._returnType
    }`;
  }
}
