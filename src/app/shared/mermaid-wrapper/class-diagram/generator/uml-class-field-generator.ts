// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import UmlClassMemberGenerator from './uml-class-member-generator';
import { EAccessModifier } from '../access-modifier.enum';

/**
 * Generator for a field within a class of the uml class diagram.
 *
 * @author Frederic Bauer
 */
export default class UmlClassFieldGenerator extends UmlClassMemberGenerator {
  /**
   * The type of the field.
   * @private
   */
  private readonly _fieldType: string;

  public constructor(name: string, accessModifier: EAccessModifier, fieldType: string) {
    super(name, accessModifier);
    this._fieldType = fieldType;
  }

  /**
   * @inheritDoc
   */
  public getMermaidString(): string {
    return `${this._accessModifier}${this._name} : ${this._fieldType} `;
  }
}
