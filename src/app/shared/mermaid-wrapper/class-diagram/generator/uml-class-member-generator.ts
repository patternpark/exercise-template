// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { EAccessModifier } from '../access-modifier.enum';
import UmlClassGenerator from './uml-class-generator';
import { MISSING_CLASS } from '../constants';

/**
 * Abstract generator for members of the uml class generated with Mermaid.
 *
 * @author Frederic Bauer
 */
export default abstract class UmlClassMemberGenerator {
  /**
   * The access modifier associated with the class member. This is expressed through {@link EAccessModifier}.
   * @protected
   */
  protected readonly _accessModifier: EAccessModifier;
  /**
   * The name of the class member.
   * @protected
   */
  protected readonly _name: string;
  /**
   * The uml class the member is a part of.
   * @protected
   */
  protected _holder: UmlClassGenerator | undefined;

  protected constructor(name: string, accessModifier: EAccessModifier) {
    this._name = name;
    this._accessModifier = accessModifier;
  }

  public get holder(): UmlClassGenerator | undefined {
    return this._holder;
  }

  public get name(): string {
    return this._name;
  }

  public setHolder(holder: UmlClassGenerator) {
    this._holder = holder;
  }

  /**
   * Builds the required Mermaid string to express the class member and its attributes
   * in the uml class diagram.
   * @returns string - Returns the Mermaid string representation of the class member.
   */
  public getMermaidString(): string {
    return `${this._holder?.className ?? MISSING_CLASS}: ${this._accessModifier}${this.name}`;
  }
}
