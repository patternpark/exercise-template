// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import UmlClassGenerator from './uml-class-generator';
import { EClassRelationType } from './class-diagram-relation-type.enum';

/**
 * Generator for a class diagram in / with Mermaid.
 *
 * @author Frederic Bauer
 */
export default class UmlClassDiagramGenerator {
  /**
   * Contains all classes that will be a part of the class diagram.
   * @private
   */
  private readonly _classes: UmlClassGenerator[] = [];
  /**
   * Contains all defined relations between the different classes of the diagram.
   * @private
   */
  private readonly _classRelations: string[] = [];

  public constructor(classes: UmlClassGenerator[] = []) {
    this._classes = classes;
  }

  /**
   * Allows adding one or more classes to the diagram.
   * @param classesToAdd
   */
  public addClasses(...classesToAdd: UmlClassGenerator[]): void {
    this._classes.push(...classesToAdd);
  }

  /**
   * Allows adding a relation between two classes known to the generator,
   * based only on the string representation of the classes.
   * @param originElementName The name of the class from which to build the relation.
   * @param endElementName The name of the class to which to build the relation.
   * @param relation The relation to build between the classes.
   */
  public addRelationBasedOnStringsBetweenClasses(
    originElementName: string,
    endElementName: string,
    relation: EClassRelationType
  ): void {
    const target = this._classes.find((classItem) => classItem.className === originElementName);

    const origin = this._classes.find((classItem) => classItem.className === endElementName);

    if (!target || !origin) return;

    this._classRelations.push(this.getRelationMermaidString(originElementName, endElementName, relation));
  }

  /**
   * Allows adding a relation between two classes known to the generator based
   * on the objects given.
   * @param originElement The object representing the source class of the relation.
   * @param endElement The object representing the end class of the relation.
   * @param relation The relation to add between the classes.
   */
  public addRelationBetweenClasses(
    originElement: UmlClassGenerator,
    endElement: UmlClassGenerator,
    relation: EClassRelationType
  ): void {
    if (!(this._classes.includes(originElement) && this._classes.includes(endElement))) return;

    this._classRelations.push(this.getRelationMermaidString(originElement.className, endElement.className, relation));
  }

  /**
   * Builds a Mermaid appropriate string representation of the class diagram to be
   * drawn with the library.
   * @returns string - Returns the Mermaid string representation of the diagram.
   */
  public getMermaidString(): string {
    let mermaidString = '';

    for (const umlClass of this._classes) {
      mermaidString = `${mermaidString}\n${umlClass.getMermaidString()}`;
    }

    let mermaidRelationString = this._classRelations.join('\n');

    return `classDiagram\n${mermaidString}\n${mermaidRelationString}`;
  }

  /**
   * Builds the Mermaid appropriate string representation of the different known relations
   * between the classes of the diagram.
   * @param originElementName The name of the class from which the relation originates.
   * @param endElementName The name of the class that serves as the target / end for the relation.
   * @param relation The relation type to be added between the classes.
   * @private
   * @returns string - The formed mermaidjs string representing the relation.
   */
  private getRelationMermaidString(
    originElementName: string,
    endElementName: string,
    relation: EClassRelationType
  ): string {
    return `${originElementName} ${relation} ${endElementName}`;
  }
}
