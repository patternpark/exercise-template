// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

/**
 * Specifies the possible relations for Mermaid's class diagrams.
 *
 * @author Frederic Bauer
 */
export enum EClassRelationType {
  /**
   * Represents the symbol for the inheritance relation.
   */
  inheritance = '<|--',
  /**
   * Represents the symbol for the composition relation.
   */
  composition = '*--',
  /**
   * Represents the symbol for the aggregation relation.
   */
  aggregation = 'o--',
  /**
   * Represents the symbol for the association relation.
   */
  association = '-->',
  /**
   * Represents the symbol for the solid link relation.
   */
  solidLink = '--',
  /**
   * Represents the symbol for the dependency relation.
   */
  dependency = '..>',
  /**
   * Represents the symbol for the realization relation.
   */
  realization = '..|>',
  /**
   * Represents the symbol for the dashed link relation.
   */
  dashedLink = '..',
}
