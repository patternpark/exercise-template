// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import UmlClassConstructorGenerator from './uml-class-constructor-generator';
import UmlClassFunctionGenerator from './uml-class-function-generator';
import UmlClassFieldGenerator from './uml-class-field-generator';
import UmlClassMemberGenerator from './uml-class-member-generator';
import { EClassAnnotations } from '../class-annotations.enum';

/**
 * Generator for a class in a Mermaid uml class diagram.
 *
 * @author Frederic Bauer
 */
export default class UmlClassGenerator {
  /**
   * Contains the constructors of the class in the diagram.
   * @private
   */
  private readonly _classConstructors: UmlClassConstructorGenerator[] = [];
  /**
   * Contains the functions of the class in the diagram.
   * @private
   */
  private readonly _classFunctions: UmlClassFunctionGenerator[] = [];
  /**
   * Contains the remaining fields of the class in the diagram.
   * @private
   */
  private readonly _classFields: UmlClassFieldGenerator[] = [];
  /**
   * The name of the class in the diagram.
   * @private
   */
  private readonly _className: string;
  /**
   * Optional annotation of the class, see {@link EClassAnnotations}.
   * @private
   */
  private readonly _classAnnotation?: EClassAnnotations;

  public constructor(
    className: string,
    classConstructors: UmlClassConstructorGenerator[] = [],
    classFunctions: UmlClassFunctionGenerator[] = [],
    classFields: UmlClassFieldGenerator[] = [],
    classAnnotation?: EClassAnnotations
  ) {
    this._className = className;
    this._classConstructors = classConstructors;
    this._classFunctions = classFunctions;
    this._classFields = classFields;
    this._classAnnotation = classAnnotation;

    this.setHolderForMembers();
  }

  public get className(): string {
    return this._className;
  }

  /**
   * Adds one or more given constructors to the list of constructors in the class.
   * @param constructorsToAdd Either one ore more constructors to add.
   */
  public addConstructors(...constructorsToAdd: UmlClassConstructorGenerator[]): void {
    for (const constr of constructorsToAdd) {
      constr.setHolder(this);
      this._classConstructors.push(constr);
    }
  }

  /**
   * Adds one ore more given functions to the list of functions in the class.
   * @param functionsToAdd Either one ore more functions to add to the class.
   */
  public addFunctions(...functionsToAdd: UmlClassFunctionGenerator[]): void {
    for (const funct of functionsToAdd) {
      funct.setHolder(this);
      this._classFunctions.push(funct);
    }
  }

  /**
   * Adds one or more given fields to the list of fields in the class.
   * @param fieldsToAdd Either one or more fields to add to the class.
   */
  public addFields(...fieldsToAdd: UmlClassFieldGenerator[]): void {
    for (const field of fieldsToAdd) {
      field.setHolder(this);
      this._classFields.push(field);
    }
  }

  /**
   * Builds the Mermaid string representation of the class and all its members in the
   * uml class diagram.
   * @returns string - Returns the Mermaid string representation of the class.
   */
  public getMermaidString(): string {
    let fieldMermaidString: string = '';
    let constructorMermaidString: string = '';
    let functionMermaidString: string = '';
    let annotationString: string = '';

    for (const field of this._classFields) {
      fieldMermaidString = `${fieldMermaidString}\n${field.getMermaidString()}`;
    }

    for (const constr of this._classConstructors) {
      constructorMermaidString = `${constructorMermaidString}\n${constr.getMermaidString()}`;
    }

    for (const funct of this._classFunctions) {
      functionMermaidString = `${functionMermaidString}\n${funct.getMermaidString()}`;
    }

    if (this._classAnnotation) annotationString = `<<${this._classAnnotation}>>`;

    return `class ${
      this._className
    }{\n${annotationString}\n${fieldMermaidString.trim()}\n${constructorMermaidString.trim()}\n${functionMermaidString.trim()}}`;
  }

  /**
   * Sets itself as the holder class for all its registered members.
   * @private
   */
  private setHolderForMembers(): void {
    const carrierList: UmlClassMemberGenerator[] = [
      ...this._classFields,
      ...this._classFunctions,
      ...this._classConstructors,
    ];

    for (const member of carrierList) member.setHolder(this);
  }
}
