// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import ClassFunctionParameter from '../class-function-parameter';
import UmlClassMemberGenerator from './uml-class-member-generator';
import { EAccessModifier } from '../access-modifier.enum';

/**
 * Generator for constructors of the Mermaid UML diagram.
 *
 * @author Frederic Bauer
 */
export default class UmlClassConstructorGenerator extends UmlClassMemberGenerator {
  /**
   * Contains the parameters for the uml constructor.
   * @protected
   */
  protected readonly _params: ClassFunctionParameter[] = [];

  public constructor(name: string, accessModifier: EAccessModifier, parameters: ClassFunctionParameter[] = []) {
    super(name, accessModifier);

    this._params = parameters;
  }

  /**
   * @inheritDoc
   */
  public getMermaidString(): string {
    return `${this._accessModifier}${this._name}(${this.getMermaidStringParameters()})`;
  }

  /**
   * Builds the string representation of the parameters of the function for the Mermaid uml
   * class diagram.
   * @returns string - Returns the Mermaid string representation of the function's parameters.
   * @protected
   */
  protected getMermaidStringParameters(): string {
    return this._params.map((parameter) => `${parameter.type} ${parameter.name}`).join(', ');
  }
}
