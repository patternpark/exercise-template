// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import ClassFunctionParameter from './class-function-parameter';
import { EAccessModifier } from './access-modifier.enum';
import escapeStringRegexp from 'escape-string-regexp';

/**
 * Specifies a list of all possible access modifiers, escaped, which can be used as a regular expression.
 */
export const REGEX_FRIENDLY_ACCESS_MODIFIERS = Object.values(EAccessModifier).map((m) => escapeStringRegexp(m));

/**
 * Gets the type of a signature. If no type is present in the signature, an empty string is returned.
 * @param signature A signature in the style of Mermaid.
 */
export function getTypeFromSignature(signature: string): string {
  const lastColon = signature.lastIndexOf(':');

  if (lastColon < 0 || lastColon == signature.length - 1) return '';

  return signature.substring(lastColon + 1).trim(); // +1 coz the colon should also be removed
}

/**
 *
 * @param definition The field definition in Mermaid style.
 */
export function getTypeFromFieldDefinition(definition: string): string {
  const parts = definition.split(/\s+/);
  if (parts.length !== 2) return '';

  return parts[0];
}

/**
 *
 * @param definition The field definition in Mermaid style.
 */
export function getNameFromFieldDefinition(definition: string): string {
  const parts = definition.split(/\s+/);
  if (parts.length !== 2) return '';

  return parts[1];
}

/**
 *
 * @param signature The signatur in Mermaid style.
 */
export function getParameters(signature: string): ClassFunctionParameter[] {
  const result: ClassFunctionParameter[] = [];

  const matchGrps = new RegExp(/(?<=[^\\(]+\()[^()]+(?=\))/).exec(signature);

  if (!matchGrps || matchGrps.length < 1) return [];

  for (const singleParaSignature of matchGrps[0].split(/,\s*/)) {
    const parts = singleParaSignature.split(/\s+/);
    if (parts.length != 2) continue;

    result.push(new ClassFunctionParameter(parts[1], parts[0]));
  }

  return result;
}
