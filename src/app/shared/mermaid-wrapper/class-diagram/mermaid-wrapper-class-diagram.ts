// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import MermaidWrapper from '../mermaid-wrapper';
import MermaidClass from './mermaid-class';
import ClassConstructor from './class-constructor';
import ClassField from './class-field';
import ClassFunction from './class-function';

type AttributeResult = MermaidClass | ClassConstructor | ClassField | ClassFunction;

/**
 * Represents an entire UML class diagram.
 */
export default class MermaidWrapperClassDiagram extends MermaidWrapper {
  private _classes: MermaidClass[] = [];

  public get classes(): MermaidClass[] {
    return [...this._classes];
  }

  public getAttributeOrClassFromElement(element: HTMLElement): AttributeResult | undefined {
    const possibleElements: AttributeResult[] = [];

    for (const cls of this._classes) {
      possibleElements.splice(0, 0, cls);

      possibleElements.push(...cls.constructors);
      possibleElements.push(...cls.members);
      possibleElements.push(...cls.functions);
    }

    for (const ele of possibleElements) {
      if (ele.htmlElement === element) return ele;
    }

    return undefined;
  }

  protected redrawnDiagram() {
    this._classes = [];

    Array.from(this.mermaidHolder.querySelectorAll(`#${this.diagramID} .nodes > .node`)).forEach((item) => {
      this._classes.push(new MermaidClass(item as HTMLElement));
    });
  }
}
