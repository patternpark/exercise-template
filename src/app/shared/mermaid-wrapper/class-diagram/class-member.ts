// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { EAccessModifier } from './access-modifier.enum';
import MermaidClass from './mermaid-class';

/**
 * Represents a class member in the UML diagram.
 */
export default abstract class ClassMember {
  private readonly _svgElementPart: HTMLElement;
  private readonly _accessModifier: EAccessModifier;
  private readonly _name: string;
  private readonly _holder: MermaidClass;

  protected constructor(
    name: string,
    accessModifier: EAccessModifier,
    svgElementPart: HTMLElement,
    holder: MermaidClass
  ) {
    this._accessModifier = accessModifier;
    this._name = name;
    this._svgElementPart = svgElementPart;
    this._holder = holder;
  }

  public get accessModifier(): EAccessModifier {
    return this._accessModifier;
  }

  public get name(): string {
    return this._name;
  }

  public get htmlElement(): HTMLElement {
    return this._svgElementPart;
  }

  public get holder(): MermaidClass {
    return this._holder;
  }
}
