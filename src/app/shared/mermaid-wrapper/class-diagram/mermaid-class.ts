// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import ClassConstructor from './class-constructor';
import ClassFunction from './class-function';
import ClassField from './class-field';
import { MISSING_CLASS } from './constants';

/**
 * Represents a class as it is shown in the UML diagram.
 */
export default class MermaidClass {
  private readonly _svgElementPart: HTMLElement;
  private readonly _clsName: string;
  private readonly _isAbstract: boolean = false;

  private readonly _ctors: ClassConstructor[];
  private readonly _funcs: ClassFunction[];
  private readonly _members: ClassField[];

  public constructor(svgElementPart: HTMLElement) {
    this._svgElementPart = svgElementPart;
    this._clsName =
      (svgElementPart.querySelector('.classTitle .nodeLabel') as HTMLSpanElement)?.innerText ?? MISSING_CLASS;

    // the first element is empty OR includes "<<abstract>>"
    this._isAbstract = svgElementPart.querySelector('.nodeLabel')?.textContent?.includes('abstract') ?? false;

    this._ctors = ClassConstructor.getClassConstructors(this);
    this._funcs = ClassFunction.getClassFunctions(this);
    this._members = ClassField.getClassMembers(this);
  }

  public get name(): string {
    return this._clsName;
  }

  public get htmlElement(): HTMLElement {
    return this._svgElementPart;
  }

  public get isAbstract(): boolean {
    return this._isAbstract;
  }

  public get constructors(): ClassConstructor[] {
    return [...this._ctors];
  }

  public get functions(): ClassFunction[] {
    return [...this._funcs];
  }

  public get members(): ClassField[] {
    return [...this._members];
  }
}
