import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HeaderImageService {
  private _headerSize = new BehaviorSubject<number>(80);
  private _headerIcon = new BehaviorSubject<string>('assets/pattern-park-logo.gif');

  public readonly headerSizeEvent = this._headerSize.asObservable();
  public readonly headerIconEvent = this._headerIcon.asObservable();

  public set headerIcon(val: string) {
    this._headerIcon.next(val);
  }
}
