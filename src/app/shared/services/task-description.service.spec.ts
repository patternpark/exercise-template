import { TestBed } from '@angular/core/testing';

import { TaskDescriptionService } from './task-description.service';

describe('TaskDescriptionService', () => {
  let service: TaskDescriptionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TaskDescriptionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
