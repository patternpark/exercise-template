// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ResizedEvent } from 'angular-resize-event';

/**
 * Enables subscribing to events globally, by providing access to thrown events anywhere in the document
 * to every component in order for them to react to these events.
 *
 * @author Frederic Bauer
 */
@Injectable({
  providedIn: 'root',
})
export class GlobalEventListenerService {
  /**
   * Contains the fired resized event for further access down the line.
   * @private
   */
  private readonly _resize = new Subject<ResizedEvent>();
  /**
   * The observable providing access and allowing subscription to the event.
   */
  public readonly resizeEvent = this._resize.asObservable();

  public set resize(val: ResizedEvent) {
    this._resize.next(val);
  }
}
