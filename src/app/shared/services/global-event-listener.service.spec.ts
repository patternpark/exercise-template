import { TestBed } from '@angular/core/testing';

import { GlobalEventListenerService } from './global-event-listener.service';

describe('GlobalEventListenerService', () => {
  let service: GlobalEventListenerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GlobalEventListenerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
