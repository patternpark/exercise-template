// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Injectable, NgZone } from '@angular/core';
import { IpcRenderer } from 'electron';
import {
  EVENT_CLOSE_APP,
  EVENT_GET_JSON_FILE,
  EVENT_GET_JSON_FILE_RESULT,
  EVENT_GET_START_ARGS,
  EVENT_GET_START_ARGS_RESULT,
  EVENT_SET_FULLSCREEN,
} from '../../../../electron/events.constants';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class IpcService {
  private _ipc: IpcRenderer | undefined;

  private _startArgs = new Subject<string[]>();

  public startArgsListener = this._startArgs.asObservable();

  public constructor(private readonly nz: NgZone) {
    if (window.require) {
      this._ipc = window.require('electron').ipcRenderer;

      if (!this._ipc) throw new Error();

      this._ipc.on(EVENT_GET_START_ARGS_RESULT, (event, args: string[]) => {
        this.nz.run(() => {
          this._startArgs.next(args);
        });
      });

      this._ipc.send(EVENT_GET_START_ARGS);
    } else {
      console.error('Could not load electron ipc');
    }
  }

  public getStartUrl() {
    this._ipc?.send(EVENT_GET_START_ARGS);
  }

  public setFullscreenOnAllVideoElements() {
    this._ipc?.send(EVENT_SET_FULLSCREEN);
  }

  public async getJSONFile<T>(file: string): Promise<T> {
    return new Promise<T>((resolve) => {
      this._ipc?.once(EVENT_GET_JSON_FILE_RESULT, (event, args: T) => resolve(args));
      this._ipc?.send(EVENT_GET_JSON_FILE, file);
    });
  }

  public closeApp(): void {
    this._ipc?.send(EVENT_CLOSE_APP);
  }
}
