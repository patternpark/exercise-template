// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TIP_INIT_MESSAGE, HEADER_MESSAGE } from '../../constants';
import { NavigationItem } from '../models/navigation-item';

@Injectable({
  providedIn: 'root',
})
export class TaskDescriptionService {
  private readonly _taskDescription = new BehaviorSubject<string>(HEADER_MESSAGE);
  private readonly _taskDescriptionTip = new BehaviorSubject<string>(TIP_INIT_MESSAGE);
  private readonly _navigationVisibility = new BehaviorSubject<boolean>(false);
  private readonly _navigationPrevious = new BehaviorSubject<NavigationItem>({
    url: '/',
    isEnabled: false,
    isVisible: false,
  });
  private readonly _navigationNext = new BehaviorSubject<NavigationItem>({
    url: '/',
    isEnabled: false,
    isVisible: false,
  });

  public readonly taskDescriptionEvent = this._taskDescription.asObservable();
  public readonly taskDescriptionTipEvent = this._taskDescriptionTip.asObservable();
  public readonly navigationVisibilityEvent = this._navigationVisibility.asObservable();
  public readonly navigationPreviousItemEvent = this._navigationPrevious.asObservable();
  public readonly navigationNextItemEvent = this._navigationNext.asObservable();

  public get taskDescription(): string {
    return this._taskDescription.value;
  }

  public set taskDescription(val: string) {
    if (!val) throw new Error('Invalid value for header description!');

    this._taskDescription.next(val);
  }

  public get taskDescriptionTip(): string {
    return this._taskDescriptionTip.value;
  }

  public set taskDescriptionTip(val: string) {
    if (!val) throw new Error('Invalid value for tip description!');

    this._taskDescriptionTip.next(val);
  }

  public get navigationVisibility(): boolean {
    return this._navigationVisibility.value;
  }

  public set navigationVisibility(val: boolean) {
    this._navigationVisibility.next(val);
  }

  public get navigationPreviousItem(): NavigationItem {
    return this._navigationPrevious.value;
  }

  public set navigationPreviousItem(val: NavigationItem) {
    this._navigationPrevious.next(val);
  }

  public get navigationNextItem(): NavigationItem {
    return this._navigationNext.value;
  }

  public set navigationNextItem(val: NavigationItem) {
    this._navigationNext.next(val);
  }
}
