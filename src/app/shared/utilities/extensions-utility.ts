// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

if (!String.prototype.format) {
  String.prototype.format = function (this: string, ...args: string[]) {
    return this.replace(/{(\d+)}/g, (match: string, number: number) =>
      typeof args[number] != 'undefined' ? args[number] : match
    );
  };
}
