// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer, Christian Hemp, Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de, christian.hemp[at]hochschule-stralsund.de, contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

/**
 * This function aims to provides a "shuffle" to a given array, meaning the objects within the array
 * are being put into a random order. <br/>
 * This is based on the stack overflow answer: https://stackoverflow.com/a/12646864
 * (last accessed on 30.06.2020)
 * @param array This is the array which's content is supposed to be ordered randomly.
 */
// eslint-disable-next-line
export function shuffle(array: any[]): void {
  for (let i = array.length - 1; i > 0; --i) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}
