/**
 * Replaces all occurences of find in str with replace.
 */
export function replaceAll(str: string, find: string, replace: string) {
  return str.replace(new RegExp(find, 'g'), replace);
}
