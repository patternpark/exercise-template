// //////////////////////////////////////////////////////////////////////////////////////////////// //
//
//  Title:                   Pattern Park Exercises
//  Module:                  Software Engineering 2 (Wintersemester 2021/22)
//
//  Authors:                 Frederic Bauer, Erik der Glückliche (Pseudonym)
//  Copyright:               Hochschule Stralsund
//
//  Contact:                 Frederic.Bauer [at] hochschule-stralsund.de, contact [at] lui-studio.net
//  LTS Contact:             wilfried.honekamp@hochschule-stralsund.de
//
// //////////////////////////////////////////////////////////////////////////////////////////////// //

/**
 * Provides KeyFrames for a border color animation based on the colors given.
 * @param color The color the border should be changed towards.
 * @param startingColor The color of the border before the animation occurs.
 * @returns Keyframe[] - An array containing the keyframes for the animation.
 */
export function getBorderColorAnimation(color: string, startingColor = 'black'): Keyframe[] | PropertyIndexedKeyframes {
  return [
    { borderColor: startingColor, offset: 0 },
    { borderColor: color, offset: 0.0425 },
    { borderColor: color, offset: 0.95 },
    { borderColor: startingColor, offset: 1 },
  ];
}

/**
 * Provides the duration for an angular animation.
 * @param duration The duration of the animation
 * @returns KeyframeAnimationOptions - The object containing the duration and other details.
 */
export function getAnimationDuration(duration: number = 5500): KeyframeAnimationOptions {
  return {
    duration: duration,
    iterations: 1,
    direction: 'alternate',
  };
}
