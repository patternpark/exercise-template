export interface MermaidNode {
  [key: string]: MermaidNodeClass;
}

export interface MermaidParserResult {
  parser: {
    yy: {
      getClasses(): MermaidNode;
    };
  };
}

export interface MermaidNodeClass {
  annotations: string[];
  cssClasses: string[];
  domId: string;
  id: string;
  members: string[];
  methods: string[];
  type: string;
}

declare module 'mermaid' {
  interface Mermaid {
    // @ts-ignore
    parse(text: string): MermaidParserResult;
  }
}
export {};
