import './app/shared/utilities/extensions-utility';

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
declare global {
  interface String {
    format(...args: string[]): string;
  }
}
