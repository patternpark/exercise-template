# Pattern Park - Exercises

The Pattern Park is an application to teach students the basics of patterns. This project includes the tasks for the newer UML and non-UML tasks for the Pattern Park.
By using Flash in the original project, an alternative had to be found. The alternative is an HTML page, which is delivered with Electron.

## Installation

### Software & System Requirements

The project requires various system and software requirements to run or compile the application.

#### Run

 * Chromium >= 70 OR Firefox >= 70
   

#### Compile

 * Chromium >= 70 OR Firefox >= 70
 * [Node.js](https://nodejs.org/en/) (version >= 12.22.0)
 * [Yarn](https://yarnpkg.com/)



### Download

An executable version of the application can be downloaded from [GitLab Releases](https://gitlab.hochschule-stralsund.de/pattern-park/exercise-template/-/releases).



### Compile

 * `yarn install`
 * `yarn build`
   

 * `yarn build:release:win` (for Windows)
 * or
 * `yarn build:release:lin` (for Linux)

 * navigate to `release-builds/pattern-park-exercises-win32-ia32` (or `release-builds/pattern-park-exercises-linux-x64` for Linux)
 * run `pattern-park-exercises.exe` (or `pattern-park-exercises` for Linux)



### Run (Development, Watch-Mode)

To run the application during development, the application can be launched in watch mode. The changes are automatically applied and the website is reloaded.

* `yarn start`

> This procedure is supposed for development only and not for a production environment.

## Usage

The application can be used for most of the tasks (with and without UML), as well as for starting videos.

### Exercises

The Angular application uses routes to reach the tasks of the different patterns.
In a normal browser environment, for example, `/factory-method/uml/task1` can be entered to reach the UML task for the pattern "factory method".

To start the task from the Electron application, an argument called "url" must be passed.

```shell
pattern-park.exe "url" "/factory-method/uml/task1"
```

`/factory-method/uml/task1` can be replaced by any route defined in `src/app/routes.ts`.

### Videos

To view a video, the Electron application must be used. Once the "movie" argument and a file are passed, the application starts as a video player.

```shell
pattern-park.exe "movie" "PATH"
```

Furthermore, it is possible to start the video without sound, but with subtitles. For this, "movie-subtitles" must be used and another path to the subtitles must be passed.

```shell
pattern-park.exe "movie-subtitles" "PATH" "PATH_TO_SUBTITLES"
```

> IMPORTANT: The video, as well as the subtitles, must be located relative to the application folder and must not start with a `/`.
> 
> A valid input looks like the following:
> ```shell
> pattern-park.exe "movie-subtitles" "assets/video/movie.mp4" "assets/video/subtitles/subtitles.json"
> ```

## Contributors

- University Stralsund:
  - course group "Softwareprojektorganisation" winter semester 2021
  - Aniketos Stamatios - aniketos.stamatios@protonmail.com
  - Erik der Glückliche - contact [at] lui-studio.net
  - NorthernSeaCharting - aasas@mail.de
  - Soto - zauntormczaun@gmail.com
  - GreenBird
  - Kibarius der Erzmagier

## Libraries

 * [Angular (Frontend Framework)](https://angular.io/)
 * [Electron](https://www.electronjs.org/)
 * [Electron Packager](https://github.com/electron/electron-packager)
 * [mermaid](https://mermaid-js.github.io/mermaid/#/)
 * [leader-line](https://www.npmjs.com/package/leader-line)

## Licence

This application is licensed under the GNU General Public License, Version 2.

[GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
